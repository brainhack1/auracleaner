package com.junk.removal.junkremoval.memory.cleaner.actives;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.junk.removal.junkremoval.memory.cleaner.R;
import com.junk.removal.junkremoval.memory.cleaner.actives.secondary.SuperActivity;
import com.junk.removal.junkremoval.memory.cleaner.enumss.Settings_enums;
import com.junk.removal.junkremoval.memory.cleaner.notific.Util_Ongoing_Notif;

import java.util.Locale;

import static com.junk.removal.junkremoval.memory.cleaner.apps.Applic.isAdAllowed;

public class PreferencesActivity extends SuperActivity {

    private ImageView backBtn;

    private Switch funcMenu, notificationCleaner, autoCleaning, notifPermission, appLocker, chargeInfo;
    private SharedPreferences configs;
    private SharedPreferences.Editor configsEditor;
    private SharedPreferences.Editor langEditor;

    private LinearLayout privacyReference, remove_all_ads, autoCleaningBlock, notifPermissionBlock;

    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        overridePendingTransition(R.anim.fast_fadein, 0);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        configs = getSharedPreferences("Configs", MODE_PRIVATE);
        sharedPreferences = getSharedPreferences("waseem", Context.MODE_PRIVATE);

        setContentView(R.layout.activity_settings_remote);

        try {
            isAdAllowed = !getSharedPreferences("waseem", Context.MODE_PRIVATE).getBoolean(getResources().getString(R.string.is_pay_completed), false);
        } catch (Exception e) {

        }

        backBtn = findViewById(R.id.exit_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        autoCleaning = findViewById(R.id.autoCleaning);
        autoCleaningBlock = findViewById(R.id.autoCleaningBlock);

        autoCleaning.setChecked(configs.getBoolean(Settings_enums.AUTO_CLEANING_ENABLED + "", false));

        autoCleaningBlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getSharedPreferences("waseem", Context.MODE_PRIVATE).getBoolean(getString(R.string.auto_cleaning_availiable), false)) {
                    configsEditor.putBoolean(Settings_enums.AUTO_CLEANING_ENABLED + "", autoCleaning.isChecked());
                    configsEditor.apply();

                    if (autoCleaning.isChecked())
                        Toast.makeText(PreferencesActivity.this, "Auto Cleaning Enabled", Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(PreferencesActivity.this, "Auto Cleaning Disabled", Toast.LENGTH_SHORT).show();
                } else {
                    showAutoCleaningWindow();
                }
            }
        });

        autoCleaning.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (getSharedPreferences("waseem", Context.MODE_PRIVATE).getBoolean(getString(R.string.auto_cleaning_availiable), false)) {
                    configsEditor.putBoolean(Settings_enums.AUTO_CLEANING_ENABLED + "", autoCleaning.isChecked());
                    configsEditor.apply();

                    if (autoCleaning.isChecked())
                        Toast.makeText(PreferencesActivity.this, R.string.auto_cleaning_enabled, Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(PreferencesActivity.this, R.string.auto_cleaning_disabled, Toast.LENGTH_SHORT).show();
                } else {
                    autoCleaning.setChecked(false);

                    showAutoCleaningWindow();
                }
            }
        });

        configsEditor = configs.edit();

        funcMenu = findViewById(R.id.func_menu_change);
        notificationCleaner = findViewById(R.id.notification_cleaner);
        notifPermission = findViewById(R.id.notif_permission_switch);
        appLocker = findViewById(R.id.lock_change);
        chargeInfo = findViewById(R.id.charge_change);

        privacyReference = findViewById(R.id.privacy_reference);
        remove_all_ads = findViewById(R.id.remove_all_ad);
        notifPermissionBlock = findViewById(R.id.notif_permission);

        privacyReference.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri policy_privacy = Uri.parse("https://drive.google.com/file/d/1Meu9pXILJwUowih_QQHJaOEIKv7Ll6AZ/view?usp=sharing");
                Intent link = new Intent (Intent.ACTION_VIEW, policy_privacy);
                startActivity (link);
            }
        });

        funcMenu.setChecked(sharedPreferences.getBoolean(getString(R.string.is_func_menu_allowed), true));
        funcMenu.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences.Editor editor = sharedPreferences.edit();

                if (isChecked) {
                    Util_Ongoing_Notif.showNotification(getApplicationContext());

                    editor.putBoolean(getString(R.string.is_func_menu_allowed), true);
                } else {
                    NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                    notificationManager.cancel(Util_Ongoing_Notif.ONGOING_NOTIFICATION_ID);

                    editor.putBoolean(getString(R.string.is_func_menu_allowed), false);
                }

                editor.apply();
            }
        });

        notificationCleaner.setChecked(checkNotificationEnabled());
        notificationCleaner.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!checkNotificationEnabled()) {
                    redirectToSecureSettings();

                    return;
                }

                configsEditor.putBoolean("isRequireNotifsControl", isChecked);
                configsEditor.apply();

                int NOTIFY_ID = 1;

                if (!isChecked) {
                    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.cancel(NOTIFY_ID);
                }
            }
        });

        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        langEditor = sharedPreferences.edit();
        final String lang = sharedPreferences.getString("lang", "");

        ImageView en = findViewById(R.id.en);
        ImageView ar = findViewById(R.id.ar);
        ImageView es = findViewById(R.id.es);
        ImageView ru = findViewById(R.id.ru);

        ImageView fr = findViewById(R.id.fr);
        ImageView zh = findViewById(R.id.zh);
        ImageView ko = findViewById(R.id.ko);
        ImageView bn = findViewById(R.id.bn);

        ImageView de = findViewById(R.id.de);
        ImageView hi = findViewById(R.id.hi);
        ImageView in = findViewById(R.id.in);
        ImageView ja = findViewById(R.id.ja);

        ImageView sw = findViewById(R.id.sw);
        ImageView ta = findViewById(R.id.ta);
        ImageView te = findViewById(R.id.te);
        ImageView tr = findViewById(R.id.tr);

        ImageView ur = findViewById(R.id.ur);
        ImageView mr = findViewById(R.id.mr);
        ImageView pt = findViewById(R.id.pt);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            en.setClipToOutline(true);
            ar.setClipToOutline(true);
            es.setClipToOutline(true);
            ru.setClipToOutline(true);
            fr.setClipToOutline(true);
            zh.setClipToOutline(true);
            ko.setClipToOutline(true);
            bn.setClipToOutline(true);
            de.setClipToOutline(true);
            hi.setClipToOutline(true);
            in.setClipToOutline(true);
            ja.setClipToOutline(true);
            sw.setClipToOutline(true);
            ta.setClipToOutline(true);
            te.setClipToOutline(true);
            tr.setClipToOutline(true);
            ur.setClipToOutline(true);
            mr.setClipToOutline(true);
            pt.setClipToOutline(true);
        }

        en.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!lang.equals("en")) {
                    setLocale("en");
                }
            }
        });

        ar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!lang.equals("ar")) {
                    setLocale("ar");
                }
            }
        });

        es.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!lang.equals("es")) {
                    setLocale("es");
                }
            }
        });

        ru.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!lang.equals("ru")) {
                    setLocale("ru");
                }
            }
        });

        fr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!lang.equals("fr")) {
                    setLocale("fr");
                }
            }
        });

        zh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!lang.equals("zh")) {
                    setLocale("zh");
                }
            }
        });

        ko.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!lang.equals("ko")) {
                    setLocale("ko");
                }
            }
        });

        bn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!lang.equals("bn")) {
                    setLocale("bn");
                }
            }
        });

        de.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!lang.equals("de")) {
                    setLocale("de");
                }
            }
        });

        hi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!lang.equals("hi")) {
                    setLocale("hi");
                }
            }
        });

        in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!lang.equals("in")) {
                    setLocale("in");
                }
            }
        });

        ja.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!lang.equals("ja")) {
                    setLocale("ja");
                }
            }
        });

        mr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!lang.equals("mr")) {
                    setLocale("mr");
                }
            }
        });

        pt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!lang.equals("pt")) {
                    setLocale("pt");
                }
            }
        });

        sw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!lang.equals("sw")) {
                    setLocale("sw");
                }
            }
        });

        ta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!lang.equals("ta")) {
                    setLocale("ta");
                }
            }
        });

        te.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!lang.equals("te")) {
                    setLocale("te");
                }
            }
        });

        tr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!lang.equals("tr")) {
                    setLocale("tr");
                }
            }
        });

        ur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!lang.equals("ur")) {
                    setLocale("ur");
                }
            }
        });

        remove_all_ads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPremiumWindow();
            }
        });

        notifPermission.setChecked(!configs.getBoolean(Settings_enums.NOTIF_PERMISSION_DISABLED + "", false));

        notifPermission.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                configsEditor.putBoolean(Settings_enums.NOTIF_PERMISSION_DISABLED + "", !notifPermission.isChecked());
                configsEditor.apply();

                if (notifPermission.isChecked())
                    Toast.makeText(PreferencesActivity.this, R.string.notifications_enabled, Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(PreferencesActivity.this, R.string.notifications_disabled, Toast.LENGTH_SHORT).show();
            }
        });

        notifPermissionBlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                notifPermission.setChecked(!notifPermission.isChecked());

                configsEditor.putBoolean(Settings_enums.NOTIF_PERMISSION_DISABLED + "", !notifPermission.isChecked());
                configsEditor.apply();

                if (notifPermission.isChecked())
                    Toast.makeText(PreferencesActivity.this, R.string.notifications_enabled, Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(PreferencesActivity.this, R.string.notifications_disabled, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showAutoCleaningWindow () {
        final RelativeLayout special_deal = findViewById(R.id.special_deal_for_auto_cleaning);
        special_deal.setVisibility(View.VISIBLE);
        special_deal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        ImageView cancel = findViewById(R.id.cancel_auto_cleaning);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                special_deal.setVisibility(View.GONE);
            }
        });

        RelativeLayout get_premium = findViewById(R.id.watch_now);
        get_premium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                special_deal.setVisibility(View.GONE);

                startActivity(new Intent(getApplicationContext(), SubsActivity.class));
                finish();
            }
        });
    }

    public void setLocale(String lang) {
        langEditor.putString("lang", lang);
        langEditor.apply();

        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);

        startActivity(new Intent(getApplicationContext(), PreferencesActivity.class));
        finishAffinity();
    }


    private void showPremiumWindow () {
        final RelativeLayout special_deal = findViewById(R.id.special_deal);
        special_deal.setVisibility(View.VISIBLE);
        special_deal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        ImageView cancel = findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                special_deal.setVisibility(View.GONE);
            }
        });

        RelativeLayout get_premium = findViewById(R.id.get_premium);
        get_premium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    public boolean checkNotificationEnabled() {
        try{
            return Settings.Secure.getString(getApplicationContext().getContentResolver(),
                    "enabled_notification_listeners").contains(getApplicationContext().getPackageName());

        }catch(Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public void redirectToSecureSettings() {
        try {
            Intent intent = new Intent();
            intent.setAction(Settings.ACTION_NOTIFICATION_LISTENER_SETTINGS);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } catch (Exception e) {}
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                startActivity (new Intent(getApplicationContext(), MenuActivity.class));

                finishAffinity ();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        startActivity (new Intent (getApplicationContext(), MenuActivity.class));
        finishAffinity();
    }

    @Override
    public boolean onKeyDown(final int pKeyCode, final KeyEvent pEvent) {
        if(pKeyCode == KeyEvent.KEYCODE_BACK && pEvent.getAction() == KeyEvent.ACTION_DOWN) {

            onBackPressed();

            return true;
        } else {
            return super.onKeyDown(pKeyCode, pEvent);
        }
    }
}
package com.junk.removal.junkremoval.memory.cleaner.actives;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdRequest;
import com.junk.removal.junkremoval.memory.cleaner.R;
import com.junk.removal.junkremoval.memory.cleaner.actives.secondary.SuperActivity;
import com.junk.removal.junkremoval.memory.cleaner.utils.AdsUtility;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import static com.junk.removal.junkremoval.memory.cleaner.apps.Applic.isAdAllowed;

public class InternetSpeedActivity extends SuperActivity {

    private TextView downloadSpeed, uploadSpeed, downloadMax, uploadMax, startBtn;
    private ImageView globe;
    private ImageView exitBtn;

    private InternetSpeedTest test;

    private boolean isSpeedDetected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        overridePendingTransition(R.anim.fast_fadein, 0);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.speed_test);

        downloadSpeed = findViewById(R.id.download_speed);
        uploadSpeed = findViewById(R.id.upload_speed);
        downloadMax = findViewById(R.id.download_speed_max);
        uploadMax = findViewById(R.id.upload_speed_max);

        startBtn = findViewById(R.id.start_btn);

        globe = findViewById(R.id.globe);

        Glide.with(this)
                .load(R.drawable.globe)
                .into(globe);

//        if (isAdAllowed) {
////            adBlockIntersitialAd = new InterstitialAd(InternetSpeedActivity.this);
////            adBlockIntersitialAd.setAdUnitId(getResources().getString(R.string.speed_test_screen));
////            AdRequest adRequestInter = new AdRequest.Builder().build();
////
////            adBlockIntersitialAd.loadAd(adRequestInter);
//
//            mInterstitialAd = new InterstitialAd(this, getString(R.string.facebook_interstitial_ad_id));
//            mInterstitialAd.loadAd(mInterstitialAd.buildLoadAdConfig().build());
//        }

        exitBtn = findViewById(R.id.exit_btn);
        exitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        startBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (hasConnection(getApplicationContext())) {

                    startBtn.setVisibility(View.GONE);

                    test = new InternetSpeedTest();
                    test.execute("https://brainhack.pw/gusi.jpeg");

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (!isSpeedDetected) {
                                isSpeedDetected = true;

                                float downloadSpeedValue = (float) Math.round((Math.random() * 20 + 15) * 10) / 10;
                                float uploadSpeedValue = (float) Math.round((Math.random() * 20 + 15) * 10) / 10;

                                animateTextView(0, downloadSpeedValue*0.9f, downloadSpeed);
                                animateTextView(0,uploadSpeedValue*0.9f, uploadSpeed);

                                animateTextView(0, downloadSpeedValue, downloadMax);
                                animateTextView(0,uploadSpeedValue, uploadMax);
                            }
                        }
                    }, 10000);
                } else
                    Toast.makeText(InternetSpeedActivity.this, R.string.you_are_offline, Toast.LENGTH_SHORT).show();
            }
        });
    }

    class InternetSpeedTest extends AsyncTask<String, Void, String> {
        long startTime;
        long endTime;
        private long takenTime;

        @Override
        protected String doInBackground(String... paramVarArgs) {
            startTime = System.currentTimeMillis();
            long lengthbmp = 0;

            Bitmap bmp = null;
            try {
                for (int i = 0; i < 10; i++) {
                    URL ulrn = new URL(paramVarArgs[0]);
                    HttpURLConnection con = (HttpURLConnection) ulrn.openConnection();
                    InputStream is = con.getInputStream();
                    bmp = BitmapFactory.decodeStream(is);

                    Bitmap bitmap = bmp;
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 99, stream);
                    byte[] imageInByte = stream.toByteArray();
                    lengthbmp = imageInByte.length;
                }

                if (null != bmp) {
                    endTime = System.currentTimeMillis();
                    return lengthbmp*10 + "";
                }
            } catch (final Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(String result) {
            if (result != null) {
                long dataSize = Integer.parseInt(result) / 1024;
                takenTime = endTime - startTime;
                double s = (double) takenTime / 1000;
                double speed = dataSize / s;
                speed /= 10;

                if (!isSpeedDetected) {
                    float speedValue = (float) Math.round(speed * 10) / 10;

                    animateTextView(0, speedValue*0.9f, downloadSpeed);
                    animateTextView(0,speedValue*0.7f, uploadSpeed);

                    animateTextView(0, speedValue, downloadMax);
                    animateTextView(0,speedValue*0.85f, uploadMax);
                }

                isSpeedDetected = true;
            }
        }
    }

    public void animateTextView(float initialValue, float finalValue, final TextView  textview) {

        ValueAnimator valueAnimator = ValueAnimator.ofFloat(initialValue, finalValue);
        valueAnimator.setDuration(2500);

        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {

                textview.setText((float)Math.round(((float)valueAnimator.getAnimatedValue())*10)/10 + "");

            }
        });
        valueAnimator.start();


        startBtn.setVisibility(View.VISIBLE);
    }

    public static boolean hasConnection(final Context context)
    {
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiInfo != null && wifiInfo.isConnected())
        {
            return true;
        }
        wifiInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (wifiInfo != null && wifiInfo.isConnected())
        {
            return true;
        }
        wifiInfo = cm.getActiveNetworkInfo();
        return wifiInfo != null && wifiInfo.isConnected();
    }

    @Override
    public void onBackPressed() {
        AdsUtility.showMrec(R.id.exit_banner, this);

        RelativeLayout exitDialog = findViewById(R.id.exit_dialog);
        exitDialog.setVisibility(View.VISIBLE);

        findViewById(R.id.yes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), MenuActivity.class));

                AdsUtility.showInterstitial(InternetSpeedActivity.this);

                finish();
            }
        });

        findViewById(R.id.no).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exitDialog.setVisibility(View.GONE);
            }
        });
    }
}

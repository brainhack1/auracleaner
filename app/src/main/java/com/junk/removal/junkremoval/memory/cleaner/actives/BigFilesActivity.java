package com.junk.removal.junkremoval.memory.cleaner.actives;

import android.Manifest;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.core.app.ActivityCompat;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

import com.junk.removal.junkremoval.memory.cleaner.actives.secondary.SuperActivity;
import com.junk.removal.junkremoval.memory.cleaner.obj.FileObj;
import com.google.android.gms.ads.AdRequest;
import com.junk.removal.junkremoval.memory.cleaner.R;
import com.junk.removal.junkremoval.memory.cleaner.utils.AdsUtility;
import com.junk.removal.junkremoval.memory.cleaner.utils.FormatFilesSize;

import static com.junk.removal.junkremoval.memory.cleaner.actives.secondary.FragmentsActivity.RC_HANDLE_PERMISSIONA_ALL;
import static com.junk.removal.junkremoval.memory.cleaner.apps.Applic.isAdAllowed;
import static com.junk.removal.junkremoval.memory.cleaner.utils.Util.openFile;

public class BigFilesActivity extends SuperActivity {

    public static myListAdapter measuringAdapter;
    private ListView listView;

    private AppsComponentsLoading appsComponentsLoading;

    private float allSize;

    private ImageView backBtn;
    private TextView allSizeText, allFilesText, cleanAll;

    private ProgressBar progressBar;

    SharedPreferences sharedpreferences, configs;
    SharedPreferences.Editor editor;

//    private InterstitialAd mInterstitialAd;

    private ArrayList<FileObj> bigFiles;

    private LinearLayout statistic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        overridePendingTransition(R.anim.fast_fadein, 0);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        try {
            ActionBar actionBar = getSupportActionBar();
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        setContentView(R.layout.activity_big_files_checker);

        try {
            isAdAllowed = !getSharedPreferences("waseem", Context.MODE_PRIVATE).getBoolean(getResources().getString(R.string.is_pay_completed), false);
        } catch (Exception e) {

        }

        if (!(ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) {

            final String [] permission = new String[]{
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE};

            if(!hasPermissions(this, permission)){
                ActivityCompat.requestPermissions(this, permission, RC_HANDLE_PERMISSIONA_ALL);

                return;
            }
        } else {
//            if (isAdAllowed) {
////                mInterstitialAd = new InterstitialAd(BigFilesActivity.this);
////                mInterstitialAd.setAdUnitId(getResources().getString(R.string.big_files_remover_screen));
////                AdRequest adRequestInter = new AdRequest.Builder().build();
////
////                mInterstitialAd.loadAd(adRequestInter);
//
//                mInterstitialAd = new com.facebook.ads.InterstitialAd(this, getString(R.string.facebook_interstitial_ad_id));
//                mInterstitialAd.loadAd(mInterstitialAd.buildLoadAdConfig().build());
//            }

            backBtn = findViewById(R.id.exit_btn);
            backBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });

            progressBar = findViewById(R.id.progressBar);

            progressBar.getIndeterminateDrawable()
                    .setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);

            allSizeText = findViewById(R.id.allSizeText);
            allFilesText = findViewById(R.id.allFilesText);

            sharedpreferences = getSharedPreferences("waseem", MODE_PRIVATE);
            configs = getSharedPreferences("Configs", MODE_PRIVATE);

            bigFiles = new ArrayList<>();

            appsComponentsLoading = new AppsComponentsLoading();
            appsComponentsLoading.execute();
        }
    }

    private static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        try {
            if (requestCode == RC_HANDLE_PERMISSIONA_ALL) {

                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    finish();
                } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startActivity(new Intent(getApplicationContext(), BigFilesActivity.class));
                    finish();
                }
            }
        } catch (Exception e) {
        }
    }

    private class AppsComponentsLoading extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            File externalStorageDirectory = Environment.getExternalStorageDirectory();
            File folder = new File(externalStorageDirectory.getAbsolutePath());

            checkFiles(folder);

            return null;
        }

        @Override
        protected void onPostExecute(final Void aVoid) {
            super.onPostExecute(aVoid);

            findViewById(R.id.files_info).setVisibility(View.VISIBLE);
            findViewById(R.id.where_listView_relative).setVisibility(View.VISIBLE);

            cleanAll = findViewById(R.id.clean_all);

            cleanAll.setVisibility(View.VISIBLE);

            animateTextView(0, Math.round(allSize), allSizeText);

            allFilesText.setText(bigFiles.size() + " " + getString(R.string.files_detected));

            measuringAdapter = new myListAdapter(getApplicationContext(), bigFiles);

            listView = findViewById(R.id.apps_delete);
            listView.setAdapter(measuringAdapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    openFile(new File(bigFiles.get(i).getPath()), BigFilesActivity.this);
                }
            });

            progressBar.setVisibility(View.GONE);

            measuringAdapter.notifyDataSetChanged();

            cleanAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(BigFilesActivity.this);
                    AlertDialog dialog = builder.setTitle(getResources().getString(R.string.delete_all_files_title)).setMessage(getResources().getString(R.string.delete_all_files_content))
                            .setCancelable(true)
                            .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    for (FileObj file : bigFiles) {
                                        //путь к определенной папке
                                        File dir = new File(file.getPath());

                                        //удаляем папку
                                        deleteRecursive(dir);

                                        measuringAdapter.notifyDataSetChanged();
                                    }

                                    bigFiles.clear();
                                    measuringAdapter.notifyDataSetChanged();

                                    showEndWord();

                                    dialog.cancel();
                                }
                            }).setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            }
                    ).create();

                    dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                        @Override
                        public void onShow(DialogInterface dialogInterface) {
                            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.RED);
                            dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.GRAY);
                        }
                    });

                    dialog.show();
                }
            });

            if(bigFiles.isEmpty()) {
                findViewById(R.id.files_info).setVisibility(View.GONE);
                findViewById(R.id.where_listView_relative).setVisibility(View.GONE);
                cleanAll.setVisibility(View.GONE);

                animateTextView(0, Math.round(allSize), allSizeText);
            }
        }
    }



    private void showEndWord() {
        findViewById(R.id.files_info).setVisibility(View.GONE);
        findViewById(R.id.where_listView_relative).setVisibility(View.GONE);

        findViewById(R.id.endWord).setVisibility(View.VISIBLE);

//        if (isAdAllowed && mInterstitialAd != null && mInterstitialAd.isAdLoaded())
//            mInterstitialAd.show();
    }

    //работа с отображением элементов ListView

    class myListAdapter extends BaseAdapter {
        private final LayoutInflater mLayoutInflater;
        private ArrayList<FileObj> arrayMyMatches;

        public myListAdapter (Context ctx, ArrayList<FileObj> arr) {
            mLayoutInflater = LayoutInflater.from(ctx);
            setArrayMyData(arr);
        }

        public ArrayList<FileObj> getArrayMyData() {
            return arrayMyMatches;
        }

        public void setArrayMyData(ArrayList<FileObj> arrayMyData) {
            this.arrayMyMatches = arrayMyData;
        }

        public boolean remove (int i) {
            arrayMyMatches.remove(i);

            return true;
        }

        public int getCount () {
            return arrayMyMatches.size();
        }

        public Object getItem (int position) {
            FileObj app = arrayMyMatches.get(position);

            return app;
        }

        public long getItemId (int position) {
            return position;
        }

        public void refuse (ArrayList<FileObj> apps) {
            arrayMyMatches.clear ();
            arrayMyMatches.addAll (apps);
            notifyDataSetChanged();
        }

        //получение элемента ListView и его отправка в активность данных

        public View getView(final int position, View convertView, ViewGroup parent) {
            try {
                if (convertView == null) {
                    LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = inflater.inflate(R.layout.init_file_row, null, true);
                }

                ImageView app_image = convertView.findViewById(R.id.app_image);
                TextView app_name = convertView.findViewById(R.id.name);
                RelativeLayout removeBtn = convertView.findViewById(R.id.remove_btn);

                final TextView size = convertView.findViewById(R.id.size);
                size.setText(arrayMyMatches.get(position).getSize() + " MB");

                app_name.setText(arrayMyMatches.get(position).getName());

                app_image.setImageDrawable(getResources().getDrawable(getFileImageByExtension(arrayMyMatches.get(position).getType())));

                removeBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //путь к определенной папке
                        File dir = new File(arrayMyMatches.get(position).getPath());

                        //удаляем папку
                        deleteRecursive(dir);

                        allSize -= bigFiles.get(position).getSize();

                        arrayMyMatches.remove(position);
                        measuringAdapter.notifyDataSetChanged();

                        animateTextView(0, Math.round(allSize), allSizeText);

                        if (arrayMyMatches.isEmpty())
                            showEndWord();
                    }
                });

            } catch (Exception e) {

            } finally {
                return convertView;
            }
        }
    }

    public static int getFileImageByExtension (String extension) {
        if (FormatFilesSize.video.contains(extension))
            return R.drawable.video;
        else if (FormatFilesSize.image.contains(extension))
            return R.drawable.image;
        else if (FormatFilesSize.text.contains(extension))
            return R.drawable.text;
        else if (FormatFilesSize.excele.contains(extension))
            return R.drawable.excele;
        else if (FormatFilesSize.music.contains(extension))
            return R.drawable.music;
        else if (FormatFilesSize.programs.contains(extension))
            return R.drawable.programs;
        else if (FormatFilesSize.archives.contains(extension))
            return R.drawable.archives;
        else if (FormatFilesSize.android.contains(extension))
            return R.drawable.droid;
        else
            return R.drawable.unknown;
    }

    //метод удаляет папку

    private void deleteRecursive(File fileOrDirectory) {
        try {
            fileOrDirectory.delete();
        } catch (Exception e) {}
    }

    private void checkFiles(File parentDir) {
        File[] files = parentDir.listFiles();

        try {
            if (files != null)
                for (File file : files) {
                    if (file.isFile()) {
                        float size = file.length()/1024/1024;

                        if (size > 10) {
                            FileObj fileObject = new FileObj(file.getName(), file.getPath(), size);

                            String extension = file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf("."));
                            Date lastModDate = new Date(file.lastModified());

                            fileObject.setType(extension);
                            fileObject.setDate(lastModDate.toString());

                            bigFiles.add(fileObject);

                            allSize += size;
                        }
                    } else if (file.isDirectory())
                        checkFiles(file);
                }
        } catch (Exception e) {}
    }

    public void animateTextView(int initialValue, int finalValue, final TextView  textview) {

        ValueAnimator valueAnimator = ValueAnimator.ofInt(initialValue, finalValue);
        valueAnimator.setDuration(1500);

        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {

                textview.setText(valueAnimator.getAnimatedValue() + " " + getString(R.string.mb));

            }
        });
        valueAnimator.start();
    }

    @Override
    public boolean onKeyDown(final int pKeyCode, final KeyEvent pEvent) {
        if(pKeyCode == KeyEvent.KEYCODE_BACK && pEvent.getAction() == KeyEvent.ACTION_DOWN) {
            onBackPressed();

            return true;
        } else {
            return super.onKeyDown(pKeyCode, pEvent);
        }
    }

    @Override
    public void onBackPressed() {
        AdsUtility.showMrec(R.id.exit_banner, this);

        RelativeLayout exitDialog = findViewById(R.id.exit_dialog);
        exitDialog.setVisibility(View.VISIBLE);

        findViewById(R.id.yes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), MenuActivity.class));

                AdsUtility.showInterstitial(BigFilesActivity.this);

                finish();
            }
        });

        findViewById(R.id.no).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exitDialog.setVisibility(View.GONE);
            }
        });
    }
}

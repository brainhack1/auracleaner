package com.junk.removal.junkremoval.memory.cleaner.actives;

import android.Manifest;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;

import androidx.annotation.RequiresApi;

import com.google.android.material.navigation.NavigationView;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.junk.removal.junkremoval.memory.cleaner.BuildConfig;
import com.junk.removal.junkremoval.memory.cleaner.actives.secondary.FragmentsActivity;
import com.junk.removal.junkremoval.memory.cleaner.R;
import com.junk.removal.junkremoval.memory.cleaner.actives.secondary.NotifsActivity;
import com.junk.removal.junkremoval.memory.cleaner.actives.secondary.SuperActivity;
import com.junk.removal.junkremoval.memory.cleaner.enumss.Settings_enums;
import com.junk.removal.junkremoval.memory.cleaner.utils.Util;

import java.util.List;
import java.util.Random;

import static com.junk.removal.junkremoval.memory.cleaner.actives.secondary.FragmentsActivity.JUNKSFILES;
import static com.junk.removal.junkremoval.memory.cleaner.actives.secondary.FragmentsActivity.MODE_START_NOW;
import static com.junk.removal.junkremoval.memory.cleaner.actives.secondary.FragmentsActivity.RC_HANDLE_PERMISSIONA_ALL;
import static com.junk.removal.junkremoval.memory.cleaner.actives.secondary.FragmentsActivity.TEMPFILES;
import static com.junk.removal.junkremoval.memory.cleaner.actives.InitialScreenActivity.isCleanRequired;
import static com.junk.removal.junkremoval.memory.cleaner.apps.Applic.isAdAllowed;

public class MenuActivity extends SuperActivity implements NavigationView.OnNavigationItemSelectedListener {
    private ImageView shaker, premium;
    private LinearLayout speedBoosterBtn, junkCleanerBtn, batterySaverBtn, appsManagerBtn, bigFilesChecker;
    private TextView headText, size;
    private Toolbar toolbar;
    private ImageView mainBtn;

    private SharedPreferences sharedPreferences, configs;

    private final Activity activity = this;

    private long lastTimeExit;
    private boolean isExitingFromApp, isMenuItemPressed;

    private boolean isOptimizePressed, isBigFilesPressed, isFilesInfoPressed, isAntivirusPressed, isWhatsappPressed, isFileStatisticPressed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        overridePendingTransition(R.anim.fadein, R.anim.fadeout);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        try {
            setContentView(R.layout.activity_main_menu);

            sharedPreferences = getSharedPreferences("waseem", Context.MODE_PRIVATE);

            headText = findViewById(R.id.mainText);
            size = findViewById(R.id.allSizeText);

            makeUnnecessaryFileSize();

            animateTextView(0, sharedPreferences.getInt(JUNKSFILES,
                    (int)(Math.round(Math.random()*30) + 20)), size);

            //ramDynamic

            ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
            ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
            activityManager.getMemoryInfo(memoryInfo);

            double totalRam = (double) (memoryInfo.totalMem) / (1024 * 1024) / 1000;
            totalRam = Math.ceil(totalRam);

            configs = getSharedPreferences("Configs", MODE_PRIVATE);

            toolbar = findViewById(R.id.toolbar);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                toolbar.setElevation(0);
            }

            setSupportActionBar(toolbar);

            speedBoosterBtn = findViewById(R.id.ram_booster);
            junkCleanerBtn = findViewById(R.id.junk_cleaner);
            batterySaverBtn = findViewById(R.id.battery_saver);
            appsManagerBtn = findViewById(R.id.apps_manager);
            bigFilesChecker = findViewById(R.id.big_files_checker);

            speedBoosterBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(getApplicationContext(), SpeedBoosterActivity.class));
                }
            });

            junkCleanerBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    startActivity(new Intent(getApplicationContext(), JunksDataRemovingActivity.class));

                    finish();
                } else {
                    requestAndExternalPermission();
                }
                }
            });

            batterySaverBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(getApplicationContext(), SaverBatActivity.class));
                }
            });

            appsManagerBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(getApplicationContext(), RemoveAppsActivity.class));
                }
            });

            bigFilesChecker.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            && ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        startActivity(new Intent(getApplicationContext(), BigFilesActivity.class));

                        finish();
                    } else {
                        isBigFilesPressed = true;

                        requestAndExternalPermission();
                    }
                }
            });

            findViewById(R.id.notification_cleaner_func).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!isNotificationEnabled())
                        startActivity(new Intent(getApplicationContext(), NotifsActivity.class));
                    else
                        startActivity(new Intent(getApplicationContext(), NotifsManageActivity.class));
                }
            });

            findViewById(R.id.whatsapp_cleaner).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            && ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        startActivity(new Intent(getApplicationContext(), WhatsappFilesActivity.class));

                        finish();
                    } else {
                        isWhatsappPressed = true;

                        requestAndExternalPermission();
                    }
                }
            });

            findViewById(R.id.speed_test_func).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(getApplicationContext(), InternetSpeedActivity.class));
                    finish();
                }
            });

            findViewById(R.id.apps_locker_func).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            && ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        startActivity(new Intent(getApplicationContext(), FilesInfoActivity.class));
                        finish();
                    } else {
                        isFilesInfoPressed = true;

                        requestAndExternalPermission();
                    }
                }
            });

            findViewById(R.id.device_info_func).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(getApplicationContext(), PhoneInfoActivity.class));
                    finish();
                }
            });

            findViewById(R.id.clipboard_func).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(getApplicationContext(), ClipboardActivity.class));
                    finish();
                }
            });

            if (!isCleanRequired) {
                headText.setText(R.string.cleared);
                size.setVisibility(View.GONE);
            } else {
                Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.on_click_hover_active);
                ImageView whiteTarget = findViewById(R.id.whiteTarget);
                whiteTarget.startAnimation(animation);
            }

            mainBtn = findViewById(R.id.mainBtn);

            final boolean[] touchStayedWithinViewBounds = {true};

            mainBtn.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    switch (motionEvent.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            touchStayedWithinViewBounds[0] = true;

                            Animation hover_down = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.on_hover_btn);
                            mainBtn.startAnimation(hover_down);

                            break;
                        case MotionEvent.ACTION_MOVE:
                            if (touchStayedWithinViewBounds[0]
                                    && !checkMotionEvent(view, motionEvent)) {

                                Animation hover_up = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.on_hover_up_btn);
                                mainBtn.startAnimation(hover_up);

                                touchStayedWithinViewBounds[0] = false;
                            }

                            break;
                        case MotionEvent.ACTION_UP:
                            if (touchStayedWithinViewBounds[0]) {
                                mainBtn.setEnabled(false);

                                Animation hover_up = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.on_hover_up_btn);
                                mainBtn.startAnimation(hover_up);

                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        makeOptimization();
                                    }
                                }, 200);
                            }

                            break;
                    }

                    return true;
                }
            });

            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.addDrawerListener(toggle);
            toggle.syncState();

            drawer.setBackgroundResource(R.drawable.main_menu_gradient_1);

            createDrawerItemsInteractive();

            final NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);
            navigationView.setItemIconTintList(null);

            shaker = findViewById(R.id.shake_light);
            shaker.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (sharedPreferences.getBoolean(getString(R.string.auto_cleaning_availiable), false)) {
                        SharedPreferences.Editor configsEditor = configs.edit();

                        configsEditor.putBoolean(Settings_enums.AUTO_CLEANING_ENABLED + "", true);
                        configsEditor.apply();

                        shaker.setVisibility(View.GONE);
                        shaker.clearAnimation();

                        Toast.makeText(activity, getResources().getString(R.string.auto_cleaning_enabled), Toast.LENGTH_SHORT).show();
                    } else
                        appearAutoCleaningWindow();
                }
            });

            premium = findViewById(R.id.premium);
            premium.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(getApplicationContext(), SubsActivity.class));
                    finish();
                }
            });

            if (!isAdAllowed) {
                premium.setVisibility(View.GONE);

                findViewById(R.id.premium_side).setVisibility(View.GONE);
            }

            if (configs.getBoolean(Settings_enums.AUTO_CLEANING_ENABLED + "", false)) {
                shaker.setVisibility(View.GONE);
                shaker.clearAnimation();
            }

            Animation animationShaker = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shaker);
            animationShaker.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {

                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

            shaker.startAnimation(animationShaker);

            if (sharedPreferences.getBoolean("isNotRequreShowingRating", false)) {
                Menu nav_Menu = navigationView.getMenu();
                nav_Menu.findItem(R.id.rate).setVisible(false);
            }

            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.hamburger_bar);
        } catch (Throwable t) {}
    }

    private void appearAutoCleaningWindow() {
        final RelativeLayout special_deal = findViewById(R.id.special_deal_for_auto_cleaning);
        special_deal.setVisibility(View.VISIBLE);
        special_deal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        ImageView cancel = findViewById(R.id.cancel_auto_cleaning);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                special_deal.setVisibility(View.GONE);
            }
        });

        RelativeLayout get_premium = findViewById(R.id.watch_now);
        get_premium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                special_deal.setVisibility(View.GONE);

                startActivity(new Intent(getApplicationContext(), SubsActivity.class));
                finish();

//                if(mRewardedVideoAd.isLoaded()) {
//                    mRewardedVideoAd.show();
//
//                    SharedPreferences.Editor editor = sharedPreferences.edit();
//                    editor.putBoolean(getString(R.string.auto_cleaning_availiable), true);
//                    editor.apply();
//
//                    SharedPreferences.Editor configsEditor = configs.edit();
//
//                    configsEditor.putBoolean(Settings_enums.AUTO_CLEANING_ENABLED + "", true);
//                    configsEditor.apply();
//
//                    shaker.setVisibility(View.GONE);
//                    shaker.clearAnimation();
//                }
            }
        });
    }

    private boolean checkMotionEvent(View view, MotionEvent event) {
        Rect viewRect = new Rect(
                view.getLeft(),
                view.getTop(),
                view.getRight(),
                view.getBottom()
        );

        return viewRect.contains(
                view.getLeft() + (int) event.getX(),
                view.getTop() + (int) event.getY()
        );
    }

    private void makeOptimization() {
        if (ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

            if (isCleanRequired)
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        appearFragmentActivity(FragmentsActivity.JUNK_CLEANER_CODE, MODE_START_NOW);

                        mainBtn.setEnabled(true);
                    }
                }, 500);
            else
                Toast.makeText(activity, R.string.phone_already_cleared, Toast.LENGTH_SHORT).show();
        } else {

            isOptimizePressed = true;

            requestAndExternalPermission();
        }
    }

    private void requestAndExternalPermission() {
        mainBtn.setEnabled(true);

        final String [] permission = new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE};

        if(!checkPermissions(this, permission)){
            ActivityCompat.requestPermissions(this, permission, RC_HANDLE_PERMISSIONA_ALL);

            return;
        }
    }

    private static boolean checkPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        try {
            if (requestCode == RC_HANDLE_PERMISSIONA_ALL) {
                // for each permission check if the user granted/denied them
                // you may want to group the rationale in a single dialog,
                // this is just an example

                String permission = permissions[0];
                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    isMenuItemPressed = false;
                    isOptimizePressed = false;
                    isBigFilesPressed = false;
                    isFilesInfoPressed = false;
                    isWhatsappPressed = false;
                    isAntivirusPressed = false;
                    isFileStatisticPressed = false;

                    boolean showRationale = shouldShowRequestPermissionRationale(permission);
                    if (!showRationale) {

                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, RC_HANDLE_PERMISSIONA_ALL);

                        Toast.makeText(activity, "Firstly accept store permission", Toast.LENGTH_SHORT).show();
                    }
                } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (isOptimizePressed)
                        makeOptimization();
                    else if (isBigFilesPressed) {
                        startActivity(new Intent(getApplicationContext(), BigFilesActivity.class));

                        finish();
                    } else if (isWhatsappPressed) {
                        startActivity(new Intent(getApplicationContext(), WhatsappFilesActivity.class));

                        finish();
                    } else if (isAntivirusPressed) {
                        startActivity(new Intent(getApplicationContext(), AntivirusScannerActivity.class));

                        finish();
                    } else if (isFilesInfoPressed) {
                        startActivity(new Intent(getApplicationContext(), FilesInfoActivity.class));

                        finish();
                    } else if (isFileStatisticPressed) {
                        Util.getTotalMediaSize();

                        ((TextView) findViewById(R.id.photo_size)).setText((float) Math.round(Util.totalImageSize * 10) / 10 + " GB");
                        ((TextView) findViewById(R.id.video_size)).setText((float) Math.round(Util.totalVideoSize * 10) / 10 + " GB");
                        ((TextView) findViewById(R.id.audio_size)).setText((float) Math.round(Util.totalMusicSize * 10) / 10 + " GB");

                        findViewById(R.id.photo_size).setVisibility(View.VISIBLE);
                        findViewById(R.id.video_size).setVisibility(View.VISIBLE);
                        findViewById(R.id.audio_size).setVisibility(View.VISIBLE);
                        findViewById(R.id.file_system_size).setVisibility(View.VISIBLE);

                        findViewById(R.id.photo_hand).setVisibility(View.GONE);
                        findViewById(R.id.video_hand).setVisibility(View.GONE);
                        findViewById(R.id.audio_hand).setVisibility(View.GONE);
                        findViewById(R.id.files_hand).setVisibility(View.GONE);
                    } else {
                        startActivity(new Intent(getApplicationContext(), JunksDataRemovingActivity.class));

                        finish();
                    }
                }
            }
        } catch (Exception e) {
        }
    }

    public boolean isNotificationEnabled() {
        try{
            return Settings.Secure.getString(MenuActivity.this.getContentResolver(),
                    "enabled_notification_listeners").contains(getApplicationContext().getPackageName());

        }catch(Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private void appearFragmentActivity(String REQUEST_ACTIVITY_CODE, String RUNTIME_MODE) {
        Intent intent = new Intent (getApplicationContext(), FragmentsActivity.class);
        intent.putExtra(FragmentsActivity.REQUEST_ACTIVITY_CODE, REQUEST_ACTIVITY_CODE);
        intent.putExtra(FragmentsActivity.RUNTIME_MODE, RUNTIME_MODE);
        startActivity (intent);

        finish();
    }

    private void makeUnnecessaryFileSize() {
        boolean isUsedCachedData = sharedPreferences.getBoolean("isUsedCachedData", false);

        if (!isUsedCachedData) {
            SharedPreferences.Editor editor = sharedPreferences.edit();

            Random ran1 = new Random();
            final int proc1 = ran1.nextInt(50) + 5;

            Random ran2 = new Random();
            final int proc3 = ran2.nextInt(50) + 10;

            Random ran3 = new Random();
            final int proc2 = ran3.nextInt(50) + 15;

            Random ran4 = new Random();
            final int proc4 = ran4.nextInt(50) + 15;

            editor.putInt("proc1", proc1);
            editor.putInt("proc2", proc2);
            editor.putInt("proc3", proc3);
            editor.putInt("proc4", proc4);

            int alljunk = proc1 + proc2 + proc3 + proc4;

            editor.putInt(JUNKSFILES, alljunk);
            editor.putInt(TEMPFILES, proc2);

            editor.putBoolean("isUsedCachedData", true);
            editor.apply();
        }
    }

    private void createDrawerItemsInteractive () {

        //storage

        Util.getTotalMediaSize();

        findViewById(R.id.file_system_statistic).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!(ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) {
                    isFileStatisticPressed = true;

                    requestAndExternalPermission();
                }
            }
        });

        ((TextView) findViewById(R.id.photo_size)).setText((float)Math.round(Util.totalImageSize * 10)/10 + " GB");
        ((TextView) findViewById(R.id.video_size)).setText((float)Math.round(Util.totalVideoSize * 10)/10 + " GB");
        ((TextView) findViewById(R.id.audio_size)).setText((float)Math.round(Util.totalMusicSize * 10)/10 + " GB");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2)
            ((TextView) findViewById(R.id.file_system_size)).setText(Util.getUsedInternalMemorySize());

        if (ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            findViewById(R.id.photo_size).setVisibility(View.VISIBLE);
            findViewById(R.id.video_size).setVisibility(View.VISIBLE);
            findViewById(R.id.audio_size).setVisibility(View.VISIBLE);
            findViewById(R.id.file_system_size).setVisibility(View.VISIBLE);

            findViewById(R.id.photo_hand).setVisibility(View.GONE);
            findViewById(R.id.video_hand).setVisibility(View.GONE);
            findViewById(R.id.audio_hand).setVisibility(View.GONE);
            findViewById(R.id.files_hand).setVisibility(View.GONE);
        }

        findViewById(R.id.premium_side).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), SubsActivity.class));
                finish();
            }
        });

        findViewById(R.id.file_system).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    startActivity(new Intent(getApplicationContext(), FilesInfoActivity.class));
                    finish();
                } else {
                    isFilesInfoPressed = true;

                    requestAndExternalPermission();
                }
            }
        });

        //opportunities

        findViewById(R.id.updates).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String appId = getApplicationContext().getPackageName();
                Intent rateIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("market://details?id=" + appId));

                final List<ResolveInfo> otherApps = getApplicationContext().getPackageManager()
                        .queryIntentActivities(rateIntent, 0);
                for (ResolveInfo otherApp : otherApps) {
                    if (otherApp.activityInfo.applicationInfo.packageName
                            .equals("com.android.vending")) {

                        ActivityInfo otherAppActivity = otherApp.activityInfo;
                        ComponentName componentName = new ComponentName(
                                otherAppActivity.applicationInfo.packageName,
                                otherAppActivity.name
                        );

                        rateIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        rateIntent.addFlags(Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                        rateIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        rateIntent.setComponent(componentName);
                        getApplicationContext().startActivity(rateIntent);

                        break;
                    }
                }
            }
        });

        findViewById(R.id.about_us).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAboutUs();
            }
        });

        //support

        findViewById(R.id.rate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MenuActivity.this);
                AlertDialog dialog = builder.setTitle(getResources().getString(R.string.rating_popup_title)).setMessage(getResources().getString(R.string.rating_popup_content))
                        .setCancelable(true)
                        .setPositiveButton(getResources().getString(R.string.rating_popup_rate_btn), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putBoolean("isNotRequreShowingRating", true);
                                editor.apply();

//                                openAppInMarket(MenuActivity.this);

                                String appId = getApplicationContext().getPackageName();
                                Intent rateIntent = new Intent(Intent.ACTION_VIEW,
                                        Uri.parse("market://details?id=" + appId));

                                final List<ResolveInfo> otherApps = getApplicationContext().getPackageManager()
                                        .queryIntentActivities(rateIntent, 0);
                                for (ResolveInfo otherApp : otherApps) {
                                    if (otherApp.activityInfo.applicationInfo.packageName
                                            .equals("com.android.vending")) {

                                        ActivityInfo otherAppActivity = otherApp.activityInfo;
                                        ComponentName componentName = new ComponentName(
                                                otherAppActivity.applicationInfo.packageName,
                                                otherAppActivity.name
                                        );

                                        rateIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        rateIntent.addFlags(Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                                        rateIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        rateIntent.setComponent(componentName);
                                        getApplicationContext().startActivity(rateIntent);

                                        break;
                                    }
                                }

                                try {
                                    final NavigationView navigationView = findViewById(R.id.nav_view);
                                    Menu nav_Menu = navigationView.getMenu();
                                    nav_Menu.findItem(R.id.rate).setVisible(false);
                                } catch (Exception e) {}

                                dialog.cancel();
                            }
                        }).setNegativeButton(getResources().getString(R.string.rating_popup_feedback_btn), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                startActivity (new Intent (getApplicationContext(), SupportActivity.class));
                                finish();

                                dialog.cancel();

                            }
                        }
                ).create();

                dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialogInterface) {
                        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.MAGENTA);
                        dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.BLUE);
                    }
                });

                dialog.show();
            }
        });

        findViewById(R.id.share).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String appIdShare = getApplicationContext().getPackageName();

                Intent emailIntent = new Intent();
                emailIntent.setAction(Intent.ACTION_SEND);
                emailIntent.setType("text/plain");
                emailIntent.putExtra(Intent.EXTRA_TEXT, ("https://play.google.com/store/apps/details?id=" + appIdShare));
                startActivity(Intent.createChooser(emailIntent, "Sent the link to your friends using:"));
            }
        });

        findViewById(R.id.feedback).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity (new Intent (getApplicationContext(), SupportActivity.class));
            }
        });

        //preferences

        findViewById(R.id.settings).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity (new Intent (getApplicationContext(), PreferencesActivity.class));
                finish();
            }
        });

        findViewById(R.id.policy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri policy_privacy = Uri.parse("https://drive.google.com/file/d/1Meu9pXILJwUowih_QQHJaOEIKv7Ll6AZ/view?usp=sharing");
                Intent link = new Intent (Intent.ACTION_VIEW, policy_privacy);
                startActivity (link);
            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void showAboutUs() {
        LayoutInflater li = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View promptsView = li.inflate(R.layout.about_us_dialog, null);

        mDialogBuilder = new AlertDialog.Builder(MenuActivity.this);
        mDialogBuilder.setView(promptsView);

        mDialogBuilder.setCancelable(true);
        final AlertDialog alertDialog = mDialogBuilder.create();

        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView versionCode = promptsView.findViewById(R.id.version_code);
        Button exitBtn = promptsView.findViewById(R.id.exit_btn);
        exitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        String versionName = BuildConfig.VERSION_NAME;

        versionCode.setText(versionName);

        alertDialog.show();
    }

    private AlertDialog.Builder mDialogBuilder;

    @Override
    public boolean onKeyDown(final int pKeyCode, final KeyEvent pEvent) {
        if(pKeyCode == KeyEvent.KEYCODE_BACK && pEvent.getAction() == KeyEvent.ACTION_DOWN) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putInt("ratingCount", sharedPreferences.getInt("ratingCount", 0) + 1);
            editor.apply();

            if (!sharedPreferences.getBoolean("isNotRequreShowingRating", false) && sharedPreferences.getInt("ratingCount", 0) > 2) {
                LayoutInflater li = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View promptsView = li.inflate(R.layout.exit_dialog, null);

                mDialogBuilder = new AlertDialog.Builder(MenuActivity.this);
                mDialogBuilder.setView(promptsView);

                final Button rate = promptsView.findViewById(R.id.rate);
                final Button later = promptsView.findViewById(R.id.later);

                mDialogBuilder.setCancelable(true);
                final AlertDialog alertDialog = mDialogBuilder.create();

                alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                rate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putBoolean("isNotRequreShowingRating", true);
                        editor.apply();

                        openAppInMarket(MenuActivity.this);

                        alertDialog.dismiss();
                    }
                });

                later.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                        finishAffinity();
                    }
                });

                alertDialog.show();
            } else
                finishAffinity();

            return true;
        } else {
            return super.onKeyDown(pKeyCode, pEvent);
        }
    }

    public void openAppInMarket(Activity activity) {
        isMenuItemPressed = true;

        String appId = activity.getPackageName();
        Intent rateIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("market://details?id=" + appId));

        final List<ResolveInfo> otherApps = activity.getPackageManager()
                .queryIntentActivities(rateIntent, 0);
        for (ResolveInfo otherApp : otherApps) {
            if (otherApp.activityInfo.applicationInfo.packageName
                    .equals("com.android.vending")) {

                ActivityInfo otherAppActivity = otherApp.activityInfo;
                ComponentName componentName = new ComponentName(
                        otherAppActivity.applicationInfo.packageName,
                        otherAppActivity.name
                );

                rateIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                rateIntent.addFlags(Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                rateIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                rateIntent.setComponent(componentName);
                activity.startActivity(rateIntent);

                break;
            }
        }
    }

    public void animateTextView(int initialValue, int finalValue, final TextView  textview) {

        ValueAnimator valueAnimator = ValueAnimator.ofInt(initialValue, finalValue);
        valueAnimator.setDuration(1500);

        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {

                textview.setText(valueAnimator.getAnimatedValue() + " " + getString(R.string.mb));

            }
        });
        valueAnimator.start();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (isExitingFromApp && (System.currentTimeMillis() - lastTimeExit) > 1000*45 && !(isMenuItemPressed
                || isOptimizePressed || isBigFilesPressed || isFilesInfoPressed || isAntivirusPressed
                || isWhatsappPressed || isFileStatisticPressed)) {
            startActivity (new Intent (getApplicationContext (), InitialScreenActivity.class));
            finish();
        }

        isMenuItemPressed = false;
        isExitingFromApp = false;
    }

    @Override
    protected void onUserLeaveHint() {
        super.onStop();

        lastTimeExit = System.currentTimeMillis();
        isExitingFromApp = true;
    }
}

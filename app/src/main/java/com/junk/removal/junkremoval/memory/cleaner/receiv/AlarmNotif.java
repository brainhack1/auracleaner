package com.junk.removal.junkremoval.memory.cleaner.receiv;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.widget.RemoteViews;

import com.junk.removal.junkremoval.memory.cleaner.R;
import com.junk.removal.junkremoval.memory.cleaner.actives.InitialScreenActivity;
import com.junk.removal.junkremoval.memory.cleaner.enumss.Settings_enums;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by artem.bolgov on 07.02.2018.
 */

public class AlarmNotif extends BroadcastReceiver {

    public static int NOTIFICATION_ID = 1;

    @Override
    public void onReceive(Context context, Intent intent) {

        SharedPreferences configs = context.getSharedPreferences("Configs", MODE_PRIVATE);

        if (!configs.getBoolean(Settings_enums.NOTIF_PERMISSION_DISABLED + "", false)) {
            NotificationManager notificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                NotificationChannel channel = new NotificationChannel((NOTIFICATION_ID + "_channel"), "Applic deleting channel",
                        NotificationManager.IMPORTANCE_HIGH);
                channel.setDescription("Applic deleting channel");
                channel.enableLights(true);
                channel.setLightColor(Color.RED);
                channel.enableVibration(true);
                notificationManager.createNotificationChannel(channel);
            }

            RemoteViews remoteViews = null;
            Intent resultIntent = null;

            if (NOTIFICATION_ID == 1) {
                NOTIFICATION_ID = 2;

                resultIntent = new Intent(context, InitialScreenActivity.class);

                remoteViews = new RemoteViews(context.getPackageName(), R.layout.whatsapp_notification);
            } else {
                NOTIFICATION_ID = 1;

                resultIntent = new Intent(context, InitialScreenActivity.class);

                remoteViews = new RemoteViews(context.getPackageName(), R.layout.big_files_notification);
            }

            PendingIntent resultPendingIntent = PendingIntent.getActivity(context, 0, resultIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);

            Notification.Builder builder = new Notification.Builder(context)
                    .setSmallIcon(R.drawable.logo)
                    .setContent(remoteViews)
                    .setContentIntent(resultPendingIntent)
                    .setContentTitle("Cache Cleaner")
                    .setContentText("Cache Cleaner")
                    .setAutoCancel(true)
                    .setDefaults(Notification.DEFAULT_LIGHTS);

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                builder.setChannelId((NOTIFICATION_ID + "_channel"));
            }

            notificationManager.notify(NOTIFICATION_ID, builder.build());
        }
    }
}

package com.junk.removal.junkremoval.memory.cleaner.actives.secondary;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.junk.removal.junkremoval.memory.cleaner.R;
import com.junk.removal.junkremoval.memory.cleaner.actives.MenuActivity;
import com.junk.removal.junkremoval.memory.cleaner.actives.NotifsManageActivity;
import com.junk.removal.junkremoval.memory.cleaner.actives.secondary.SuperActivity;

public class NotifsActivity extends SuperActivity {

    Handler permissionCheckingHandler;
    Runnable waitingRunnable;

    public static ImageView mainbutton;

    private TextView active;

    private ImageView backBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        overridePendingTransition(R.anim.fast_fadein, 0);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_notification_cleaner);

        backBtn = findViewById(R.id.exit_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        active = findViewById(R.id.btn_active);

        active.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setContentView(R.layout.notifs_cleaning_hint);

                if (checkNotificationEnabled())
                    startActivity(new Intent(getApplicationContext(), NotifsManageActivity.class));
                else
                    usageAccessSettingsPage();
            }
        });
    }

    public boolean checkNotificationEnabled() {
        try{
            return Settings.Secure.getString(getApplicationContext().getContentResolver(),
                    "enabled_notification_listeners").contains(getApplicationContext().getPackageName());

        }catch(Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public void usageAccessSettingsPage(){
        waitingRunnable = new Runnable() {
            @Override
            public void run() {
                redirectToSecureSettings();

                while (!checkNotificationEnabled()) {
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    if (checkNotificationEnabled()) {
                        startActivity(new Intent(getApplicationContext(), NotifsManageActivity.class));

                        break;
                    }
                }
            }
        };

        permissionCheckingHandler = new Handler();
        permissionCheckingHandler.postDelayed(waitingRunnable, 2000);
    }

    public void redirectToSecureSettings() {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_NOTIFICATION_LISTENER_SETTINGS);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        startActivity(new Intent (getApplicationContext(), MenuActivity.class));

        startActivity(intent);
    }

    @Override
    public void onDestroy() {
        try {
            permissionCheckingHandler.removeCallbacks(waitingRunnable);
        } catch (Exception e) {

        }

        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        Intent startHomescreen = new Intent(getApplicationContext(), MenuActivity.class);
        startActivity(startHomescreen);

        finish();
    }
}
package com.junk.removal.junkremoval.memory.cleaner.actives;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.junk.removal.junkremoval.memory.cleaner.R;
import com.junk.removal.junkremoval.memory.cleaner.actives.secondary.NotifsActivity;
import com.junk.removal.junkremoval.memory.cleaner.utils.AdsUtility;

import static com.junk.removal.junkremoval.memory.cleaner.actives.secondary.FragmentsActivity.RC_HANDLE_PERMISSIONA_ALL;
import static com.junk.removal.junkremoval.memory.cleaner.apps.Applic.isAdAllowed;

public class AfterCleaningActivity extends AppCompatActivity {
    private boolean isBigFilesButtonPressed;

    private RelativeLayout cacheCleanerBtn, bigFilesBtn, phoneBoosterBtn, notifsCleanerBtn, appsCleanerBtn;
    private LinearLayout cacheCleanerBtnContainer, bigFilesBtnContainer, phoneBoosterBtnContainer, notifsCleanerBtnContainer, appsCleanerBtnContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        overridePendingTransition(R.anim.fast_fadein, 0);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.after_cleaning_activity);

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        findViewById(R.id.premium_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), SubsActivity.class));
            }
        });

        if (!isAdAllowed)
            findViewById(R.id.premium_btn).setVisibility(View.GONE);

        cacheCleanerBtn = findViewById(R.id.cache_cleaner_btn);
        cacheCleanerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    startActivity(new Intent(getApplicationContext(), JunksDataRemovingActivity.class));

                    finish();
                } else {
                    requestAndExternalPermission();
                }
            }
        });

        bigFilesBtn = findViewById(R.id.big_files_btn);
        bigFilesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    startActivity(new Intent(getApplicationContext(), BigFilesActivity.class));

                    finish();
                } else {
                    isBigFilesButtonPressed = true;

                    requestAndExternalPermission();
                }
            }
        });

        phoneBoosterBtn = findViewById(R.id.phone_booster_btn);
        phoneBoosterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), SpeedBoosterActivity.class));
            }
        });

        notifsCleanerBtn = findViewById(R.id.notif_cleaner_btn);
        notifsCleanerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!checkNotificationEnabled())
                    startActivity(new Intent(getApplicationContext(), NotifsActivity.class));
                else
                    startActivity(new Intent(getApplicationContext(), NotifsManageActivity.class));
            }
        });

        appsCleanerBtn = findViewById(R.id.apps_cleaner_btn);
        appsCleanerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), RemoveAppsActivity.class));
            }
        });

        cacheCleanerBtnContainer = findViewById(R.id.cache_cleaner_btn_container);
        bigFilesBtnContainer = findViewById(R.id.big_files_btn_container);
        phoneBoosterBtnContainer = findViewById(R.id.phone_booster_btn_container);
        notifsCleanerBtnContainer = findViewById(R.id.notif_cleaner_btn_container);
        appsCleanerBtnContainer = findViewById(R.id.apps_cleaner_btn_container);

        if (!getSharedPreferences("waseem", Context.MODE_PRIVATE).getString("booster", "1").equals("1"))
            phoneBoosterBtnContainer.setVisibility(View.GONE);

        if (checkNotificationEnabled())
            notifsCleanerBtnContainer.setVisibility(View.GONE);
    }



    private void requestAndExternalPermission() {
        final String[] permission = new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE};

        if(!hasPermissions(this, permission)){
            ActivityCompat.requestPermissions(this, permission, RC_HANDLE_PERMISSIONA_ALL);

            return;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        try {
            if (requestCode == RC_HANDLE_PERMISSIONA_ALL) {
                // for each permission check if the user granted/denied them
                // you may want to group the rationale in a single dialog,
                // this is just an example

                String permission = permissions[0];
                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    isBigFilesButtonPressed = false;

                    boolean showRationale = shouldShowRequestPermissionRationale(permission);
                    if (!showRationale) {

                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, RC_HANDLE_PERMISSIONA_ALL);

                        Toast.makeText(this, "Firstly accept store permission", Toast.LENGTH_SHORT).show();
                    }
                } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (isBigFilesButtonPressed) {
                        startActivity(new Intent(getApplicationContext(), BigFilesActivity.class));

                        finish();
                    } else {
                        startActivity(new Intent(getApplicationContext(), JunksDataRemovingActivity.class));

                        finish();
                    }
                }
            }
        } catch (Exception e) {
        }
    }

    private static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean checkNotificationEnabled() {
        try{
            return Settings.Secure.getString(getContentResolver(),
                    "enabled_notification_listeners").contains(getApplicationContext().getPackageName());

        }catch(Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        AdsUtility.showMrec(R.id.exit_banner, this);

        RelativeLayout exitDialog = findViewById(R.id.exit_dialog);
        exitDialog.setVisibility(View.VISIBLE);

        findViewById(R.id.yes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), MenuActivity.class));

                finish();
            }
        });

        findViewById(R.id.no).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exitDialog.setVisibility(View.GONE);
            }
        });
    }
}
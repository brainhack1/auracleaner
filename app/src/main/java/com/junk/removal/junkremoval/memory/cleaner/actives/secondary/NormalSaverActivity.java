package com.junk.removal.junkremoval.memory.cleaner.actives.secondary;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.junk.removal.junkremoval.memory.cleaner.R;
import com.google.android.gms.ads.AdRequest;
import com.hookedonplay.decoviewlib.DecoView;
import com.hookedonplay.decoviewlib.charts.SeriesItem;
import com.hookedonplay.decoviewlib.events.DecoEvent;
import com.junk.removal.junkremoval.memory.cleaner.actives.AfterCleaningActivity;
import com.junk.removal.junkremoval.memory.cleaner.utils.AdsUtility;

import static com.junk.removal.junkremoval.memory.cleaner.apps.Applic.isAdAllowed;

public class NormalSaverActivity extends SuperActivity {

    DecoView arcView;
    TextView ist,sec,thir,fou,completion;
    ImageView istpic,secpic,thirpic,foupic;
    View one, two, three, four;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;
    int check=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.revert_to_normal);

        ist= findViewById(R.id.ist);
        sec= findViewById(R.id.sec);
        thir= findViewById(R.id.thi);
        fou= findViewById(R.id.fou);
        istpic= findViewById(R.id.istpic);
        secpic= findViewById(R.id.secpic);
        thirpic= findViewById(R.id.thipic);
        foupic= findViewById(R.id.foupic);
        completion= findViewById(R.id.completion);

        one= findViewById(R.id.view_one);
        two= findViewById(R.id.view_two);
        three= findViewById(R.id.view_three);
        four= findViewById(R.id.view_four);


        sharedpreferences = getSharedPreferences("was", Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();

        arcView = findViewById(R.id.dynamicArcView2);

        arcView.addSeries(new SeriesItem.Builder(Color.parseColor("#FFFFFF"))
                .setRange(0, 100, 100)
                .setInitialVisibility(false)
                .setLineWidth(45f)
                .build());

        SeriesItem seriesItem2 = new SeriesItem.Builder(Color.parseColor("#00ff00"))
                .setRange(0, 100, 0)
                .setLineWidth(45f)
                .build();

        int series1Index2 = arcView.addSeries(seriesItem2);

        seriesItem2.addArcSeriesItemListener(new SeriesItem.SeriesItemListener() {
            @Override
            public void onSeriesItemAnimationProgress(float v, float v1) {

                Float obj = new Float(v1);
                int i = obj.intValue();
                completion.setText(i+"%");

                if(v1>=10 && v1<50)
                {
                    istpic.setImageResource(R.drawable.orange_circle);
                }
                else if(v1>=50 && v1<75)
                {
                    secpic.setImageResource(R.drawable.orange_circle);
                }
                else if(v1>=75 && v1<90)
                {
                    thirpic.setImageResource(R.drawable.orange_circle);
                }
                else if(v1>=90 && v1<=100)
                {
                    foupic.setImageResource(R.drawable.orange_circle);
                }
            }

            @Override
            public void onSeriesItemDisplayProgress(float v) {

            }
        });

        arcView.addEvent(new DecoEvent.Builder(DecoEvent.EventType.EVENT_SHOW, true)
                .setDelay(0)
                .setDuration(0)
                .setListener(new DecoEvent.ExecuteEventListener() {
                    @Override
                    public void onEventStart(DecoEvent decoEvent) {
                    }

                    @Override
                    public void onEventEnd(DecoEvent decoEvent) {

                    }

                })
                .build());

        arcView.addEvent(new DecoEvent.Builder(100).setIndex(series1Index2).setDelay(1000).setListener(new DecoEvent.ExecuteEventListener() {
            @Override
            public void onEventStart(DecoEvent decoEvent) {
            }

            @Override
            public void onEventEnd(DecoEvent decoEvent) {
                try
                {
                    AdsUtility.showInterstitial(NormalSaverActivity.this);
                }
                catch (Exception e)
                {

                }

                check=1;
                youDesirePermissionCode(NormalSaverActivity.this);

                editor.putString("mode", "0");
                editor.commit();

            }
        }).build());
    }

    public void enablesall()
    {
        CompSaverActivity.setAutoOrientationEnabled(getApplicationContext(), true);

        Settings.System.putInt(this.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, 255);

        ContentResolver.setMasterSyncAutomatically(true);

    }

    @Override
    public void onBackPressed() {
    }

    public void youDesirePermissionCode(Activity context){
        boolean permission;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            permission = Settings.System.canWrite(context);
        } else {
            permission = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_SETTINGS) == PackageManager.PERMISSION_GRANTED;
        }
        if (permission) {
            //do your code
            enablesall();

            finish();
        }  else {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
                intent.setData(Uri.parse("package:" + context.getPackageName()));
                context.startActivityForResult(intent, 1);
            } else {
                ActivityCompat.requestPermissions(context, new String[]{Manifest.permission.WRITE_SETTINGS}, 1);
            }
        }
    }
    //
    @SuppressLint("NewApi")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && Settings.System.canWrite(this)){

            enablesall();

            finish();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

           enablesall();

            finish();
        }

    }


    @Override
    protected void onResume() {
        super.onResume();
        if(check==1)
        {
            try
            {
                enablesall();
            }
            catch(Exception e)
            {
                finish();
            }
            finish();
        }
    }
}

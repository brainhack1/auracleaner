package com.junk.removal.junkremoval.memory.cleaner.actives.secondary;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.junk.removal.junkremoval.memory.cleaner.R;
import com.hookedonplay.decoviewlib.DecoView;
import com.hookedonplay.decoviewlib.charts.SeriesItem;
import com.hookedonplay.decoviewlib.events.DecoEvent;

public class ActivityApply extends SuperActivity {
    DecoView arcView;
    TextView ist,sec,thir,fou,completion,fif;
    View vOne, vTwo, vThree, vFour, vFive;
    ImageView istpic,secpic,thirpic,foupic,fifthpic;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;
    int check=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.applying_ultra);

        ist= findViewById(R.id.ist);
        sec= findViewById(R.id.sec);
        thir= findViewById(R.id.thi);
        fou= findViewById(R.id.fou);
        fif= findViewById(R.id.fif);
        istpic= findViewById(R.id.istpic);
        secpic= findViewById(R.id.secpic);
        thirpic= findViewById(R.id.thipic);
        foupic= findViewById(R.id.foupic);
        foupic= findViewById(R.id.foupic);
        fifthpic = findViewById(R.id.fifthpic);
        completion= findViewById(R.id.completion);

        vOne= findViewById(R.id.view_one);
        vTwo= findViewById(R.id.view_two);
        vThree= findViewById(R.id.view_three);
        vFour= findViewById(R.id.view_four);
        vFive= findViewById(R.id.view_five);


        sharedpreferences = getSharedPreferences("was", Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();


        arcView = findViewById(R.id.dynamicArcView2);

        arcView.addSeries(new SeriesItem.Builder(Color.parseColor("#FFFFFF"))
                .setRange(0, 100, 100)
                .setInitialVisibility(false)
                .setLineWidth(45f)
                .build());

        SeriesItem seriesItem1 = new SeriesItem.Builder(Color.parseColor("#FFFFFF"))
                .setRange(0, 100, 0)
                .setLineWidth(45f)
                .build();

        SeriesItem seriesItem2 = new SeriesItem.Builder(Color.parseColor("#00ff00"))
                .setRange(0, 100, 0)
                .setLineWidth(45f)
                .build();
//
//        int series1Index = arcView.addSeries(seriesItem1);
        int series1Index2 = arcView.addSeries(seriesItem2);

        seriesItem2.addArcSeriesItemListener(new SeriesItem.SeriesItemListener() {
            @Override
            public void onSeriesItemAnimationProgress(float v, float v1) {


                Float obj = new Float(v1);
                int i = obj.intValue();
                completion.setText(i+"%");

                if(v1>=10 && v1<40)
                {
                    ist.setTextColor(Color.parseColor("#4e5457"));
                    istpic.setImageResource(R.mipmap.ic_blue_dot);
//                    vOne.setVisibility(View.GONE);

                }
                else if(v1>=40 && v1<65)
                {
                    sec.setTextColor(Color.parseColor("#4e5457"));
                    secpic.setImageResource(R.mipmap.ic_blue_dot);
//                    vTwo.setVisibility(View.GONE);

                }
                else if(v1>=65 && v1<80)
                {
                    thir.setTextColor(Color.parseColor("#4e5457"));
                    thirpic.setImageResource(R.mipmap.ic_blue_dot);
//                    vThree.setVisibility(View.GONE);

                }
                else if(v1>=80 && v1<90)
                {
                    fou.setTextColor(Color.parseColor("#4e5457"));
                    foupic.setImageResource(R.mipmap.ic_blue_dot);
//                    vFour.setVisibility(View.GONE);

                }
                else if(v1>=90 && v1<100)
                {
                    fif.setTextColor(Color.parseColor("#4e5457"));
                    fifthpic.setImageResource(R.mipmap.ic_blue_dot);
//                    vFive.setVisibility(View.GONE);
                }
            }

            @Override
            public void onSeriesItemDisplayProgress(float v) {

            }
        });

        arcView.addEvent(new DecoEvent.Builder(DecoEvent.EventType.EVENT_SHOW, true)
                .setDelay(0)
                .setDuration(0)
                .setListener(new DecoEvent.ExecuteEventListener() {
                    @Override
                    public void onEventStart(DecoEvent decoEvent) {
//                        bottom.setText("");
//                        top.setText("");
//                        centree.setText("Optimizing...");
                    }

                    @Override
                    public void onEventEnd(DecoEvent decoEvent) {

                    }

                })
                .build());

        arcView.addEvent(new DecoEvent.Builder(100).setIndex(series1Index2).setDelay(1000).setListener(new DecoEvent.ExecuteEventListener() {
            @Override
            public void onEventStart(DecoEvent decoEvent) {
//                bottom.setText("");
//                top.setText("");
//                centree.setText("Optimizing...");
            }

            @Override
            public void onEventEnd(DecoEvent decoEvent) {
//                bottom.setText("Found");
//                top.setText("Storage");
//                Random ran3 = new Random();
//                ramperct.setText(ran3.nextInt(40) + 20+"%");


                check=1;
                youDesirePermissionCode(ActivityApply.this);


                enablesall();
//                editor.putString("mode", "2");
//                editor.commit();
            }
        }).build());
    }

    public void enablesall()
    {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter.isEnabled()) {
            mBluetoothAdapter.disable();
        }

        WifiManager wifiManager = (WifiManager) getApplication().getApplicationContext().getSystemService(Context.WIFI_SERVICE);


        boolean wifiEnabled = wifiManager.isWifiEnabled();
        if(wifiEnabled)
        {
            wifiManager.setWifiEnabled(false);
        }

//        setAutoOrientationEnabled(getApplicationContext(), false);
//
//        Settings.System.putInt(this.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, 20);
//
//        ContentResolver.setMasterSyncAutomatically(false);


    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }

    @SuppressLint("NewApi")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && Settings.System.canWrite(this)){
            Log.d("TAG", "CODE_WRITE_SETTINGS_PERMISSION success");


//            Toast.makeText(getApplicationContext(),"onActivityResult",Toast.LENGTH_LONG).show();
            //do your code
            CompSaverActivity.setAutoOrientationEnabled(getApplicationContext(), false);

            Settings.System.putInt(this.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, 20);

            ContentResolver.setMasterSyncAutomatically(false);


            Intent i=new Intent(ActivityApply.this, BlackSaverActivity.class);
            startActivity(i);
            finish();
        }
    }

    public void youDesirePermissionCode(Activity context){
        boolean permission;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            permission = Settings.System.canWrite(context);
        } else {
            permission = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_SETTINGS) == PackageManager.PERMISSION_GRANTED;
//            Settings.System.putInt(context.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, 30);
//            setAutoOrientationEnabled(context, false);
        }
        if (permission) {
            //do your code
            CompSaverActivity.setAutoOrientationEnabled(getApplicationContext(), false);

            Settings.System.putInt(this.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, 20);

            ContentResolver.setMasterSyncAutomatically(false);


            Intent i=new Intent(ActivityApply.this, BlackSaverActivity.class);
            startActivity(i);
            finish();

        }  else {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
                intent.setData(Uri.parse("package:" + context.getPackageName()));
                context.startActivityForResult(intent, 1);
            } else {
                ActivityCompat.requestPermissions(context, new String[]{Manifest.permission.WRITE_SETTINGS}, 1);
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //do your code

//            Toast.makeText(getApplicationContext(),"onRequestPermissionsResult",Toast.LENGTH_LONG).show();

            CompSaverActivity.setAutoOrientationEnabled(getApplicationContext(), false);

            Settings.System.putInt(this.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, 20);

            ContentResolver.setMasterSyncAutomatically(false);


            Intent i=new Intent(ActivityApply.this, BlackSaverActivity.class);
            startActivity(i);
            finish();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(check==1)
        {
            try
            {
                CompSaverActivity.setAutoOrientationEnabled(getApplicationContext(), false);

                Settings.System.putInt(this.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, 20);

                ContentResolver.setMasterSyncAutomatically(false);

            }
            catch(Exception e)
            {
                Intent i=new Intent(ActivityApply.this, BlackSaverActivity.class);
                startActivity(i);
                finish();
            }

            Intent i=new Intent(ActivityApply.this, BlackSaverActivity.class);
            startActivity(i);
            finish();
        }
    }
}

package com.junk.removal.junkremoval.memory.cleaner.actives;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import androidx.core.app.NotificationCompat;

import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import com.junk.removal.junkremoval.memory.cleaner.actives.secondary.SuperActivity;
import com.junk.removal.junkremoval.memory.cleaner.users_cust.SwipeAdapter;
import com.junk.removal.junkremoval.memory.cleaner.R;
import com.junk.removal.junkremoval.memory.cleaner.serves.NotifServ;
import com.junk.removal.junkremoval.memory.cleaner.obj.NotifInit;
import com.junk.removal.junkremoval.memory.cleaner.utils.AdsUtility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import static com.junk.removal.junkremoval.memory.cleaner.apps.Applic.isAdAllowed;

public class NotifsManageActivity extends SuperActivity {
    private boolean isExitingFromApp, endOfDetails;

    private TextView clear_notices;
    private ListView notices;
    private SharedPreferences sharedPreferences, configsPreferences;
    private ArrayList<NotifInit> notificationsList;

    public NotificationManagement allNotifications;

//    private InterstitialAd mInterstitialAd;
    private Timer interstitialAsLoad;

    private boolean adIsLoaded = false, exitToPreferences;
    private ImageView cancel, settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        overridePendingTransition(R.anim.fast_fadein, 0);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_notification_remote_control);

        try {
            isAdAllowed = !getSharedPreferences("waseem", Context.MODE_PRIVATE).getBoolean(getResources().getString(R.string.is_pay_completed), false);
        } catch (Exception e) {

        }

        configsPreferences = getSharedPreferences("Configs", MODE_PRIVATE);

        try {

            if (!checkNotificationEnabled()) {
                redirectToSecureSettings();
            }

            cancel = findViewById(R.id.cancel);
            settings = findViewById(R.id.settings);

            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getApplicationContext(), MenuActivity.class));

                    finish();
                }
            });

            settings.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    exitToPreferences = true;
                    startActivity(new Intent(getApplicationContext(), NotifsPreferencesActivity.class));
                }
            });

            try {
                setTitle("Notifications menu");

                notificationsList = getNotifications();
                allNotifications = new NotificationManagement(getApplicationContext(), notificationsList);

                clear_notices = findViewById(R.id.btn_clear_all_notice);
                notices = findViewById(R.id.results);
                notices.setAdapter(allNotifications);

                SwipeAdapter touchListener =
                        new SwipeAdapter(
                                notices,
                                new SwipeAdapter.DismissCallbacks() {
                                    @Override
                                    public boolean canDismiss(int position) {
                                        return true;
                                    }

                                    @Override
                                    public void onDismiss(ListView listView, int[] reverseSortedPositions) {
                                        for (int position : reverseSortedPositions) {

                                            allNotifications.remove(position);
                                            allNotifications.notifyDataSetChanged();

                                        }


                                    }
                                });

                notices.setOnTouchListener(touchListener);
                notices.setOnScrollListener(touchListener.makeScrollListener());
                notices.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        if (!notificationsList.get(position).getMessageIntent().equals("")) {
                            try {
                                Intent appLaunch = getPackageManager().getLaunchIntentForPackage(notificationsList.get(position).getMessageIntent());
                                startActivity(appLaunch);
                            } catch (Exception e) {

                            }
                        }
                    }
                });
            } catch (Exception e) {
                Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
            }

            if (notificationsList.size() <= 0) {
                clear_notices.setEnabled(false);
                clear_notices.setBackgroundColor(getResources().getColor(R.color.colorinactive));
            }

            try {
                clear_notices.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        switch (event.getAction()) {
                            case MotionEvent.ACTION_DOWN:
                                clear_notices.setBackgroundColor(getResources().getColor(R.color.yellow));
                                break;
                            case MotionEvent.ACTION_UP:
                                clear_notices.setBackgroundColor(getResources().getColor(R.color.colortextred));
                                break;
                        }

                        return false;
                    }
                });
            } catch (Exception e) {

            }

            clear_notices.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteAllNotificatonsList();
                }
            });

            startService(new Intent(getApplicationContext(), NotifServ.class));

            if (notificationsList.size() == 0) {
                setContentView(R.layout.not_notifs_list);

                cancel = findViewById(R.id.cancel);
                settings = findViewById(R.id.settings);

                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(getApplicationContext(), MenuActivity.class));

                        finish();
                    }
                });

                settings.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(getApplicationContext(), NotifsPreferencesActivity.class));
                    }
                });
            }
        } catch (Exception e) {}
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (isExitingFromApp && !endOfDetails && !exitToPreferences) {
//            mInterstitialAd.setAdListener(new AdListener() {
//                @Override
//                public void onAdLoaded() {
//                    mInterstitialAd.show ();
//                }
//
//                @Override
//                public void onAdFailedToLoad(int i) {
//                    super.onAdFailedToLoad(i);
//                }
//            });

            startActivity (new Intent (getApplicationContext (), InitialScreenActivity.class));

            finish();
        }

        exitToPreferences = false;
    }

    class NotificationManagement extends BaseAdapter {
        private final LayoutInflater mLayoutInflater;
        private ArrayList<NotifInit> arrayMyMatches;

        public NotificationManagement (Context ctx, ArrayList<NotifInit> arr) {
            mLayoutInflater = LayoutInflater.from(ctx);
            setArrayMyData(arr);
        }
        public ArrayList<NotifInit> getArrayMyData() {
            return arrayMyMatches;
        }

        public void setArrayMyData(ArrayList<NotifInit> arrayMyData) {
            this.arrayMyMatches = arrayMyData;
        }
        public int getCount () {
            return arrayMyMatches.size();
        }

        @Override
        public NotifInit getItem (int position) {
            NotifInit app = arrayMyMatches.get(position);

            return app;
        }

        public void remove(int position) {
            try {
                if (arrayMyMatches.size() <= 1)
                    deleteAllNotificatonsList();
                else {
                    arrayMyMatches.remove(position);
                    this.notifyDataSetChanged();

                    sharedPreferences = getSharedPreferences(getString(R.string.APPS_NOTICES), MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();

                    if (sharedPreferences.contains(arrayMyMatches.get(position).getNoticeKey()))
                        editor.remove(arrayMyMatches.get(position).getNoticeKey());

                    editor.apply();
                }
            } catch (Exception e) {
            }
        }

        public long getItemId (int position) {
            return position;
        }

        public void refuse (ArrayList<NotifInit> apps) {
            arrayMyMatches.clear ();
            arrayMyMatches.addAll (apps);
            notifyDataSetChanged();
        }

        //получение элемента ListView и его отправка в активность данных

        public View getView(final int position, View convertView, ViewGroup parent) {
            try {
                if (convertView == null) {
                    LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = inflater.inflate(R.layout.notification_adapter_row, null, true);
                }

                ImageView notif_icon = convertView.findViewById(R.id.notif_icon);
                TextView notif_title = convertView.findViewById(R.id.notif_title);
                TextView notif_message = convertView.findViewById(R.id.notif_message);
                TextView notif_time = convertView.findViewById(R.id.notif_time);
                TextView notif_date = convertView.findViewById(R.id.notif_date);

                notif_icon.setImageBitmap(arrayMyMatches.get(position).getApp_image());
                notif_title.setText (arrayMyMatches.get(position).getTitle());
                notif_message.setText (arrayMyMatches.get(position).getMessage());
                notif_time.setText (arrayMyMatches.get(position).getDate().get (Calendar.HOUR_OF_DAY)
                        + ":" + arrayMyMatches.get(position).getDate().get (Calendar.MINUTE));
                notif_time.setTextColor(Color.CYAN);
                notif_date.setText (arrayMyMatches.get(position).getDate().get(Calendar.DAY_OF_MONTH) + "."
                        + arrayMyMatches.get(position).getDate().get(Calendar.MONTH) + "."
                        + arrayMyMatches.get(position).getDate().get(Calendar.YEAR));
                notif_date.setTextColor(Color.BLACK);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
            } finally {
                return convertView;
            }
        }
    }

    public ArrayList<NotifInit> getNotifications () {
        PackageManager packageManager = getPackageManager();
        Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);

        List<ResolveInfo> appList = packageManager.queryIntentActivities(mainIntent, 0);
        Collections.sort(appList, new ResolveInfo.DisplayNameComparator(packageManager));

        sharedPreferences = getSharedPreferences(getString(R.string.APPS_NOTICES), MODE_PRIVATE);
        HashMap<String, String> notifications = (HashMap<String, String>) sharedPreferences.getAll();

        ArrayList<NotifInit> notificationsList = new ArrayList<>();

        for (Map.Entry<String, String> notification : notifications.entrySet()) {

            String[] compressNotif = notification.getValue().split("///");

            String packageName = compressNotif[4];
            String title = compressNotif[1];
            String message = compressNotif[2];
            String time = compressNotif[3];
            String date = compressNotif[3];
            String pendingIntent = "";

            if (compressNotif.length >= 6)
                pendingIntent = compressNotif[5];

            for (ResolveInfo appInfo : appList) {
                if (appInfo.activityInfo.packageName.equals(packageName)) {
                    Bitmap icon = loadImageFromStorage(compressNotif[0], notification.getKey());

                    NotifInit notify = null;

                    if (icon != null) {
                        notify = new NotifInit(notification.getKey(), icon,
                                title, message, time, date, pendingIntent);
                    } else {
                        icon = drawableToBitmap(appInfo.loadIcon(packageManager));

                        if (icon != null)
                            notify = new NotifInit(notification.getKey(), icon,
                                    title, message, time, date, pendingIntent);
                    }

                    notificationsList.add(notify);
                }

                Log.e ("TAG_PACKAGE", appInfo.activityInfo.packageName);
            }
        }

        return notificationsList;
    }

    private void deleteAllNotificatonsList () {
        setContentView(R.layout.loading_screen);

        ProgressBar progressBar = findViewById(R.id.progressBar);

        if (progressBar != null)
            progressBar.getIndeterminateDrawable()
                    .setColorFilter(getResources().getColor(R.color.yellow), PorterDuff.Mode.SRC_IN);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.status_bar_color));
        }

        setAnotherScreenCast();

        AdsUtility.showInterstitial(NotifsManageActivity.this);

        notificationsList.clear();
        notices.deferNotifyDataSetChanged();

        sharedPreferences = getSharedPreferences(getString(R.string.APPS_NOTICES), MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

    private void setAnotherScreenCast () {
        endOfDetails = true;

        setContentView(R.layout.notifs_successly_cleaner);

        ImageView cancel = findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (getApplicationContext(), MenuActivity.class);
                startActivity (intent);
                finish();
            }
        });

        ImageView settings = findViewById(R.id.settings);
        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exitToPreferences = true;
                startActivity (new Intent (getApplicationContext(), NotifsPreferencesActivity.class));
            }
        });

        final TextView continueProtect = findViewById(R.id.continue_btn);

        continueProtect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (getApplicationContext(), MenuActivity.class);
                startActivity (intent);
                finish();
            }
        });
    }

    //Load adMob
    private void waitInterstitialAsLoad() {

        interstitialAsLoad = new Timer();
        final int[] timer = {0};

        interstitialAsLoad.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        if (isAdAllowed) {
                            if (!adIsLoaded) {
                                adIsLoaded = true;

                                setAnotherScreenCast();

                                AdsUtility.showInterstitial(NotifsManageActivity.this);

                                interstitialAsLoad.cancel();
                            }
                        } else if (timer[0] >= 16) {
                            setAnotherScreenCast();

                            interstitialAsLoad.cancel();
                        }
                        timer[0]++;
                    }
                });
            }
        }, 0, 500);
    }

    //check notification access setting is enabled or not
    public boolean checkNotificationEnabled() {
        try{
            return Settings.Secure.getString(NotifsManageActivity.this.getContentResolver(),
                    "enabled_notification_listeners").contains(getApplicationContext().getPackageName());

        }catch(Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static Bitmap drawableToBitmap (Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if(bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if(drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    private Bitmap loadImageFromStorage(String path, String key)
    {
        try {
            File f=new File(path, key);
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));

            return b;
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    public void redirectToSecureSettings() {
        Toast.makeText(getApplicationContext(), "You should allow this app to use notification control before using",
                Toast.LENGTH_LONG).show();

        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_NOTIFICATION_LISTENER_SETTINGS);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void sendUserNotification () {
        PackageManager packageManager = getPackageManager();
        Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);

        List<ResolveInfo> appList = packageManager.queryIntentActivities(mainIntent, 0);
        Collections.sort(appList, new ResolveInfo.DisplayNameComparator(packageManager));

        sharedPreferences = getSharedPreferences(getString(R.string.APPS_NOTICES), MODE_PRIVATE);
        HashMap<String, String> notifications = (HashMap<String, String>) sharedPreferences.getAll();

        int NOTIFY_ID = 1;

        if (notifications.size() > 0) {
            Intent notificationIntent = new Intent(this, NotifsManageActivity.class);
            PendingIntent contentIntent = PendingIntent.getActivity(this,
                    0, notificationIntent,
                    PendingIntent.FLAG_CANCEL_CURRENT);

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getApplicationContext());

            RemoteViews remoteViews = new RemoteViews(getPackageName(), R.layout.notification_cleaner_notif);
            remoteViews.setTextViewText(R.id.wanring_title_num, String.valueOf (notifications.size()));
            remoteViews.setOnClickPendingIntent(R.id.clear_notifs, contentIntent);

            try {
                switch (notifications.size()) {
                    case 1:
                        ArrayList<Bitmap> imagesFor1 = getIconForApp(notifications, appList, packageManager);

                        if (imagesFor1.get(0) != null)
                            remoteViews.setImageViewBitmap(R.id.img_app_slot_1, imagesFor1.get(0));

                        remoteViews.setViewVisibility(R.id.img_app_slot_1, View.VISIBLE);
                        break;
                    case 2:
                        ArrayList<Bitmap> imagesFor2 = getIconForApp(notifications, appList, packageManager);

                        if (imagesFor2.get(0) != null)
                            remoteViews.setImageViewBitmap(R.id.img_app_slot_1, imagesFor2.get(0));
                        if (imagesFor2.get(1) != null)
                            remoteViews.setImageViewBitmap(R.id.img_app_slot_2, imagesFor2.get(1));

                        remoteViews.setViewVisibility(R.id.img_app_slot_1, View.VISIBLE);
                        remoteViews.setViewVisibility(R.id.img_app_slot_2, View.VISIBLE);
                        break;
                    case 3:
                        ArrayList<Bitmap> imagesFor3 = getIconForApp(notifications, appList, packageManager);

                        if (imagesFor3.get(0) != null)
                            remoteViews.setImageViewBitmap(R.id.img_app_slot_1, imagesFor3.get(0));
                        if (imagesFor3.get(1) != null)
                            remoteViews.setImageViewBitmap(R.id.img_app_slot_2, imagesFor3.get(1));
                        if (imagesFor3.get(2) != null)
                            remoteViews.setImageViewBitmap(R.id.img_app_slot_3, imagesFor3.get(2));

                        remoteViews.setViewVisibility(R.id.img_app_slot_1, View.VISIBLE);
                        remoteViews.setViewVisibility(R.id.img_app_slot_2, View.VISIBLE);
                        remoteViews.setViewVisibility(R.id.img_app_slot_3, View.VISIBLE);
                        break;
                    default:
                        ArrayList<Bitmap> imagesForDefault = getIconForApp(notifications, appList, packageManager);

                        if (imagesForDefault.get(0) != null)
                            remoteViews.setImageViewBitmap(R.id.img_app_slot_1, imagesForDefault.get(0));
                        if (imagesForDefault.get(1) != null)
                            remoteViews.setImageViewBitmap(R.id.img_app_slot_2, imagesForDefault.get(1));
                        if (imagesForDefault.get(2) != null)
                            remoteViews.setImageViewBitmap(R.id.img_app_slot_3, imagesForDefault.get(2));

                        remoteViews.setViewVisibility(R.id.img_app_slot_1, View.VISIBLE);
                        remoteViews.setViewVisibility(R.id.img_app_slot_2, View.VISIBLE);
                        remoteViews.setViewVisibility(R.id.img_app_slot_3, View.VISIBLE);
                        remoteViews.setViewVisibility(R.id.img_app_slot_4, View.VISIBLE);
                }
            } catch (Exception e) {}

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                String CHANNEL_ID = "my_channel_01";// The id of the channel.
                CharSequence name = getString(R.string.channel_name);// The user-visible name of the channel.
                int importance = NotificationManager.IMPORTANCE_HIGH;
                NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);

                notificationManager.createNotificationChannel(mChannel);

                notificationBuilder.setChannelId(CHANNEL_ID);
            }

            notificationBuilder.setContentIntent(contentIntent)
                    .setDefaults(NotificationCompat.DEFAULT_ALL)
                    // обязательные настройки
                    .setSmallIcon(R.drawable.warning)
                    .setPriority(Notification.PRIORITY_HIGH)
                    .setShowWhen(false)
                    .setSubText("")
                    .setContent(remoteViews)
                    .setNumber(notifications.size())
//                    .setContentText("Просмотрите конфиденциальность") // Текст уведомления
                    // необязательные настройки
//                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.warning)) // большая
                    .setTicker("Ненужные уведомления: " + notifications.size())
                    .setWhen(System.currentTimeMillis())
//                    .setContentTitle("Ненужные уведомления: " + notifications.size())
                    .setLights(Color.argb(255, 252, 148, 0), 1500, 1000)
                    .setDefaults(Notification.FLAG_SHOW_LIGHTS
                            | Notification.DEFAULT_VIBRATE
                            | Notification.FLAG_NO_CLEAR
                            | Notification.FLAG_FOREGROUND_SERVICE)
                    .setOngoing(true)
                    .setPriority(NotificationCompat.PRIORITY_MAX)
                    .setOnlyAlertOnce(true);

            if (notifications.size() < 1) {
                notificationBuilder.setContentTitle("У вас нет новых уведомлений!");
                notificationBuilder.setContentText("Конфиденциальность под контролем...");
                notificationBuilder.setTicker("Ненужные уведомления отсутствуют!");
            }

            if (configsPreferences.getBoolean("isRequireNotifsControl", true))
                notificationManager.notify(NOTIFY_ID, notificationBuilder.build());
            else
                notificationManager.cancel(NOTIFY_ID);
        } else {
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(NOTIFY_ID);
        }
    }

    private ArrayList<Bitmap> getIconForApp (HashMap<String, String> notifications, List<ResolveInfo> appList, PackageManager packageManager) {
        ArrayList<Bitmap> images = new ArrayList<>();

        for (Map.Entry<String, String> notification : notifications.entrySet()) {

            String[] compressNotif = notification.getValue().split("///");

            String packageName = compressNotif[4];

            for (ResolveInfo appInfo : appList) {
                if (appInfo.activityInfo.packageName.equals(packageName)) {
                    Bitmap icon = loadImageFromStorage(compressNotif[0], notification.getKey());

                    if (icon == null) {
                        icon = drawableToBitmap(appInfo.loadIcon(packageManager));

                        images.add(icon);
                    }
                }
            }
        }

        return images;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.gear_menu,menu);//insert the name of the menu file

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                startActivity (new Intent (getApplicationContext(), MenuActivity.class));

                finish ();
                return true;
            case R.id.settings_notify:
                startActivity (new Intent (getApplicationContext(), NotifsPreferencesActivity.class));

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();

        isExitingFromApp = true;
    }

    @Override
    public boolean onKeyDown(final int pKeyCode, final KeyEvent pEvent) {
        if(pKeyCode == KeyEvent.KEYCODE_BACK && pEvent.getAction() == KeyEvent.ACTION_DOWN) {
            sendUserNotification();

            startActivity (new Intent (getApplicationContext(), MenuActivity.class));
            finish ();

            return true;
        } else {
            return super.onKeyDown(pKeyCode, pEvent);
        }
    }

    @Override
    protected void onDestroy() {
        sendUserNotification();

        super.onDestroy();
    }
}
package com.junk.removal.junkremoval.memory.cleaner.actives;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.junk.removal.junkremoval.memory.cleaner.R;
import com.junk.removal.junkremoval.memory.cleaner.actives.secondary.SuperActivity;
import com.junk.removal.junkremoval.memory.cleaner.obj.FileObj;
import com.junk.removal.junkremoval.memory.cleaner.obj.AppInit;
import com.junk.removal.junkremoval.memory.cleaner.utils.AdsUtility;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import static com.junk.removal.junkremoval.memory.cleaner.actives.secondary.FragmentsActivity.RC_HANDLE_PERMISSIONA_ALL;
import static com.junk.removal.junkremoval.memory.cleaner.apps.Applic.isAdAllowed;

public class JunksDataRemovingActivity extends SuperActivity {

    private TextView cleanBtn;
    private CheckBox systemCacheCheck, appsCacheCheck, emptyFoldersCheck, miniaturesCheck, downloadsCheck;
    private TextView systemCacheText, appsCacheText, emptyFoldersText, miniaturesText, downloadsText;
    private ImageView systemCacheExpand, appsCacheExpand, emptyFoldersExpand, miniaturesExpand, downloadsExpand;
    private ListView systemCacheList, appsCacheList, emptyFoldersList, miniaturesList, downloadsList;

    private float systemCacheSize, appsCacheSize, emptyFoldersSize, miniaturesSize, downloadsSize;

    private ArrayList<FileObj> emptyFoldersArray, miniaturesArray, downloadsArray;

    private EmptyFoldersFinderTask emptyFoldersFinderTask;

    private ArrayList<AppInit> apps;
    private AppsCacheTask appsCacheTask;

//    private InterstitialAd mInterstitialAd;

    private float allSize;

    List<ApplicationInfo> packages;

    private final ArrayList itemsPackage = new ArrayList();
    private final ArrayList itemsSize = new ArrayList();

    ArrayList cacheFiles = new ArrayList();

    private float sizeToDelete;

    private LinearLayout progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        overridePendingTransition(R.anim.fast_fadein, 0);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        try {
            ActionBar actionBar = getSupportActionBar();
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

//        if (isAdAllowed) {
////            mInterstitialAd = new InterstitialAd(getApplicationContext());
////
////            mInterstitialAd.setAdUnitId(getResources().getString(R.string.cache_remover_screen));
////            AdRequest adRequestInter = new AdRequest.Builder().build();
////
////            mInterstitialAd.loadAd(adRequestInter);
//
//            mInterstitialAd = new InterstitialAd(this, getString(R.string.facebook_interstitial_ad_id));
//            mInterstitialAd.loadAd(mInterstitialAd.buildLoadAdConfig().build());
//        }

        setContentView(R.layout.cache_remover_content);

        try {
            isAdAllowed = !getSharedPreferences("waseem", Context.MODE_PRIVATE).getBoolean(getResources().getString(R.string.is_pay_completed), false);
        } catch (Exception e) {

        }

        if (!(ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) {

            final String [] permission = new String[]{
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE};

            if(!hasPermissions(this, permission)){
                ActivityCompat.requestPermissions(this, permission, RC_HANDLE_PERMISSIONA_ALL);

                return;
            }
        } else {
            ImageView backBtn = findViewById(R.id.exit_btn);
            backBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });

            cleanBtn = findViewById(R.id.clean_all);

            systemCacheCheck = findViewById(R.id.system_cache_check);
            appsCacheCheck = findViewById(R.id.apps_cache_check);
            emptyFoldersCheck = findViewById(R.id.empty_folders_check);
            miniaturesCheck = findViewById(R.id.miniatures_check);
            downloadsCheck = findViewById(R.id.downloads_check);

            systemCacheText = findViewById(R.id.system_cache_text);
            appsCacheText = findViewById(R.id.apps_cache_text);
            emptyFoldersText = findViewById(R.id.empty_folders_text);
            miniaturesText = findViewById(R.id.miniatures_text);
            downloadsText = findViewById(R.id.downloads_text);

            systemCacheExpand = findViewById(R.id.system_cache_expand);
            appsCacheExpand = findViewById(R.id.apps_cache_expand);
            emptyFoldersExpand = findViewById(R.id.empty_folders_expand);
            miniaturesExpand = findViewById(R.id.miniatures_expand);
            downloadsExpand = findViewById(R.id.downloads_expand);

            systemCacheList = findViewById(R.id.system_cache_list);
            appsCacheList = findViewById(R.id.apps_cache_list);
            emptyFoldersList = findViewById(R.id.empty_folders_list);
            miniaturesList = findViewById(R.id.miniatures_list);
            downloadsList = findViewById(R.id.downloads_list);

            systemCacheCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b)
                        sizeToDelete += systemCacheSize;
                    else
                        sizeToDelete -= systemCacheSize;

                    cleanBtn.setText(getString(R.string.clean) + " " + (int) sizeToDelete + " " + getString(R.string.mb));
                }
            });

            appsCacheCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b)
                        sizeToDelete += appsCacheSize;
                    else
                        sizeToDelete -= appsCacheSize;

                    cleanBtn.setText(getString(R.string.clean) + " " + (int) sizeToDelete + " " + getString(R.string.mb));
                }
            });

            emptyFoldersCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b)
                        sizeToDelete += emptyFoldersSize / 1024 / 1024;
                    else
                        sizeToDelete -= emptyFoldersSize / 1024 / 1024;

                    cleanBtn.setText(getString(R.string.clean) + " " + (int) sizeToDelete + " " + getString(R.string.mb));
                }
            });

            miniaturesCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b)
                        sizeToDelete += miniaturesSize;
                    else
                        sizeToDelete -= miniaturesSize;

                    cleanBtn.setText(getString(R.string.clean) + " " + (int) sizeToDelete + " " + getString(R.string.mb));
                }
            });

            downloadsCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b)
                        sizeToDelete += downloadsSize;
                    else
                        sizeToDelete -= downloadsSize;

                    cleanBtn.setText(getString(R.string.clean) + " " + (int) sizeToDelete + " " + getString(R.string.mb));
                }
            });

            systemCacheExpand.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (systemCacheList.getVisibility() == View.GONE) {
                        closeAll();

                        systemCacheList.setVisibility(View.VISIBLE);
                        systemCacheExpand.setRotation(180);
                    } else {
                        systemCacheList.setVisibility(View.GONE);
                        systemCacheExpand.setRotation(0);
                    }
                }
            });

            appsCacheExpand.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (appsCacheList.getVisibility() == View.GONE) {
                        closeAll();

                        appsCacheList.setVisibility(View.VISIBLE);
                        appsCacheExpand.setRotation(180);
                    } else {
                        appsCacheList.setVisibility(View.GONE);
                        appsCacheExpand.setRotation(0);
                    }
                }
            });

            emptyFoldersExpand.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (emptyFoldersList.getVisibility() == View.GONE) {
                        closeAll();

                        emptyFoldersList.setVisibility(View.VISIBLE);
                        emptyFoldersExpand.setRotation(180);
                    } else {
                        emptyFoldersList.setVisibility(View.GONE);
                        emptyFoldersExpand.setRotation(0);
                    }
                }
            });

            miniaturesExpand.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (miniaturesList.getVisibility() == View.GONE) {
                        closeAll();

                        miniaturesList.setVisibility(View.VISIBLE);
                        miniaturesExpand.setRotation(180);
                    } else {
                        miniaturesList.setVisibility(View.GONE);
                        miniaturesExpand.setRotation(0);
                    }
                }
            });

            downloadsExpand.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (downloadsList.getVisibility() == View.GONE) {
                        closeAll();

                        downloadsList.setVisibility(View.VISIBLE);
                        downloadsExpand.setRotation(180);
                    } else {
                        downloadsList.setVisibility(View.GONE);
                        downloadsExpand.setRotation(0);
                    }
                }
            });

            cleanBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                if ((systemCacheCheck.isChecked() || appsCacheCheck.isChecked()) && apps != null)
                    try {
                        for (AppInit app : new ArrayList<AppInit>(apps))
                            deleteRecursive(new File(app.getAppDate()));
                    } catch (Exception e) {}

                try {
                    if (emptyFoldersCheck.isChecked() && emptyFoldersArray != null)
                        for (FileObj object : new ArrayList<FileObj>(emptyFoldersArray))
                            (new File(object.getPath())).delete();
                } catch (Exception e) {}

                if (miniaturesCheck.isChecked() && miniaturesArray != null)
                    for (FileObj object : miniaturesArray)
                        (new File(object.getPath())).delete();

                if (downloadsCheck.isChecked() && downloadsArray != null)
                    for (FileObj object : downloadsArray)
                        (new File(object.getPath())).delete();

                    findViewById(R.id.content).setVisibility(View.GONE);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            progressBar = findViewById(R.id.progressBar);
                            progressBar.setVisibility(View.GONE);
                        }
                    }, 1000);
                }
            });

            emptyFoldersFinderTask = new EmptyFoldersFinderTask();
            emptyFoldersFinderTask.execute();

            appsCacheTask = new AppsCacheTask();
            appsCacheTask.execute();
        }
    }

    private static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        try {
            if (requestCode == RC_HANDLE_PERMISSIONA_ALL) {

                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    finish();
                } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startActivity(new Intent(getApplicationContext(), JunksDataRemovingActivity.class));
                    finish();
                }
            }
        } catch (Exception e) {
        }
    }

    private void closeAll () {
        systemCacheList.setVisibility(View.GONE);
        appsCacheList.setVisibility(View.GONE);
        emptyFoldersList.setVisibility(View.GONE);
        miniaturesList.setVisibility(View.GONE);
        downloadsList.setVisibility(View.GONE);

        systemCacheExpand.setRotation(0);
        appsCacheExpand.setRotation(0);
        emptyFoldersExpand.setRotation(0);
        miniaturesExpand.setRotation(0);
        downloadsExpand.setRotation(0);
    }

    private class EmptyFoldersFinderTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            emptyFoldersArray = new ArrayList<>();
            miniaturesArray = new ArrayList<>();
            downloadsArray = new ArrayList<>();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            for (FileObj object : emptyFoldersArray)
                emptyFoldersSize += object.getSize();

            for (FileObj object : miniaturesArray)
                miniaturesSize += object.getSize();

            for (FileObj object : downloadsArray)
                downloadsSize += object.getSize();

            emptyFoldersText.setText(emptyFoldersSize + " " + getString(R.string.b));

            animateTextView(0, miniaturesSize, miniaturesText);
            animateTextView(0, downloadsSize, downloadsText);

            emptyFoldersList.setAdapter(new FileAdapter(getApplicationContext(), emptyFoldersArray));
            miniaturesList.setAdapter(new FileAdapter(getApplicationContext(), miniaturesArray));
            downloadsList.setAdapter(new FileAdapter(getApplicationContext(), downloadsArray));
        }

        @Override
        protected Void doInBackground(Void... voids) {
            checkEmptyFolders (Environment.getExternalStorageDirectory());
            checkThumbnails(Environment.getExternalStorageDirectory());
            checkDownloads(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS));

            return null;
        }

        private void checkEmptyFolders(File parentDir) {
            File[] files = parentDir.listFiles();

            try {
                if (files != null)
                    for (File file : files) {
                        if (file.isDirectory()) {
                            if (file.listFiles().length > 0)
                                checkEmptyFolders(file);
                            else {
                                FileObj fileObject = new FileObj(file.getName(), file.getPath(), 4);
                                fileObject.setType("folder");
                                emptyFoldersArray.add(fileObject);
                            }
                        }
                    }
            } catch (Exception e) {}
        }

        private void checkDownloads(File parentDir) {
            File[] files = parentDir.listFiles();

            try {
                if (files != null)
                    for (File file : files) {
                        if (file.isFile()) {
                            float size = file.length()/1024/1024;

                            FileObj fileObject = new FileObj(file.getName(), file.getPath(), size);
                            String extension = file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf("."));
                            fileObject.setType(extension);

                            downloadsArray.add(fileObject);
                        } else if (file.isDirectory())
                            checkDownloads(file);
                    }
            } catch (Exception e) {}
        }

        private void checkThumbnails(File parentDir) {
            File[] files = parentDir.listFiles();

            try {
                if (files != null)
                    for (File file : files) {
                        if (file.isFile()) {
                            String extension = file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf("."));

                            if (extension.contains("thumbnail")) {

                                float size = file.length() / 1024 / 1024;

                                FileObj fileObject = new FileObj(file.getName(), file.getPath(), size);
                                fileObject.setType(extension);

                                miniaturesArray.add(fileObject);
                            }
                        } else if (file.isDirectory())
                            checkThumbnails(file);
                    }
            } catch (Exception e) {}
        }
    }

    private class AppsCacheTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            Collections.sort(apps, new Comparator<AppInit>() {
                @Override
                public int compare(AppInit initApp, AppInit t1) {
                    if (initApp.getAppDateMillis() < t1.getAppDateMillis())
                        return 1;
                    else if (initApp.getAppDateMillis() > t1.getAppDateMillis())
                        return -1;

                    return 0;
                }
            });

            appsCacheSize = (((float) (Math.round(allSize * 10))) / 10);
            animateTextView(0, appsCacheSize, appsCacheText);
            appsCacheList.setAdapter(new AppsCacheAdapter(getApplicationContext(), apps));

            systemCacheSize = (((float) (Math.round(allSize * (Math.random() + 0.1) * 10))) / 10);

            animateTextView(0, systemCacheSize, systemCacheText);
            systemCacheList.setAdapter(new AppsCacheAdapter(getApplicationContext(), apps));

            animateTextView(0, (systemCacheSize + appsCacheSize + emptyFoldersSize + miniaturesSize + downloadsSize), findViewById(R.id.allSizeText));

            sizeToDelete = systemCacheSize + appsCacheSize + (emptyFoldersSize/1024/1025) + miniaturesSize;

            cleanBtn.setText(getString(R.string.clean) + " " + (int)sizeToDelete + " " + getString(R.string.mb));
        }

        @Override
        protected Void doInBackground(Void... voids) {
            apps = new ArrayList<>();

            PackageManager packageManager = getPackageManager();
            Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
            mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);

            List<ResolveInfo> appList = packageManager.queryIntentActivities(mainIntent, 0);
            Collections.sort(appList, new ResolveInfo.DisplayNameComparator(packageManager));

            for (int i = 0; i < appList.size(); i++) {
                if (appList.get(i).activityInfo.packageName.equals(getPackageName()))
                    continue;

                apps.add(new AppInit(appList.get(i).loadIcon(getPackageManager()),
                        (String) appList.get(i).loadLabel(getPackageManager()), appList.get(i).activityInfo.packageName, false));
                try {
                    ApplicationInfo applicationInfo = packageManager.getApplicationInfo(appList.get(i).activityInfo.packageName, 0);
                    File file = new File(applicationInfo.publicSourceDir);
                    apps.get(apps.size() - 1).setAppSize((int)(file.length()/(1024*1024)));

                    PackageInfo packageInfo = packageManager.getPackageInfo(applicationInfo.packageName, 0);
                    long installTimeInMilliseconds = packageInfo.firstInstallTime;

                    apps.get(apps.size() - 1).setAppDate(getInstallDate(installTimeInMilliseconds, applicationInfo.packageName));
                    apps.get(apps.size() - 1).setAppDateMillis(installTimeInMilliseconds);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
            }

            if (hasStoragePermission(1)) {
                final PackageManager pm = getPackageManager(); //ищем все пакеты на устройстве
                packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);

                //путь, по которому будут искаться папки с кэшем
                File externalStorageDirectory = Environment.getExternalStorageDirectory();
                File folder = new File(externalStorageDirectory.getAbsolutePath());

                //запускаем метод поиска папок

                checkFiles(folder);
            }

            ArrayList<AppInit> appsToDelete = new ArrayList();

            for (AppInit app : apps) {
                if (!itemsPackage.contains(app.getApp_package()))
                    appsToDelete.add(app);
                else {
                    app.setAppDate(cacheFiles.get(itemsPackage.indexOf(app.getApp_package())).toString());
                    app.setStorage(String.valueOf(Math.abs((float)itemsSize.get(itemsPackage.indexOf(app.getApp_package())))));
                    app.setStorageData((float)itemsSize.get(itemsPackage.indexOf(app.getApp_package())));
                    allSize += Math.abs(app.getStorageData());
                }
            }

            for (int i = 0; i < appsToDelete.size(); i++)
                apps.remove(appsToDelete.get(i));

            return null;
        }

        private String getInstallDate(long installTimeInMilliseconds, String packageName) {
            // install time is conveniently provided in milliseconds

            Date installDate = null;
            String installDateString = null;

            try {

                Calendar mydate = Calendar.getInstance();
                mydate.setTimeInMillis(installTimeInMilliseconds);

                installDateString  = (mydate.get(Calendar.DAY_OF_MONTH)+"."+mydate.get(Calendar.MONTH)+"."+mydate.get(Calendar.YEAR));
            }
            catch (Exception e) {
                // an error occurred, so display the Unix epoch
                installDate = new Date(0);
                installDateString = installDate.toString();
            }

            return installDateString;
        }

        //метод обработки всех папок на устройстве

        private void checkFiles(File parentDir) {
            File[] files = parentDir.listFiles();
            try {
                if (files != null)
                    for (File file : files)
                        //проверяем текущую папку
                        if (file.isDirectory()) {
                            //если папка = дириктория - проверяем ее на принадлежность к приложениям
                            for (ApplicationInfo packageInfo : packages) {
                                if (file.getName().equals(packageInfo.packageName)) { //если нашли такую - выполняем нужные с ней операции

                                    //добавляем новый элемент в список
                                    itemsPackage.add(file.getName());
                                    itemsSize.add((float) ((int) dirSize(file) * 100 / 1024 / 1024 + 10) / 100);
                                    cacheFiles.add(file.getAbsoluteFile());
                                }
                            }

                            //тут единственный момент - вызываем рекурсивно данную функцию, чтобы посмотреть также дочерние файлы
                            checkFiles(file);
                        }
            } catch (Exception e) {}
        }

        //метод, который проверяет размер папки

        private long dirSize(File dir) {

            if (dir.exists()) {
                long result = 0;
                File[] fileList = dir.listFiles();
                for(int i = 0; i < fileList.length; i++) {
                    // Recursive call if it's a directory
                    if(fileList[i].isDirectory()) {
                        result += dirSize(fileList [i]);
                    } else {
                        // Sum the file size in bytes
                        result += fileList[i].length();
                    }
                }
                return result; // return the file size
            }
            return 0;
        }

        //метод запроса разрешений

        private boolean hasStoragePermission(int requestCode) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.CLEAR_APP_CACHE}, requestCode);
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;
            }
        }
    }

    public void animateTextView(float initialValue, float finalValue, final TextView  textview) {

        ValueAnimator valueAnimator = ValueAnimator.ofFloat(initialValue, finalValue);
        valueAnimator.setDuration(1500);

        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {

                textview.setText((float)Math.round(((float)valueAnimator.getAnimatedValue())*10)/10 + " " + getString(R.string.mb));

            }
        });
        valueAnimator.start();
    }

    class FileAdapter extends BaseAdapter {
        private final LayoutInflater mLayoutInflater;
        private ArrayList<FileObj> arrayMyMatches;

        public FileAdapter (Context ctx, ArrayList<FileObj> arr) {
            mLayoutInflater = LayoutInflater.from(ctx);
            setArrayMyData(arr);
        }

        public void setArrayMyData(ArrayList<FileObj> arrayMyData) {
            this.arrayMyMatches = arrayMyData;
        }

        public boolean remove (int i) {
            arrayMyMatches.remove(i);

            return true;
        }

        public int getCount () {
            return arrayMyMatches.size();
        }

        public Object getItem (int position) {
            FileObj app = arrayMyMatches.get(position);

            return app;
        }

        public long getItemId (int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            try {
                if (convertView == null) {
                    LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = inflater.inflate(R.layout.cache_file_row, null, true);
                }

                ImageView app_image = convertView.findViewById(R.id.app_image);
                TextView app_name = convertView.findViewById(R.id.name);
                RelativeLayout removeBtn = convertView.findViewById(R.id.remove_btn);

                final TextView size = convertView.findViewById(R.id.size);
                size.setText(arrayMyMatches.get(position).getSize() + " MB");

                app_name.setText(arrayMyMatches.get(position).getName());

                if (arrayMyMatches.get(position).getType().equals("folder"))
                    app_image.setImageDrawable(getResources().getDrawable(R.drawable.folder_file));
                else
                    app_image.setImageDrawable(getResources().getDrawable(BigFilesActivity.getFileImageByExtension(arrayMyMatches.get(position).getType())));

                removeBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //путь к определенной папке
                        File dir = new File(arrayMyMatches.get(position).getPath());

                        try {
                            dir.delete();
                        } catch (Exception e) {}

                        emptyFoldersSize -= arrayMyMatches.get(position).getSize();

                        arrayMyMatches.remove(position);
                        notifyDataSetChanged();
                    }
                });

            } catch (Exception e) {

            } finally {
                return convertView;
            }
        }
    }

    class AppsCacheAdapter extends BaseAdapter {
        private final LayoutInflater mLayoutInflater;
        private ArrayList<AppInit> arrayMyMatches;

        public AppsCacheAdapter(Context ctx, ArrayList<AppInit> arr) {
            mLayoutInflater = LayoutInflater.from(ctx);
            setArrayMyData(arr);
        }

        public void setArrayMyData(ArrayList<AppInit> arrayMyData) {
            this.arrayMyMatches = arrayMyData;
        }

        public boolean remove(int i) {
            arrayMyMatches.remove(i);

            return true;
        }

        public int getCount() {
            return arrayMyMatches.size();
        }

        public Object getItem(int position) {
            AppInit app = arrayMyMatches.get(position);

            return app;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            try {
                if (convertView == null) {
                    LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = inflater.inflate(R.layout.cache_file_row, null, true);
                }

                ImageView app_image = convertView.findViewById(R.id.app_image);
                TextView app_name = convertView.findViewById(R.id.name);
                RelativeLayout removeBtn = convertView.findViewById(R.id.remove_btn);

                TextView size = convertView.findViewById(R.id.size);

                app_image.setImageDrawable(arrayMyMatches.get(position).getApp_image());
                app_name.setText(arrayMyMatches.get(position).getApp_name());

                size.setText(arrayMyMatches.get(position).getStorage() + " MB");

                removeBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //путь к определенной папке
                        File dir = new File(apps.get(position).getAppDate());

                        //удаляем папку
                        deleteRecursive(dir);

                        allSize -= apps.get(position).getStorageData();

                        //обновляем список
                        itemsPackage.remove(position);
                        itemsSize.remove(position);
                        cacheFiles.remove(position);
                        apps.remove(position);
                        notifyDataSetChanged();
                    }
                });

            } catch (Exception e) {

            } finally {
                return convertView;
            }
        }
    }

    void deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child);

        fileOrDirectory.delete();
    }

    @Override
    public boolean onKeyDown(final int pKeyCode, final KeyEvent pEvent) {
        if(pKeyCode == KeyEvent.KEYCODE_BACK && pEvent.getAction() == KeyEvent.ACTION_DOWN) {
            onBackPressed();

            return true;
        } else {
            return super.onKeyDown(pKeyCode, pEvent);
        }
    }

    @Override
    public void onBackPressed() {
        AdsUtility.showMrec(R.id.exit_banner, this);

        RelativeLayout exitDialog = findViewById(R.id.exit_dialog);
        exitDialog.setVisibility(View.VISIBLE);

        findViewById(R.id.yes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), MenuActivity.class));

                AdsUtility.showInterstitial(JunksDataRemovingActivity.this);

                finish();
            }
        });

        findViewById(R.id.no).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exitDialog.setVisibility(View.GONE);
            }
        });
    }
}
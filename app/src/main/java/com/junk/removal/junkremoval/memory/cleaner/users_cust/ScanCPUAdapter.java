package com.junk.removal.junkremoval.memory.cleaner.users_cust;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.junk.removal.junkremoval.memory.cleaner.R;
import com.junk.removal.junkremoval.memory.cleaner.obj.AppFile;

import java.util.List;

public class ScanCPUAdapter extends RecyclerView.Adapter<ScanCPUAdapter.MyViewHolder> {


    /// Get List of AppFile Causing Junk Files

    public List<AppFile> apps;

    public ScanCPUAdapter(List<AppFile> getapps) {
        apps = getapps;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.scan_cpu_apps, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        AppFile app = apps.get(position);
        holder.size.setText("");
        holder.image.setImageDrawable(app.getImage());
    }

    @Override
    public int getItemCount() {
        return apps.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView size;
        ImageView image;

        public MyViewHolder(View view) {
            super(view);
            size = view.findViewById(R.id.apptext);
            image = view.findViewById(R.id.appimage);
        }
    }
}

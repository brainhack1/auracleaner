package com.junk.removal.junkremoval.memory.cleaner.actives.secondary;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.OvershootInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.junk.removal.junkremoval.memory.cleaner.R;
import com.junk.removal.junkremoval.memory.cleaner.actives.AfterCleaningActivity;
import com.junk.removal.junkremoval.memory.cleaner.obj.AppFile;
import com.junk.removal.junkremoval.memory.cleaner.users_cust.ScanCPUAdapter;
import com.junk.removal.junkremoval.memory.cleaner.fragments.Frag_CPU_cooler;
import com.junk.removal.junkremoval.memory.cleaner.utils.AdsUtility;

import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;

import static com.junk.removal.junkremoval.memory.cleaner.apps.Applic.isAdAllowed;

public class ScannerCPUActivity extends SuperActivity {
    private static final String TAG = "ScannerCPUActivity";

    ///// Scan Cpu For Power Consuming and Over heating AppFile

    ImageView scanner,img_animation,ivCompltecheck,shadowCpu;
    ScanCPUAdapter mAdapter;
    RecyclerView recyclerView;
    List<AppFile> app=null;
    TextView cooledcpu;
    RelativeLayout rel;

//    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.cpu_scanner);
        scanner = findViewById(R.id.scann);
        cooledcpu= findViewById(R.id.cpucooler);
        img_animation = findViewById(R.id.heart);
        rel= findViewById(R.id.rel);
        ivCompltecheck= findViewById(R.id.iv_completecheck);
        shadowCpu= findViewById(R.id.shadowcpu);
        app=new ArrayList<>();

        RotateAnimation rotate = new RotateAnimation(0, 360, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(1500);
        rotate.setRepeatCount(3);
        rotate.setInterpolator(new LinearInterpolator());
        scanner.startAnimation(rotate);

        TranslateAnimation animation = new TranslateAnimation(0.0f, 1000.0f, 0.0f, 0.0f);          //  new TranslateAnimation(xFrom,xTo, yFrom,yTo)
        animation.setDuration(5000);  // animation duration
        animation.setRepeatCount(0);
        animation.setInterpolator(new LinearInterpolator());// animation repeat count
//        animation.setRepeatMode(2);   // repeat animation (left to right, right to left )
        animation.setFillAfter(true);

        img_animation.startAnimation(animation);

        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                img_animation.setImageResource(0);
                img_animation.setBackgroundResource(0);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        recyclerView = findViewById(R.id.recycler_view);

        recyclerView.setItemAnimator(new SlideInUpAnimator());

        mAdapter = new ScanCPUAdapter(Frag_CPU_cooler.apps);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new SlideInUpAnimator(new OvershootInterpolator(1f)));
        recyclerView.computeHorizontalScrollExtent();
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

        try {
            final Handler handler1 = new Handler();
            handler1.postDelayed(new Runnable() {
                @Override
                public void run() {
                    add("Limit Brightness Upto 80%", 0);


                }
            }, 0);

            final Handler handler2 = new Handler();
            handler2.postDelayed(new Runnable() {
                @Override
                public void run() {
                    remove(0);
                    add("Decrease Device Performance", 1);


                }
            }, 900);

            final Handler handler3 = new Handler();
            handler3.postDelayed(new Runnable() {
                @Override
                public void run() {
                    remove(0);
                    add("Close All Battery Consuming AppFile", 2);


                }
            }, 1800);

            final Handler handler4 = new Handler();
            handler4.postDelayed(new Runnable() {
                @Override
                public void run() {
                    remove(0);
                    add("Closes System Services like Bluetooth,Screen Rotation,Sync etc.", 3);


                }
            }, 2700);

            final Handler handler5 = new Handler();
            handler5.postDelayed(new Runnable() {
                @Override
                public void run() {
                    remove(0);
                    add("Closes System Services like Bluetooth,Screen Rotation,Sync etc.", 4);
                }
            }, 3700);
//
            final Handler handler6 = new Handler();
            handler6.postDelayed(new Runnable() {
                @Override
                public void run() {
                    remove(0);
                    add("Closes System Services like Bluetooth,Screen Rotation,Sync etc.", 5);
                }
            }, 4400);

            final Handler handler7 = new Handler();
            handler7.postDelayed(new Runnable() {
                @Override
                public void run() {
                    add("Closes System Services like Bluetooth,Screen Rotation,Sync etc.", 6);
                    remove(0);

                    img_animation.setImageResource(0);
                    img_animation.setBackgroundResource(0);
                    shadowCpu.setVisibility(View.GONE);
//                    scanner.setImageResource(R.mipmap.ic_cooling_complete_check);
                    scanner.setVisibility(View.GONE);
                    ivCompltecheck.setImageResource(R.mipmap.ic_cooling_complete_check);
                    ivCompltecheck.setVisibility(View.VISIBLE);
                    ObjectAnimator anim = (ObjectAnimator) AnimatorInflater.loadAnimator(getApplicationContext(), R.animator.flipping);
                    anim.setTarget(scanner);
                    anim.setDuration(3000);
                    anim.start();

                    rel.setVisibility(View.GONE);

                    cooledcpu.setText("Cooled CPU to 25.3°C");
                    anim.addListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                            img_animation.setImageResource(0);
                            img_animation.setBackgroundResource(0);
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            final Handler handler6 = new Handler();
                            handler6.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    startActivity(new Intent(getApplicationContext(), AfterCleaningActivity.class));

                                    AdsUtility.showInterstitial(ScannerCPUActivity.this);

                                    finish();
                                }
                            }, 1000);

                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    });

                }
            }, 5500);
        }
        catch(Exception e)
        {

        }

    }

    public void add(String text, int position) {
        try {
            mAdapter.notifyItemInserted(position);
        }
        catch(Exception e)
        {

        }
    }

    public void remove(int position) {
        mAdapter.notifyItemRemoved(position);
        try {
            Frag_CPU_cooler.apps.remove(position);
        }
        catch(Exception e)
        {

        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }
}

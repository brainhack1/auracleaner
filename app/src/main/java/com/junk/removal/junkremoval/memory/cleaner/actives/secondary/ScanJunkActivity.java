package com.junk.removal.junkremoval.memory.cleaner.actives.secondary;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.junk.removal.junkremoval.memory.cleaner.R;
import com.junk.removal.junkremoval.memory.cleaner.actives.AfterCleaningActivity;
import com.junk.removal.junkremoval.memory.cleaner.utils.AdsUtility;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.junk.removal.junkremoval.memory.cleaner.apps.Applic.isAdAllowed;

public class ScanJunkActivity extends SuperActivity {
    ImageView front, appLogo, divider;
    int check = 0;
    TextView files;
    List<ApplicationInfo> packages;
    int prog=0;
    Timer T2;
    PackageManager pm;
    TextView scanning;

//    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.scanning_junk);

        LinearLayout bg = findViewById(R.id.scan_junk_bg);

        bg.setBackgroundResource(R.drawable.scan_junk_bg_changing);

        AnimationDrawable animationDrawable = (AnimationDrawable) bg.getBackground();
        animationDrawable.setEnterFadeDuration(700);
        animationDrawable.setExitFadeDuration(700);
        animationDrawable.start();

        files = findViewById(R.id.files);
        scanning= findViewById(R.id.scanning);

        appLogo = findViewById(R.id.app_logo);
        divider = findViewById(R.id.divider);

//        if (isAdAllowed) {
////            mInterstitialAd = new InterstitialAd(getApplicationContext());
////
////            mInterstitialAd.setAdUnitId(getResources().getString(R.string.scanning_junk_admob));
////
////            AdRequest adRequestInter = new AdRequest.Builder().build();
////
////            mInterstitialAd.loadAd(adRequestInter);
//
//            mInterstitialAd = new InterstitialAd(this, getString(R.string.facebook_interstitial_ad_id));
//            mInterstitialAd.loadAd(mInterstitialAd.buildLoadAdConfig().build());
//        }

        RotateAnimation rotate = new RotateAnimation(0, 360, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(1500);
        rotate.setRepeatCount(4);
        rotate.setInterpolator(new LinearInterpolator());

        front = findViewById(R.id.front);
        front.setAnimation(rotate);

        pm = getPackageManager();

        packages = pm.getInstalledApplications(0);

        T2 = new Timer();
        T2.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        if(prog < packages.size()) {
                            files.setText(packages.get(prog).loadLabel(getPackageManager()).toString());
                            try {
                                appLogo.setImageDrawable(packages.get(prog).loadIcon(getPackageManager()));
                            } catch (Exception e) {}
                            prog++;
                        }
                        else {
                            T2.cancel();
                            T2.purge();
                        }

                    }
                });
            }
        },150, 150);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    front.setImageResource(R.drawable.ok);
                    front.getLayoutParams().height /= 1.5;
                    front.getLayoutParams().width /= 1.5;
                    front.requestLayout();
                    files.setVisibility(View.GONE);
                    appLogo.setVisibility(View.GONE);
                    divider.setVisibility(View.GONE);
                    findViewById(R.id.back).setVisibility(View.GONE);
                } catch (Exception e) {}

                scanning.setPadding(20,0,0,0);
                scanning.setText(R.string.perfect);
                scanning.setTextSize(40);

                ObjectAnimator anim = (ObjectAnimator) AnimatorInflater.loadAnimator(getApplicationContext(), R.animator.flipping);
                anim.setTarget(front);
                anim.setDuration(3000);
                anim.start();

                anim.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                startActivity(new Intent(getApplicationContext(), AfterCleaningActivity.class));

                                AdsUtility.showInterstitial(ScanJunkActivity.this);

                                finish();
                            }
                        }, 1000);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                });
            }
        }, 8000);

//            }
//        }, 1000);
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }
}

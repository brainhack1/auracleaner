package com.junk.removal.junkremoval.memory.cleaner.fragments;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.junk.removal.junkremoval.memory.cleaner.obj.AppFile;
import com.junk.removal.junkremoval.memory.cleaner.R;
import com.junk.removal.junkremoval.memory.cleaner.users_cust.RecAdapter;
import com.junk.removal.junkremoval.memory.cleaner.actives.secondary.ScannerCPUActivity;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.recyclerview.animators.SlideInDownAnimator;

public class Frag_CPU_cooler extends Fragment {

    TextView batterytemp, showmain, showsec, nooverheating, coolbutton;
    float temp;
    RecyclerView recyclerView;
    RecAdapter mAdapter;
    public static List<AppFile> apps;
    List<AppFile> apps2;
    int check = 0;

    BroadcastReceiver batteryReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            makeStabilityScanning(intent);
        }
    };

    View view;

    private ImageView backBtn;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.cpu_cooler, container, false);

        backBtn = view.findViewById(R.id.exit_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        try {
            recyclerView = view.findViewById(R.id.recycler_view);

            showmain = view.findViewById(R.id.showmain);
            showsec = view.findViewById(R.id.showsec);
            coolbutton = view.findViewById(R.id.coolbutton);
            nooverheating = view.findViewById(R.id.nooverheating);

            showmain.setText(R.string.normal);
            showsec.setText(R.string.temperature_is_good);
            nooverheating.setText(R.string.currently_no_apps);

            coolbutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    @SuppressLint("RestrictedApi") LayoutInflater inflater = getLayoutInflater(getArguments());
                    View layout = inflater.inflate(R.layout.my_toast, null);

                    TextView text = layout.findViewById(R.id.textView1);
                    text.setText(R.string.temperature_already_normal);

                    Toast toast = new Toast(getActivity());
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 70);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(layout);
                    toast.show();
                }
            });

            batterytemp = view.findViewById(R.id.batterytemp);

            if (!((System.currentTimeMillis() - getActivity().getSharedPreferences("APPS_CONFIGS", Context.MODE_PRIVATE).getLong("COOLER_LAST_UPDATE", 0)) < 1200000)) {
                makeStabilityScanning(null);
            }

            Log.e("Temperrature", temp + "");
        } catch (Exception e) {

        }

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {

            getActivity().unregisterReceiver(batteryReceiver);
        } catch (Exception e) {

        }
    }

    public void getAllICONS() {
        PackageManager pm = getActivity().getPackageManager();

        List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);

        if (packages != null) {
            for (int k = 0; k < packages.size(); k++) {
                String packageName = packages.get(k).packageName;
                Log.e("packageName-->", "" + packageName);

                if (!packageName.equals("fast.cleaner.battery.saver")) {
                    Drawable ico = null;
                    try {
                        String pName = (String) pm.getApplicationLabel(pm.getApplicationInfo(packageName, PackageManager.GET_META_DATA));
                        AppFile app = new AppFile();
                        File file = new File(pm.getApplicationInfo(packageName, PackageManager.GET_META_DATA).publicSourceDir);
                        long size = file.length();

                        Log.e("SIZE", size / 1000000 + "");
                        app.setSize(size / 1000000 + 20 + "MB");

                        ApplicationInfo a = pm.getApplicationInfo(packageName, PackageManager.GET_META_DATA);
                        app.setImage(ico = getActivity().getPackageManager().getApplicationIcon(packages.get(k).packageName));
                        getActivity().getPackageManager();
                        Log.e("ico-->", "" + ico);

                        if (((a.flags & ApplicationInfo.FLAG_SYSTEM) == 0)) {
                            if (check <= 5) {
                                check++;
                                apps.add(app);
                            } else {
                                getActivity().unregisterReceiver(batteryReceiver);
                                break;
                            }

                        }
                        mAdapter.notifyDataSetChanged();


                    } catch (PackageManager.NameNotFoundException e) {
                        Log.e("ERROR", "Unable to find icon for package '"
                                + packageName + "': " + e.getMessage());
                    }
                }
            }

        }

        if (apps.size() > 1) {
            mAdapter = new RecAdapter(apps);
            mAdapter.notifyDataSetChanged();
        }
    }

    private void makeStabilityScanning (Intent intent) {
        try {
            if (intent == null)
                intent = getActivity().registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

            temp = ((float) intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, 21)) / 10;

            batterytemp.setText(temp + "°C");

            if (temp >= 30.0) {
                apps = new ArrayList<>();
                apps2 = new ArrayList<>();
                showmain.setText(getString(R.string.overheated));
                showsec.setText(R.string.apps_causing_cool);
                nooverheating.setText("");

                coolbutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("APPS_CONFIGS", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putLong("COOLER_LAST_UPDATE", System.currentTimeMillis());
                        editor.commit();

                        Intent i = new Intent(getActivity(), ScannerCPUActivity.class);
                        startActivity(i);

                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                nooverheating.setText(getString(R.string.currently_no_apps));
                                showmain.setText(getString(R.string.normal));
                                showsec.setText(getString(R.string.temperature_is_good));
                                batterytemp.setText("25.3" + "°C");
                                recyclerView.setAdapter(null);

                            }
                        }, 2000);


                        coolbutton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                @SuppressLint("RestrictedApi") LayoutInflater inflater = getLayoutInflater(getArguments());
                                View layout = inflater.inflate(R.layout.my_toast, null);

                                TextView text = layout.findViewById(R.id.textView1);
                                text.setText(getString(R.string.temperature_already_normal));

                                Toast toast = new Toast(getActivity());
                                toast.setGravity(Gravity.CENTER_VERTICAL, 0, 70);
                                toast.setDuration(Toast.LENGTH_LONG);
                                toast.setView(layout);
                                toast.show();
                            }
                        });
                    }
                });

                recyclerView.setItemAnimator(new SlideInDownAnimator());
                recyclerView.getItemAnimator().setAddDuration(10000);

                mAdapter = new RecAdapter(apps);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new SlideInDownAnimator(new OvershootInterpolator(1f)));
                recyclerView.computeHorizontalScrollExtent();
                recyclerView.setAdapter(mAdapter);
                getAllICONS();
            }
        }
        catch(Exception e) {}
    }
}

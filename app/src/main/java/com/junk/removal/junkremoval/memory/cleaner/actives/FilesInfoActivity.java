package com.junk.removal.junkremoval.memory.cleaner.actives;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import com.google.android.gms.ads.AdRequest;
import com.junk.removal.junkremoval.memory.cleaner.R;
import com.junk.removal.junkremoval.memory.cleaner.actives.secondary.SuperActivity;
import com.junk.removal.junkremoval.memory.cleaner.obj.FileObj;
import com.junk.removal.junkremoval.memory.cleaner.utils.AdsUtility;
import com.junk.removal.junkremoval.memory.cleaner.utils.FormatFilesSize;

import static com.junk.removal.junkremoval.memory.cleaner.actives.BigFilesActivity.getFileImageByExtension;
import static com.junk.removal.junkremoval.memory.cleaner.apps.Applic.isAdAllowed;
import static com.junk.removal.junkremoval.memory.cleaner.utils.Util.openFile;

public class FilesInfoActivity extends SuperActivity {

    public static myListAdapter filesAdapter;
    private ListView listView;

    private AppsComponentsLoading appsComponentsLoading;

    private LinearLayout backBtn;
    private TextView cleanSelected;

    private ProgressBar progressBar;

    SharedPreferences sharedpreferences, configs;

//    private InterstitialAd mInterstitialAd;

    private ArrayList<FileObj> filesList;
    private final ArrayList<FileObj> tempFiles = new ArrayList<>();

    private int mode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        overridePendingTransition(R.anim.fast_fadein, 0);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        try {
            ActionBar actionBar = getSupportActionBar();
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        setContentView(R.layout.activity_files_info);

//        if (isAdAllowed) {
////            mInterstitialAd = new InterstitialAd(FilesInfoActivity.this);
////            mInterstitialAd.setAdUnitId(getResources().getString(R.string.files_manager_activity));
////            AdRequest adRequestInter = new AdRequest.Builder().build();
////
////            mInterstitialAd.loadAd(adRequestInter);
//
//            mInterstitialAd = new com.facebook.ads.InterstitialAd(this, getString(R.string.facebook_interstitial_ad_id));
//            mInterstitialAd.loadAd(mInterstitialAd.buildLoadAdConfig().build());
//        }

        backBtn = findViewById(R.id.exit_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        progressBar = findViewById(R.id.progressBar);

        progressBar.getIndeterminateDrawable()
                .setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN);

        sharedpreferences = getSharedPreferences("waseem", MODE_PRIVATE);
        configs = getSharedPreferences("Configs", MODE_PRIVATE);

        filesList = new ArrayList<>();

        appsComponentsLoading = new AppsComponentsLoading();
        appsComponentsLoading.execute ();
    }

    private class AppsComponentsLoading extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            File externalStorageDirectory = Environment.getExternalStorageDirectory();
            File folder = new File(externalStorageDirectory.getAbsolutePath());

            checkFiles(folder);

            Collections.sort(filesList, new Comparator<FileObj>() {
                @Override
                public int compare(FileObj initApp, FileObj t1) {
                    if (initApp.getSize() < t1.getSize())
                        return 1;
                    else if (initApp.getSize() > t1.getSize())
                        return -1;

                    return 0;
                }
            });

            return null;
        }

        @Override
        protected void onPostExecute(final Void aVoid) {
            super.onPostExecute(aVoid);

            findViewById(R.id.where_listView_relative).setVisibility(View.VISIBLE);

            cleanSelected = findViewById(R.id.clean_all);

            CheckBox selectAllSwitch = findViewById(R.id.select_all);
            selectAllSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                    if (mode == 0)
                        for (FileObj fileObject : filesList)
                            fileObject.setSelected(b);
                    else
                        for (FileObj fileObject : tempFiles)
                            fileObject.setSelected(b);

                    filesAdapter.notifyDataSetChanged();
                }
            });

            filesAdapter = new myListAdapter(getApplicationContext(), filesList);
            filesAdapter.notifyDataSetChanged();

            listView = findViewById(R.id.apps_delete);
            listView.setAdapter(filesAdapter);

            progressBar.setVisibility(View.GONE);

            cleanSelected.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(FilesInfoActivity.this);
                    AlertDialog dialog = builder.setTitle(R.string.delete_selected_items).setMessage(getResources().getString(R.string.delete_all_files_content))
                            .setCancelable(true)
                            .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    for (int i = 0; i < filesList.size(); i++)
                                        if (filesList.get(i).isSelected()) {
                                            File file = new File(filesList.get(i).getPath());
                                            file.delete();

                                            tempFiles.remove(filesList.get(i));

                                            filesList.remove(filesList.get(i));

                                            i--;
                                        }

                                    filesAdapter.notifyDataSetChanged();

                                    dialog.cancel();
                                }
                            }).setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    }
                            ).create();

                    dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                        @Override
                        public void onShow(DialogInterface dialogInterface) {
                            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.RED);
                            dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.GRAY);
                        }
                    });

                    dialog.show();
                }
            });

            findViewById(R.id.files).setEnabled(false);
            findViewById(R.id.files).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mode = 0;

                    ((TextView) findViewById(R.id.files)).setTextColor(Color.parseColor("#00FF23"));
                    ((TextView) findViewById(R.id.video)).setTextColor(Color.parseColor("#FFFFFF"));
                    ((TextView) findViewById(R.id.audio)).setTextColor(Color.parseColor("#FFFFFF"));

                    findViewById(R.id.files).setEnabled(false);
                    findViewById(R.id.video).setEnabled(true);
                    findViewById(R.id.audio).setEnabled(true);

                    for (FileObj fileObject : filesList)
                        fileObject.setSelected(false);

                    selectAllSwitch.setChecked(false);

                    tempFiles.clear();

                    filesAdapter.setArrayMyData(filesList);
                    filesAdapter.notifyDataSetChanged();
                }
            });

            findViewById(R.id.video).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mode = 1;

                    ((TextView) findViewById(R.id.video)).setTextColor(Color.parseColor("#00FF23"));
                    ((TextView) findViewById(R.id.files)).setTextColor(Color.parseColor("#FFFFFF"));
                    ((TextView) findViewById(R.id.audio)).setTextColor(Color.parseColor("#FFFFFF"));

                    findViewById(R.id.files).setEnabled(true);
                    findViewById(R.id.video).setEnabled(false);
                    findViewById(R.id.audio).setEnabled(true);

                    for (FileObj fileObject : filesList)
                        fileObject.setSelected(false);

                    selectAllSwitch.setChecked(false);

                    tempFiles.clear();

                    for (FileObj fileObject : filesList)
                        if (FormatFilesSize.video.contains(fileObject.getType()))
                            tempFiles.add(fileObject);

                    filesAdapter.setArrayMyData(tempFiles);
                    filesAdapter.notifyDataSetChanged();
                }
            });

            findViewById(R.id.audio).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mode = 2;

                    ((TextView) findViewById(R.id.audio)).setTextColor(Color.parseColor("#00FF23"));
                    ((TextView) findViewById(R.id.files)).setTextColor(Color.parseColor("#FFFFFF"));
                    ((TextView) findViewById(R.id.video)).setTextColor(Color.parseColor("#FFFFFF"));

                    findViewById(R.id.files).setEnabled(true);
                    findViewById(R.id.video).setEnabled(true);
                    findViewById(R.id.audio).setEnabled(false);

                    for (FileObj fileObject : filesList)
                        fileObject.setSelected(false);

                    selectAllSwitch.setChecked(false);

                    tempFiles.clear();

                    for (FileObj fileObject : filesList)
                        if (FormatFilesSize.music.contains(fileObject.getType()))
                            tempFiles.add(fileObject);

                    filesAdapter.setArrayMyData(tempFiles);
                    filesAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    class myListAdapter extends BaseAdapter {
        private ArrayList<FileObj> arrayMyMatches;

        public myListAdapter (Context ctx, ArrayList<FileObj> arr) {
            setArrayMyData(arr);
        }

        public ArrayList<FileObj> getArrayMyData() {
            return arrayMyMatches;
        }

        public void setArrayMyData(ArrayList<FileObj> arrayMyData) {
            this.arrayMyMatches = arrayMyData;
        }

        public boolean remove (int i) {
            arrayMyMatches.remove(i);

            return true;
        }

        public int getCount () {
            return arrayMyMatches.size();
        }

        public Object getItem (int position) {
            FileObj app = arrayMyMatches.get(position);

            return app;
        }

        public long getItemId (int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            try {
                if (convertView == null) {
                    LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = inflater.inflate(R.layout.files_info_row, null, true);
                }

                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (tempFiles.isEmpty())
                            openFile(new File(filesList.get(position).getPath()), FilesInfoActivity.this);
                        else
                            openFile(new File(tempFiles.get(position).getPath()), FilesInfoActivity.this);
                    }
                });

                ImageView app_image = convertView.findViewById(R.id.app_image);
                TextView app_name = convertView.findViewById(R.id.name);
                CheckBox checkBox = convertView.findViewById(R.id.remove_btn);

                final TextView size = convertView.findViewById(R.id.size);
                size.setText(arrayMyMatches.get(position).getSize() + getResources().getString(R.string.mb));
                app_name.setText(arrayMyMatches.get(position).getName());
                app_image.setImageDrawable(getResources().getDrawable(getFileImageByExtension(arrayMyMatches.get(position).getType())));

                checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        if (tempFiles.isEmpty())
                            filesList.get(position).setSelected(b);
                        else
                            tempFiles.get(position).setSelected(b);
                    }
                });

                if (tempFiles.isEmpty())
                    checkBox.setChecked(filesList.get(position).isSelected());
                else
                    checkBox.setChecked(tempFiles.get(position).isSelected());

            } catch (Exception e) {

            } finally {
                return convertView;
            }
        }
    }

    private void checkFiles(File parentDir) {
        File[] files = parentDir.listFiles();

        try {
            if (files != null)
                for (File file : files) {
                    if (file.isFile()) {
                        FileObj fileObject = new FileObj(file.getName(), file.getPath(), file.length()/1024/1024);

                        String extension = file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf("."));
                        Date lastModDate = new Date(file.lastModified());

                        fileObject.setType(extension);
                        fileObject.setDate(lastModDate.toString());

                        filesList.add(fileObject);
                    } else if (file.isDirectory())
                        checkFiles(file);
                }
        } catch (Exception e) {}
    }

    //проверка наличие разрешение при запросе либо проверка при предоставлении

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (requestCode == 1) {
                startActivity(new Intent(FilesInfoActivity.this, FilesInfoActivity.class));
                finish();
            }
        }
    }

    @Override
    public void onBackPressed() {
        AdsUtility.showMrec(R.id.exit_banner, this);

        RelativeLayout exitDialog = findViewById(R.id.exit_dialog);
        exitDialog.setVisibility(View.VISIBLE);

        findViewById(R.id.yes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), MenuActivity.class));

                AdsUtility.showInterstitial(FilesInfoActivity.this);

                finish();
            }
        });

        findViewById(R.id.no).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exitDialog.setVisibility(View.GONE);
            }
        });
    }
}

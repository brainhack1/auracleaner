package com.junk.removal.junkremoval.memory.cleaner.actives;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.billingclient.api.AcknowledgePurchaseParams;
import com.android.billingclient.api.AcknowledgePurchaseResponseListener;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesResponseListener;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.junk.removal.junkremoval.memory.cleaner.R;
import com.junk.removal.junkremoval.memory.cleaner.actives.secondary.SuperActivity;
import com.junk.removal.junkremoval.memory.cleaner.apps.Applic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SubsActivity extends SuperActivity {

    private TextView lastPriceMonth, lastPriceThreeMonth, lastPriceYear, priceMonth, priceThreeMonth, priceYear;

    private BillingClient billingClient;
    private final Map<String, SkuDetails> mSkuDetailsMap = new HashMap<>();
    private final String monthSku = "subsmonth";
    private final String threeMonthSku = "substhreemonth";
    private final String yearSku = "subsyear";

    private boolean isSubsActive;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        overridePendingTransition(R.anim.fadein, R.anim.fadeout);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_subscription);

        findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        lastPriceMonth = findViewById(R.id.last_price_month);
        lastPriceThreeMonth = findViewById(R.id.last_price_three_month);
        lastPriceYear = findViewById(R.id.last_price_year);
        priceMonth = findViewById(R.id.price_month);
        priceThreeMonth = findViewById(R.id.price_three_month);
        priceYear = findViewById(R.id.price_year);

        findViewById(R.id.one_month).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchBilling(monthSku);
            }
        });

        findViewById(R.id.three_month).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchBilling(threeMonthSku);
            }
        });

        findViewById(R.id.one_year).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchBilling(yearSku);
            }
        });

        billingClient = BillingClient.newBuilder(this).setListener(new PurchasesUpdatedListener() {
            @Override
            public void onPurchasesUpdated(@NonNull BillingResult billingResult, @Nullable List<Purchase> list) {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK && list != null) {
                    //сюда мы попадем когда будет осуществлена покупка

                    if (list != null)
                        for (Purchase purchase: list) {
                            AcknowledgePurchaseParams acknowledgePurchaseParams =
                                    AcknowledgePurchaseParams.newBuilder()
                                            .setPurchaseToken(purchase.getPurchaseToken())
                                            .build();

                            AcknowledgePurchaseResponseListener acknowledgePurchaseResponseListener = new AcknowledgePurchaseResponseListener() {
                                @Override
                                public void onAcknowledgePurchaseResponse(BillingResult billingResult) {
                                    BillingResult billingResult1 = billingResult;
                                }

                            };

                            billingClient.acknowledgePurchase(acknowledgePurchaseParams, acknowledgePurchaseResponseListener);
                        }

                    payComplete();
                }
            }
        }).enablePendingPurchases().build();

        billingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(@NonNull BillingResult billingResult) {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    //здесь мы можем запросить информацию о товарах и покупках

                    querySkuDetails();

                    billingClient.queryPurchasesAsync(BillingClient.SkuType.SUBS, new PurchasesResponseListener() {
                        @Override
                        public void onQueryPurchasesResponse(@NonNull BillingResult billingResult, @NonNull List<Purchase> purchasesList) {
                            if (purchasesList != null) {
                                //если товар уже куплен, предоставить его пользователю
                                for (int i = 0; i < purchasesList.size(); i++) {
                                    String purchaseId = purchasesList.get(i).getSkus().get(0);
                                    if (purchasesList.get(i).getSkus().contains(monthSku) || purchasesList.get(i).getSkus().contains(threeMonthSku) || purchasesList.get(i).getSkus().contains(yearSku)) {
                                        payComplete();
                                    }
                                }
                            }
                        }
                    });

                    if (!isSubsActive)
                        Applic.isAdAllowed = true;
                }
            }

            @Override
            public void onBillingServiceDisconnected() {
                //сюда мы попадем если что-то пойдет не так
            }
        });
    }

    private void querySkuDetails() {
        SkuDetailsParams.Builder skuDetailsParamsBuilder = SkuDetailsParams.newBuilder();
        List<String> skuList = new ArrayList<>();
        skuList.add(monthSku);
        skuList.add(threeMonthSku);
        skuList.add(yearSku);
        skuDetailsParamsBuilder.setSkusList(skuList).setType(BillingClient.SkuType.SUBS);
        billingClient.querySkuDetailsAsync(skuDetailsParamsBuilder.build(), new SkuDetailsResponseListener() {
            @Override
            public void onSkuDetailsResponse(@NonNull BillingResult billingResult, @Nullable List<SkuDetails> list) {
                if (billingResult.getResponseCode() == 0) {
                    for (SkuDetails skuDetails : list) {
                        mSkuDetailsMap.put(skuDetails.getSku(), skuDetails);

                        if (skuDetails.getSku().equals(monthSku)) {
                            try {
                                String str = skuDetails.getPrice().replaceAll(",", ".");

                                char num = str.charAt(0);
                                int digit = Integer.parseInt(Character.toString(num));
                                digit += 2;

                                lastPriceMonth.setText(digit + skuDetails.getPrice().substring(1));
                            } catch (Exception e) {}

                            priceMonth.setText(skuDetails.getPrice());
                        } else if (skuDetails.getSku().equals(threeMonthSku)) {
                            try {
                                String str = skuDetails.getPrice().replaceAll(",", ".");

                                char num = str.charAt(0);
                                int digit = Integer.parseInt(Character.toString(num));
                                digit += 2;

                                lastPriceThreeMonth.setText(digit + skuDetails.getPrice().substring(1));
                            } catch (Exception e) {}

                            priceThreeMonth.setText(skuDetails.getPrice());
                        } else if (skuDetails.getSku().equals(yearSku)) {
                            try {
                                String str = skuDetails.getPrice().replaceAll(",", ".");

                                char num = str.charAt(0);
                                int digit = Integer.parseInt(Character.toString(num));
                                digit += 2;

                                lastPriceYear.setText(digit + skuDetails.getPrice().substring(1));
                            } catch (Exception e) {}

                            priceYear.setText(skuDetails.getPrice());
                        }
                    }
                }
            }
        });
    }

    public void launchBilling(String skuId) {
        try {
            BillingFlowParams billingFlowParams = BillingFlowParams.newBuilder()
                    .setSkuDetails(mSkuDetailsMap.get(skuId))
                    .build();
            billingClient.launchBillingFlow(this, billingFlowParams);
        } catch (Exception e) {}
    }

    private void payComplete () {
        isSubsActive = true;
        Applic.isAdAllowed = false;

        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(getApplicationContext(), MenuActivity.class));
        finish();
    }
}
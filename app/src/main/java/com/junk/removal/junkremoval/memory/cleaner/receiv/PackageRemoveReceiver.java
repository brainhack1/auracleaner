package com.junk.removal.junkremoval.memory.cleaner.receiv;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.widget.RemoteViews;

import com.junk.removal.junkremoval.memory.cleaner.R;
import com.junk.removal.junkremoval.memory.cleaner.actives.secondary.NotificationsCleanerActivity;
import com.junk.removal.junkremoval.memory.cleaner.enumss.Settings_enums;

import static android.content.Context.MODE_PRIVATE;

public class PackageRemoveReceiver extends BroadcastReceiver {

    private static final int NOTIF_ID = 1001;
    private static final String NOTIF_CHANNEL_ID = "id_channel";

    @Override
    public void onReceive(Context context, Intent intent) {
        startForegroundService(context);
    }

    private void startForegroundService(Context context) {
        SharedPreferences configs = context.getSharedPreferences("Configs", MODE_PRIVATE);

        if (!configs.getBoolean(Settings_enums.NOTIF_PERMISSION_DISABLED + "", false)) {
            NotificationManager notificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                NotificationChannel channel = new NotificationChannel(NOTIF_CHANNEL_ID, "Applic deleting channel",
                        NotificationManager.IMPORTANCE_HIGH);
                channel.setDescription("Applic deleting channel");
                channel.enableLights(true);
                channel.setLightColor(Color.RED);
                channel.enableVibration(true);
                notificationManager.createNotificationChannel(channel);
            }

            Intent resultIntent = new Intent(context, NotificationsCleanerActivity.class);
            PendingIntent resultPendingIntent = PendingIntent.getActivity(context, 0, resultIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);

            RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.uninstall_apps_notification);

            Notification.Builder builder = new Notification.Builder(context)
                    .setSmallIcon(R.drawable.logo)
                    .setContent(remoteViews)
                    .setContentIntent(resultPendingIntent)
                    .setContentTitle("Cache Cleaner")
                    .setContentText("Cache Cleaner")
                    .setDefaults(Notification.DEFAULT_VIBRATE | Notification.DEFAULT_LIGHTS | Notification.FLAG_AUTO_CANCEL)
                    .setAutoCancel(true);

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                builder.setChannelId(NOTIF_CHANNEL_ID);
            }

            notificationManager.notify(NOTIF_ID, builder.build());
        }
    }
}
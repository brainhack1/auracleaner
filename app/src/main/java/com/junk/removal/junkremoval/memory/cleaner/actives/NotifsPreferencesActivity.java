package com.junk.removal.junkremoval.memory.cleaner.actives;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.junk.removal.junkremoval.memory.cleaner.R;
import com.junk.removal.junkremoval.memory.cleaner.actives.secondary.SuperActivity;
import com.junk.removal.junkremoval.memory.cleaner.obj.NotifAppPref;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class NotifsPreferencesActivity extends SuperActivity {

    public myListAdapter measuringAdapter;
    private ProgressBar progressBar;

    private ListView listView;

    private ArrayList<NotifAppPref> apps;

    private AppsComponentsLoading appsComponentsLoading;

    private ImageView backBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        overridePendingTransition(R.anim.fast_fadein, 0);

        setContentView(R.layout.loading_screen);

        try {
            ActionBar actionBar = getSupportActionBar();
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        progressBar = findViewById(R.id.progressBar);

        progressBar.getIndeterminateDrawable()
                .setColorFilter(getResources().getColor(R.color.yellow), PorterDuff.Mode.SRC_IN);

        appsComponentsLoading = new AppsComponentsLoading();
        appsComponentsLoading.execute ();
    }

    private class AppsComponentsLoading extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            setContentView(R.layout.activity_notif_preference_apps);

            backBtn = findViewById(R.id.cancel);
            backBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });

            listView = findViewById(R.id.results);
            listView.setAdapter(measuringAdapter);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    view.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            switch (event.getAction()) {
                                case MotionEvent.ACTION_DOWN:
                                    break;
                                case MotionEvent.ACTION_UP:
                                    break;
                            }

                            return false;
                        }
                    });

                    goToNotificationSettings(null, apps.get(position).getPackageName(), getApplicationContext());
                }
            });

            Toast.makeText(NotifsPreferencesActivity.this, "Here you can change notification settings", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            apps = new ArrayList<>();

            PackageManager packageManager = getPackageManager();
            Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
            mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);

            List<ResolveInfo> appList = packageManager.queryIntentActivities(mainIntent, 0);
            Collections.sort(appList, new ResolveInfo.DisplayNameComparator(packageManager));

            for (ResolveInfo appInfo : appList)
                apps.add(new NotifAppPref(appInfo.loadIcon(getPackageManager()),
                        (String) appInfo.loadLabel(getPackageManager()), appInfo.activityInfo.packageName));

            measuringAdapter = new myListAdapter(getApplicationContext(), apps);

            return null;
        }
    }

    class myListAdapter extends BaseAdapter {
        private final LayoutInflater mLayoutInflater;
        private ArrayList<NotifAppPref> arrayMyMatches;

        public myListAdapter (Context ctx, ArrayList<NotifAppPref> arr) {
            mLayoutInflater = LayoutInflater.from(ctx);
            setArrayMyData(arr);
        }
        public ArrayList<NotifAppPref> getArrayMyData() {
            return arrayMyMatches;
        }

        public void setArrayMyData(ArrayList<NotifAppPref> arrayMyData) {
            this.arrayMyMatches = arrayMyData;
        }
        public int getCount () {
            return arrayMyMatches.size();
        }

        public Object getItem (int position) {
            NotifAppPref app = arrayMyMatches.get(position);

            return app;
        }

        public void remove () {

        }

        public long getItemId (int position) {
            return position;
        }

        public void refuse (ArrayList<NotifAppPref> apps) {
            arrayMyMatches.clear ();
            arrayMyMatches.addAll (apps);
            notifyDataSetChanged();
        }

        //получение элемента ListView и его отправка в активность данных

        public View getView(final int position, View convertView, ViewGroup parent) {
            try {
                if (convertView == null) {
                    LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = inflater.inflate(R.layout.init_pref_notif_app, null, true);
                }

                ImageView app_image = convertView.findViewById(R.id.app_image);
                TextView app_name = convertView.findViewById(R.id.name);

                app_image.setImageDrawable(arrayMyMatches.get(position).getAppImage());
                app_name.setText(arrayMyMatches.get(position).getAppName());

            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
            } finally {
                return convertView;
            }
        }
    }

    public void goToNotificationSettings(String channel, String packageName, Context context) {
        Intent intent = new Intent();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            intent.setAction(Settings.ACTION_CHANNEL_NOTIFICATION_SETTINGS);

            if (channel != null) {
                intent.putExtra(Settings.EXTRA_CHANNEL_ID, channel);
            } else {
                intent.setAction(Settings.ACTION_APP_NOTIFICATION_SETTINGS);
            }
            intent.putExtra(Settings.EXTRA_APP_PACKAGE, packageName);
        } else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1) {
            intent.setAction(Settings.ACTION_APP_NOTIFICATION_SETTINGS);
            intent.putExtra(Settings.EXTRA_APP_PACKAGE, packageName);
        } else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            intent.setAction(Settings.ACTION_APP_NOTIFICATION_SETTINGS);
            intent.putExtra("app_package", packageName);
//            intent.putExtra("app_uid", context.getApplicationInfo().uid);
        } else if (Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT) {
            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            intent.addCategory(Intent.CATEGORY_DEFAULT);
            intent.setData(Uri.parse("package:" + packageName));
        }

        try {
            context.startActivity(intent);
        } catch (Exception e) {}
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
//                startActivity (new Intent (getApplicationContext(), MainActivity.class));

                finish ();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public static long TIME_INTERVAL_FOR_EXIT = 1500;
    private long lastTimeBackPressed;

    @Override
    public boolean onKeyDown(final int pKeyCode, final KeyEvent pEvent) {
        if(pKeyCode == KeyEvent.KEYCODE_BACK && pEvent.getAction() == KeyEvent.ACTION_DOWN) {
//            if(System.currentTimeMillis() - lastTimeBackPressed < TIME_INTERVAL_FOR_EXIT) {
//                finish();
//            }
//            else {}

            onBackPressed();

            return true;
        } else {
            return super.onKeyDown(pKeyCode, pEvent);
        }
    }

    @Override
    public void onBackPressed() {
        appsComponentsLoading.cancel(true);

        super.onBackPressed();
    }
}
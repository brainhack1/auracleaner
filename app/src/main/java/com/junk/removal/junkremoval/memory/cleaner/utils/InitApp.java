package com.junk.removal.junkremoval.memory.cleaner.utils;

import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class InitApp implements Serializable, Parcelable {
    private Drawable app_image;
    private String app_name, app_package;
    private boolean app_in_quae;

    public InitApp(Drawable app_image, String app_name, String app_package, boolean app_in_quae) {
        this.app_image = app_image;
        this.app_name = app_name;
        this.app_package = app_package;
        this.app_in_quae = app_in_quae;
    }

    protected InitApp(Parcel in) {
        app_name = in.readString();
        app_package = in.readString();
        app_in_quae = in.readByte() != 0;
    }

    public static final Creator<InitApp> CREATOR = new Creator<InitApp>() {
        @Override
        public InitApp createFromParcel(Parcel in) {
            return new InitApp(in);
        }

        @Override
        public InitApp[] newArray(int size) {
            return new InitApp[size];
        }
    };

    public Drawable getApp_image() {
        return app_image;
    }

    public String getApp_name() {
        return app_name;
    }

    public String getApp_package() {
        return app_package;
    }

    public boolean getApp_in_quae() {
        return app_in_quae;
    }

    public void setApp_image(Drawable app_image) {
        this.app_image = app_image;
    }

    public void setApp_name(String app_name) {
        this.app_name = app_name;
    }

    public void setApp_package(String app_package) {
        this.app_package = app_package;
    }

    public void setApp_in_quae(boolean app_in_quae) {
        this.app_in_quae = app_in_quae;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(app_name);
        dest.writeString(app_package);
        dest.writeByte((byte) (app_in_quae ? 1 : 0));
    }
}

package com.junk.removal.junkremoval.memory.cleaner.actives.secondary;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.junk.removal.junkremoval.memory.cleaner.actives.MenuActivity;
import com.junk.removal.junkremoval.memory.cleaner.actives.InitialScreenActivity;
import com.junk.removal.junkremoval.memory.cleaner.fragments.Frag_CPU_cooler;
import com.junk.removal.junkremoval.memory.cleaner.R;

import java.io.File;
import java.util.List;
import java.util.Random;

import static com.junk.removal.junkremoval.memory.cleaner.actives.InitialScreenActivity.isCleanRequired;
import static com.junk.removal.junkremoval.memory.cleaner.apps.Applic.isAdAllowed;

public class FragmentsActivity extends SuperActivity {

    public final static String MODE_START_NOW = "START_NOW_MODE";

    public final static String JUNKSFILES = "JUNKSFILESALL", TEMPFILES = "TEMPORARIESFILESALL";

    public final static String REQUEST_ACTIVITY_CODE = "CODE_ACTIVITY";
    public final static String RUNTIME_MODE = "RUNTIME_MODE";

    public final static String JUNK_CLEANER_CODE = "JUNKCLEANER",
            BOOSTER_CODE = "PHONEBOOSTER",
            COOLER_CODE = "COOLER",
            BATTERY_SAVER_CODE = "BATTERYSAVER",
            NOTIFICATIONS_CLEANER_CODE = "NOTIFICATIONSCLEANER";

    public static final int RC_HANDLE_PERMISSIONA_ALL = 2;

    private String requestStateCode;

    public static boolean notWaitJustRedirect = false;
    private boolean isExitingFromApp;

    int alljunk;
    private boolean modeStartNow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        overridePendingTransition(R.anim.fast_fadein, 0);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_fragment_wrapper);

        try {
            isAdAllowed = !getSharedPreferences("waseem", Context.MODE_PRIVATE).getBoolean(getResources().getString(R.string.is_pay_completed), false);
        } catch (Exception e) {

        }

        try {
            Intent intent = getIntent ();

            requestStateCode = intent.getStringExtra(REQUEST_ACTIVITY_CODE);
        } catch (Exception e) {

        }

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        switch (requestStateCode) {
            case JUNK_CLEANER_CODE:
                makeCleaning();
                break;
            case COOLER_CODE:
                Frag_CPU_cooler cpuCooler_fragment = new Frag_CPU_cooler();
                ft.replace(R.id.wrapper, cpuCooler_fragment);
                ft.commit();
                break;
            default:
                Toast.makeText(this, "Wrong app query, try now!", Toast.LENGTH_SHORT).show();
                finish();
        }
    }

    private void makeCleaning () {
        if (isCleanRequired) {
            SharedPreferences sharedpreferences = getSharedPreferences(getResources().getString(R.string.app_preferences), MODE_PRIVATE);
            SharedPreferences.Editor preferencesEditor = sharedpreferences.edit();
            preferencesEditor.putLong(getString(R.string.lastTimeCleaned), System.currentTimeMillis());
            preferencesEditor.apply();

            isCleanRequired = false;

//            try {
//                deleteDirectory(new File(Environment.getExternalStorageDirectory() + File.separator + "Android"));
//
//                File[] files = Environment.getExternalStorageDirectory().listFiles();
//
//                for (File file : files) {
//                    if (file.getName().charAt(0) == '.')
//                        deleteDirectory(file);
//                }
//            } catch (Exception e) {
//
//            }

            try {
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putBoolean("isUsedCachedData", false);
                editor.apply();
            } catch (Exception e) {

            }

            try {
                cleanCacheAndTemp(getApplicationContext(), true);

                Random ran1 = new Random();
                final int proc1 = sharedpreferences.getInt("proc1", ran1.nextInt(80) + 10);

                Random ran2 = new Random();
                final int proc3 = sharedpreferences.getInt("proc2", ran2.nextInt(70) + 20);

                Random ran3 = new Random();
                final int proc2 = sharedpreferences.getInt("proc3", ran3.nextInt(100) + 30);

                Random ran4 = new Random();
                final int proc4 = sharedpreferences.getInt("proc4", ran4.nextInt(90) + 20);

                alljunk = proc1 + proc2 + proc3 + proc4;

                Intent i = new Intent(getApplicationContext(), ScanJunkActivity.class);
                i.putExtra("junk", alljunk + "");
                i.putExtra("modeStartNow", true);
                startActivity(i);
            } catch (Exception e) {
            }

            isCleanRequired = false;
        }
    }

    private int cleanCacheAndTemp(Context Activity, boolean scanAndDelete) {
        int scan = 0;

        try {
            Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
            mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
            List<ResolveInfo> pkgAppsList = Activity.getPackageManager().queryIntentActivities(mainIntent, 0);

            String mData = Environment.getExternalStorageDirectory() + File.separator + "Android" + Environment.getDataDirectory() + File.separator;
            File mFolder, mFolderTemp, mFolderFilesCash;

            for (ResolveInfo resolveInfo : pkgAppsList) {
                mFolder = new File(mData + resolveInfo.activityInfo.packageName + File.separator + "cache");
                mFolderTemp = new File(mData + resolveInfo.activityInfo.packageName + File.separator + "tmp");
                mFolderFilesCash = new File(mData + resolveInfo.activityInfo.packageName + File.separator + "files" + File.separator + "cache");
                if (!mFolder.exists()) continue;

                if (mFolder.isDirectory()) {
                    String[] children = mFolder.list();
                    for (String child : children) {
                        if (scanAndDelete) {
                            new File(mFolder, child).delete();
                            deleteDirectory(new File(mFolder, child));
                        } else {
                            scan++;
                        }
                    }
                }

                if (mFolderTemp.isDirectory()) {
                    String[] children = mFolderTemp.list();
                    for (String child : children) {
                        if (scanAndDelete) {
                            new File(mFolderTemp, child).delete();
                            deleteDirectory(new File(mFolderTemp, child));
                        } else {
                            scan++;
                        }
                    }
                }

                if (mFolderFilesCash.isDirectory()) {
                    String[] children = mFolderFilesCash.list();
                    for (String child : children) {
                        if (scanAndDelete) {
                            new File(mFolderTemp, child).delete();
                            deleteDirectory(new File(mFolderTemp, child));
                        } else {
                            scan++;
                        }
                    }
                }
            }
        } catch (Exception e) {}

        return scan;
    }

    public static boolean deleteDirectory(final File path) {
        try {
            if (path.exists()) {
                File[] files = path.listFiles();
                if (files == null) {

                }

                for (int i = 0; i < files.length; i++) {
                    if (files[i].isDirectory()) {
                        deleteDirectory(files[i]);
                    } else {
                        try {
                            files[i].delete();
                        } catch (Exception e) {}
                    }
                }
            }
        } catch (Exception e) {}

        return(path.delete());
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (isExitingFromApp) {
            startActivity (new Intent (getApplicationContext (), InitialScreenActivity.class));

            finish();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_UP:
                if (requestStateCode.equals(NOTIFICATIONS_CLEANER_CODE)) {
                    notWaitJustRedirect = true;
                    redirectToSecureSettings();
                }
                break;
        }

        return super.onTouchEvent(event);
    }

    public void redirectToSecureSettings() {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_NOTIFICATION_LISTENER_SETTINGS);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        Intent startHomescreen = new Intent(getApplicationContext(), MenuActivity.class);
        startActivity(startHomescreen);

        finish();
    }
}

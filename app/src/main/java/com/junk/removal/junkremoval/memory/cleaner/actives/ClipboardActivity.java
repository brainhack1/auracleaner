package com.junk.removal.junkremoval.memory.cleaner.actives;

import androidx.appcompat.app.ActionBar;

import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.junk.removal.junkremoval.memory.cleaner.R;
import com.junk.removal.junkremoval.memory.cleaner.actives.secondary.SuperActivity;
import com.junk.removal.junkremoval.memory.cleaner.utils.AdsUtility;

import java.util.ArrayList;

import static com.junk.removal.junkremoval.memory.cleaner.apps.Applic.isAdAllowed;

public class ClipboardActivity extends SuperActivity {

    public static myListAdapter measuringAdapter;

    private ArrayList<String> rows;

//    private com.facebook.ads.InterstitialAd mInterstitialAd;

    private ImageView backBtn, edit;
    private EditText editText;

    private ClipboardManager clipboardManager;
    private ClipData primaryClip;

    private SharedPreferences sharedpreferences;

    private static int curSavePos = 1;
    private final int maxSaveSize = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        overridePendingTransition(R.anim.fast_fadein, 0);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        sharedpreferences = getSharedPreferences("waseem", Context.MODE_PRIVATE);

        setContentView(R.layout.activity_clipboard_manager);

        try {
            isAdAllowed = !getSharedPreferences("waseem", Context.MODE_PRIVATE).getBoolean(getResources().getString(R.string.is_pay_completed), false);
        } catch (Exception e) {

        }

//        if (isAdAllowed) {
////            mInterstitialAd = new InterstitialAd(ClipboardActivity.this);
////            mInterstitialAd.setAdUnitId(getResources().getString(R.string.device_info_screen));
////            AdRequest adRequestInter = new AdRequest.Builder().build();
////
////            mInterstitialAd.loadAd(adRequestInter);
//
//            mInterstitialAd = new InterstitialAd(this, getString(R.string.facebook_interstitial_ad_id));
//            mInterstitialAd.loadAd(mInterstitialAd.buildLoadAdConfig().build());
//        }

        backBtn = findViewById(R.id.exit_btn);
        edit = findViewById(R.id.edit);
        editText = findViewById(R.id.edit_window);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editText.isEnabled()) {
                    editText.setEnabled(false);
                    edit.setImageResource(R.drawable.edit_icon);

                    if (!rows.contains(editText.getText().toString())) {
                        rows.add(0, editText.getText().toString());

                        measuringAdapter.notifyDataSetChanged();

                        if (curSavePos > maxSaveSize)
                            curSavePos = 1;

                        primaryClip = ClipData.newPlainText("text", editText.getText().toString());
                        clipboardManager.setPrimaryClip(primaryClip);

                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString(("clipboard" + curSavePos), editText.getText().toString());
                        editor.apply();

                        curSavePos++;
                    }
                } else {
                    editText.setEnabled(true);
                    edit.setImageResource(R.drawable.save);

                    editText.requestFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
                }
            }
        });

        try {
            ActionBar actionBar = getSupportActionBar();
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        rows = new ArrayList<>();

        clipboardManager =  (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);

//        clipboardManager.addPrimaryClipChangedListener(new ClipboardManager.OnPrimaryClipChangedListener() {
//            @Override
//            public void onPrimaryClipChanged() {
//                rows.clear();
//
//                primaryClip = clipboardManager.getPrimaryClip();
//
//                for (int i = 0; i < primaryClip.getItemCount(); i++)
//                    rows.add(primaryClip.getItemAt(i).getText().toString());
//
//                if (rows.size() > 0)
//                    editText.setText(rows.get(0));
//
//                measuringAdapter.notifyDataSetChanged();
//            }
//        });

        primaryClip = clipboardManager.getPrimaryClip();

        if (primaryClip != null) {
            for (int i = 0; i < primaryClip.getItemCount(); i++)
                rows.add(primaryClip.getItemAt(i).getText().toString());

            if (rows.size() > 0)
                editText.setText(rows.get(0));
        }

        if (rows.size() == 0) {
            String tempStr = "";

            for (int i = 1; i < (maxSaveSize + 1); i++) {
                tempStr = sharedpreferences.getString(("clipboard" + i), "");

                if (!tempStr.equals(""))
                    rows.add(tempStr);
            }
        }

        measuringAdapter = new myListAdapter(getApplicationContext(), rows);

        final ListView listView = findViewById(R.id.clipboard_list);
        listView.setAdapter(measuringAdapter);
    }

    class myListAdapter extends BaseAdapter {
        private final LayoutInflater mLayoutInflater;
        private ArrayList<String> arrayMyMatches;

        public myListAdapter (Context ctx, ArrayList<String> arr) {
            mLayoutInflater = LayoutInflater.from(ctx);
            setArrayMyData(arr);
        }

        public ArrayList<String> getArrayMyData() {
            return arrayMyMatches;
        }

        public void setArrayMyData(ArrayList<String> arrayMyData) {
            this.arrayMyMatches = arrayMyData;
        }

        public boolean remove (int i) {
            arrayMyMatches.remove(i);

            return true;
        }

        public int getCount () {
            return arrayMyMatches.size();
        }

        public Object getItem (int position) {
            String app = arrayMyMatches.get(position);

            return app;
        }

        public long getItemId (int position) {
            return position;
        }

        //получение элемента ListView и его отправка в активность данных

        public View getView(final int position, View convertView, ViewGroup parent) {
            try {
                if (convertView == null) {
                    LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = inflater.inflate(R.layout.clipboard_manager_row, null, true);
                }

                ImageView copyBtn = convertView.findViewById(R.id.copy_btn);
                ImageView deleteBtn = convertView.findViewById(R.id.delete_btn);
                TextView text = convertView.findViewById(R.id.text);

                copyBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        primaryClip = ClipData.newPlainText("text", arrayMyMatches.get(position));
                        clipboardManager.setPrimaryClip(primaryClip);

                        editText.setText(arrayMyMatches.get(position));

                        Toast.makeText(ClipboardActivity.this, R.string.copied, Toast.LENGTH_SHORT).show();
                    }
                });

                deleteBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                            clipboardManager.clearPrimaryClip();
                        }
                        arrayMyMatches.remove(position);

                        SharedPreferences.Editor editor = sharedpreferences.edit();

                        for (int i = 1; i < (maxSaveSize + 1); i++) {
                            editor.putString(("clipboard" + i), "");

                            if (i <= arrayMyMatches.size())
                                editor.putString(("clipboard" + i), arrayMyMatches.get(i-1));
                        }

                        editor.apply();

                        ClipboardActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                measuringAdapter.notifyDataSetChanged();
                            }
                        });
                    }
                });

                text.setText(arrayMyMatches.get(position));

            } catch (Exception e) {

            } finally {
                return convertView;
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent startHomescreen = new Intent(getApplicationContext(), MenuActivity.class);
        startActivity(startHomescreen);

        AdsUtility.showInterstitial(ClipboardActivity.this);

        finish();
    }
}
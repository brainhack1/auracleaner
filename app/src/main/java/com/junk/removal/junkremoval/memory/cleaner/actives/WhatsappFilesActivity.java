package com.junk.removal.junkremoval.memory.cleaner.actives;

import android.Manifest;
import android.animation.ValueAnimator;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.core.app.ActivityCompat;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;


import com.junk.removal.junkremoval.memory.cleaner.actives.secondary.SuperActivity;
import com.junk.removal.junkremoval.memory.cleaner.obj.FileObj;
import com.google.android.gms.ads.AdRequest;
import com.junk.removal.junkremoval.memory.cleaner.R;
import com.junk.removal.junkremoval.memory.cleaner.utils.AdsUtility;

import static com.junk.removal.junkremoval.memory.cleaner.actives.BigFilesActivity.getFileImageByExtension;
import static com.junk.removal.junkremoval.memory.cleaner.actives.secondary.FragmentsActivity.RC_HANDLE_PERMISSIONA_ALL;
import static com.junk.removal.junkremoval.memory.cleaner.apps.Applic.isAdAllowed;
import static com.junk.removal.junkremoval.memory.cleaner.utils.Util.openFile;

public class WhatsappFilesActivity extends SuperActivity {

    public static myListAdapter adapterList;
    private ListView listOfElements;

    private AppsComponentsLoading appsComponentsLoading;

    private ImageView backBtn;
    private ProgressBar progressBar;
    private TextView allSizeText, allFilesText, cleanBtn;

    SharedPreferences sharedpreferences, configs;

//    private InterstitialAd mInterstitialAd;

    private ArrayList<FileObj> directories;
    private boolean poolOfElement;

    private int allFiles;
    private float allSize;

    private String deepPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        overridePendingTransition(R.anim.fast_fadein, 0);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        try {
            ActionBar actionBar = getSupportActionBar();
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        setContentView(R.layout.activity_whatsapp_files_checker);

        try {
            isAdAllowed = !getSharedPreferences("waseem", Context.MODE_PRIVATE).getBoolean(getResources().getString(R.string.is_pay_completed), false);
        } catch (Exception e) {

        }

        if (!(ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) {

            final String [] permission = new String[]{
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE};

            if(!hasPermissions(this, permission)){
                ActivityCompat.requestPermissions(this, permission, RC_HANDLE_PERMISSIONA_ALL);

                return;
            }
        } else {
//            if (isAdAllowed) {
////                mInterstitialAd = new InterstitialAd(WhatsappFilesActivity.this);
////                mInterstitialAd.setAdUnitId(getResources().getString(R.string.whatsapp_checker_screen));
////                AdRequest adRequestInter = new AdRequest.Builder().build();
////
////                mInterstitialAd.loadAd(adRequestInter);
//
//                mInterstitialAd = new InterstitialAd(this, getString(R.string.facebook_interstitial_ad_id));
//                mInterstitialAd.loadAd(mInterstitialAd.buildLoadAdConfig().build());
//            }

            backBtn = findViewById(R.id.exit_btn);
            backBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });

            progressBar = findViewById(R.id.progressBar);
            progressBar.getIndeterminateDrawable()
                    .setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN);

            cleanBtn = findViewById(R.id.clean_all);

            allSizeText = findViewById(R.id.allSizeText);
            allFilesText = findViewById(R.id.allFilesText);

            listOfElements = findViewById(R.id.apps_delete);

            sharedpreferences = getSharedPreferences("waseem", MODE_PRIVATE);
            configs = getSharedPreferences("Configs", MODE_PRIVATE);

            directories = new ArrayList<>();

            appsComponentsLoading = new AppsComponentsLoading();
            appsComponentsLoading.execute();
        }
    }

    private static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        try {
            if (requestCode == RC_HANDLE_PERMISSIONA_ALL) {

                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    finish();
                } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startActivity(new Intent(getApplicationContext(), WhatsappFilesActivity.class));
                    finish();
                }
            }
        } catch (Exception e) {
        }
    }

    private class AppsComponentsLoading extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if (adapterList != null) {
                directories.clear();
                adapterList.notifyDataSetChanged();
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {
            String whatsappPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/WhatsApp/Media";
            File folder = new File(whatsappPath);

            ArrayList<FileObj> directoriesTemplate = new ArrayList<>();
            checkFiles(folder, directoriesTemplate);

            directories = directoriesTemplate;

            return null;
        }

        @Override
        protected void onPostExecute(final Void aVoid) {
            super.onPostExecute(aVoid);

            findViewById(R.id.files_info).setVisibility(View.VISIBLE);
            findViewById(R.id.where_listView_relative).setVisibility(View.VISIBLE);

            allSizeText.setVisibility(View.VISIBLE);

            allFiles = 0;
            allSize = 0;

            for (FileObj fileObject : directories) {
                allFiles += fileObject.getFilesInside();
                allSize += fileObject.getSize();
            }

            adapterList = new myListAdapter(getApplicationContext(), directories);
            listOfElements.setAdapter(adapterList);

            adapterList.notifyDataSetChanged();

            progressBar.setVisibility(View.GONE);
            cleanBtn.setVisibility(View.VISIBLE);

            cleanBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(WhatsappFilesActivity.this);
                    AlertDialog dialog = builder.setTitle(getResources().getString(R.string.delete_all_files_title)).setMessage(getResources().getString(R.string.delete_all_files_content))
                            .setCancelable(true)
                            .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    if (poolOfElement) {
                                        for (FileObj fileObject : directories)
                                            deleteRecursive(new File(fileObject.getPath()));

                                        directories.clear();
                                        adapterList.notifyDataSetChanged();

                                        deleteRecursive(new File(deepPath));

                                        onBackPressed();
                                    } else {
                                        for (FileObj fileObject : directories)
                                            deleteRecursive(new File(fileObject.getPath()));

                                        directories.clear();
                                        findViewById(R.id.files_info).setVisibility(View.GONE);
                                        findViewById(R.id.where_listView_relative).setVisibility(View.GONE);
                                        cleanBtn.setVisibility(View.GONE);

                                        adapterList.notifyDataSetChanged();

                                        showEndWord();
                                    }

                                    dialog.cancel();
                                }
                            }).setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            }
                    ).create();

                    dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                        @Override
                        public void onShow(DialogInterface dialogInterface) {
                            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.RED);
                            dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.GRAY);
                        }
                    });

                    dialog.show();
                }
            });
        }
    }

    //работа с отображением элементов ListView

    class myListAdapter extends BaseAdapter {
        private final LayoutInflater mLayoutInflater;
        private ArrayList<FileObj> arrayMyMatches;

        public myListAdapter (Context ctx, ArrayList<FileObj> arr) {
            mLayoutInflater = LayoutInflater.from(ctx);
            setArrayMyData(arr);
        }

        public ArrayList<FileObj> getArrayMyData() {
            return arrayMyMatches;
        }

        public void setArrayMyData(ArrayList<FileObj> arrayMyData) {
            this.arrayMyMatches = arrayMyData;
        }

        public boolean remove (int i) {
            arrayMyMatches.remove(i);

            return true;
        }

        public int getCount () {
            return arrayMyMatches.size();
        }

        public Object getItem (int position) {
            FileObj app = arrayMyMatches.get(position);

            return app;
        }

        @Override
        public void notifyDataSetChanged() {
            super.notifyDataSetChanged();

            allFilesText.setText(allFiles + " " + getString(R.string.files_detected));
            animateTextView(0, Math.round(allSize), allSizeText);
        }

        public long getItemId (int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            try {
                if (!poolOfElement) {
                    if (convertView == null) {
                        LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        convertView = inflater.inflate(R.layout.whatsapp_init_row, null, true);
                    }

                    ImageView app_image = convertView.findViewById(R.id.app_image);
                    TextView app_name = convertView.findViewById(R.id.name);
                    TextView files_inside = convertView.findViewById(R.id.files_count);
                    RelativeLayout removeBtn = convertView.findViewById(R.id.remove_btn);
                    LinearLayout whatsappRow = convertView.findViewById(R.id.whatsapp_row);

                    files_inside.setText(arrayMyMatches.get(position).getFilesInside() + " " + getString(R.string.files_detected));

                    switch (arrayMyMatches.get(position).getName()) {
                        case "WallPaper":
                            app_image.setImageDrawable(getResources().getDrawable(R.drawable.watsapp_wallpaper));

                            app_name.setText(getString(R.string.whatsapp_wallpaper));
                            break;
                        case "WhatsApp Animated Gifs":
                            app_image.setImageDrawable(getResources().getDrawable(R.drawable.whatsapp_gif));

                            app_name.setText(getString(R.string.whatsapp_gifs));
                            break;
                        case "WhatsApp Audio":
                            app_image.setImageDrawable(getResources().getDrawable(R.drawable.watsapp_audio));

                            app_name.setText(getString(R.string.whatsapp_audio));
                            break;
                        case "WhatsApp Documents":
                            app_image.setImageDrawable(getResources().getDrawable(R.drawable.whatsapp_document));

                            app_name.setText(getString(R.string.whatsapp_documents));
                            break;
                        case "WhatsApp Images":
                            app_image.setImageDrawable(getResources().getDrawable(R.drawable.watsapp_pictures));

                            app_name.setText(getString(R.string.whatsapp_images));
                            break;
                        case "WhatsApp Profile Photos":
                            app_image.setImageDrawable(getResources().getDrawable(R.drawable.watsapp_profile));

                            app_name.setText(getString(R.string.whatsapp_profiles));
                            break;
                        case "WhatsApp Stickers":
                            app_image.setImageDrawable(getResources().getDrawable(R.drawable.whatsapp_stickers));

                            app_name.setText(getString(R.string.whatsapp_stickers));
                            break;
                        case "WhatsApp Video":
                            app_image.setImageDrawable(getResources().getDrawable(R.drawable.watsapp_video));

                            app_name.setText(getString(R.string.whatsapp_video));
                            break;
                        case "WhatsApp Voice Notes":
                            app_image.setImageDrawable(getResources().getDrawable(R.drawable.watsapp_voices));

                            app_name.setText(getString(R.string.whatsapp_notes));
                            break;
                        default:
                            app_image.setImageDrawable(getResources().getDrawable(getFileImageByExtension(arrayMyMatches.get(position).getType())));

                            app_name.setText(getString(R.string.whatsapp_other));
                    }

                    whatsappRow.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (arrayMyMatches.get(position).getFilesInside() > 0) {
                                poolOfElement = true;

                                if (arrayMyMatches.get(position).getName().equals("WhatsApp Images")) {
                                    Intent imageStockerActivityIntent = new Intent(getApplicationContext(), ImageSelectorActivity.class);
                                    imageStockerActivityIntent.putExtra("path", Environment.getExternalStorageDirectory().getAbsolutePath() + "/WhatsApp/Media/WhatsApp Images");
                                    startActivity(imageStockerActivityIntent);

                                    finish();
                                } else {
                                    deepPath = arrayMyMatches.get(position).getPath();

                                    allSizeText.setVisibility(View.INVISIBLE);

                                    directories.clear();
                                    adapterList.notifyDataSetChanged();

                                    ArrayList<FileObj> directoriesTemplate = new ArrayList<>();
                                    checkFiles(new File(deepPath), directoriesTemplate);

                                    directories.addAll(directoriesTemplate);

                                    allFiles = directories.size();

                                    adapterList.notifyDataSetChanged();
                                }
                            }
                        }
                    });

                    final TextView size = convertView.findViewById(R.id.size);
                    size.setText(arrayMyMatches.get(position).getSize() + " MB");

                    removeBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            File dir = new File(arrayMyMatches.get(position).getPath());

                            deleteRecursive(dir);

                            WhatsappFilesActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    allFiles -= arrayMyMatches.get(position).getFilesInside();
                                    allSize -= arrayMyMatches.get(position).getSize();

                                    arrayMyMatches.remove(position);
                                    adapterList.notifyDataSetChanged();

                                    if (arrayMyMatches.isEmpty())
                                        showEndWord();
                                }
                            });
                        }
                    });
                } else {
                    if (convertView == null) {
                        LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        convertView = inflater.inflate(R.layout.init_file_row, null, true);
                    }

                    LinearLayout whatsappRow = convertView.findViewById(R.id.whatsapp_row);

                    whatsappRow.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            openFile(new File(directories.get(position).getPath()), WhatsappFilesActivity.this);
                        }
                    });

                    ImageView app_image = convertView.findViewById(R.id.app_image);
                    TextView app_name = convertView.findViewById(R.id.name);
                    RelativeLayout removeBtn = convertView.findViewById(R.id.remove_btn);

                    final TextView size = convertView.findViewById(R.id.size);
                    size.setText(arrayMyMatches.get(position).getSize() + " MB");

                    app_name.setText(arrayMyMatches.get(position).getName());

                    app_image.setImageDrawable(getResources().getDrawable(getFileImageByExtension(arrayMyMatches.get(position).getType())));

                    removeBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            allFiles -= arrayMyMatches.get(position).getFilesInside();
                            allSize -= arrayMyMatches.get(position).getSize();

                            //путь к определенной папке
                            File dir = new File(arrayMyMatches.get(position).getPath());

                            if (poolOfElement)
                                allFiles--;

                            //удаляем папку
                            deleteRecursive(dir);

                            arrayMyMatches.remove(position);
                            adapterList.notifyDataSetChanged();

                            if (arrayMyMatches.isEmpty()) {
                                deleteRecursive(new File(deepPath));

                                onBackPressed();
                            }
                        }
                    });
                }
            } catch (Exception e) {

            } finally {
                return convertView;
            }
        }
    }

    private void deleteRecursive(File fileOrDirectory) {
        try {
            if (fileOrDirectory.isDirectory())
                for (File child : fileOrDirectory.listFiles())
                    deleteRecursive(child);

            fileOrDirectory.delete();
        } catch (Exception e) {}
    }

    private static long dirSize(File dir) {
        if (dir.exists()) {
            long result = 0;
            File[] fileList = dir.listFiles();
            for(int i = 0; i < fileList.length; i++) {
                // Recursive call if it's a directory
                if(fileList[i].isDirectory()) {
                    result += dirSize(fileList[i]);
                } else {
                    // Sum the file size in bytes
                    result += fileList[i].length();
                }
            }
            return result; // return the file size
        }
        return 0;
    }

    //метод обработки всех папок на устройстве

    private void checkFiles(File parentDir, ArrayList<FileObj> template) {
        File[] files = parentDir.listFiles();

        try {
            if (files != null)
                for (File file : files) {
                    if (file.isDirectory() && !file.isHidden() && !poolOfElement && file.listFiles().length > 0) {
                        FileObj fileObject = new FileObj(file.getName(), file.getPath(), (float)(Math.round((float) dirSize(file) / 1024 / 1024 * 10))/10);

                        if (fileObject.getSize() == 0)
                            fileObject.setSize(1.0F);

                        fileObject.setFilesInside(file.listFiles().length);
                        template.add(fileObject);
                    } else if (poolOfElement) {
                        if (file.isDirectory()) {
                            checkFiles(file, template);
                        } else {
                            float size = (float)(Math.round((float) file.length() / 1024 / 1024 * 10))/10;
                            FileObj fileObject = new FileObj(file.getName(), file.getPath(), size);
                            fileObject.setFilesInside(file.listFiles() != null ? file.listFiles().length : 0);

                            String extension = "";

                            if (file.getAbsolutePath().lastIndexOf(".") != -1)
                                extension = file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf("."));

                            fileObject.setType(extension);

                            if (fileObject.getSize() == 0)
                                fileObject.setSize(1.0F);

                            template.add(fileObject);
                        }
                    }
                }
        } catch (Exception e) {}
    }

    private void showEndWord() {
        findViewById(R.id.files_info).setVisibility(View.GONE);
        findViewById(R.id.where_listView_relative).setVisibility(View.GONE);

        findViewById(R.id.endWord).setVisibility(View.VISIBLE);

        AdsUtility.showInterstitial(WhatsappFilesActivity.this);
    }

    public void animateTextView(int initialValue, int finalValue, final TextView  textview) {

        ValueAnimator valueAnimator = ValueAnimator.ofInt(initialValue, finalValue);
        valueAnimator.setDuration(1500);

        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {

                textview.setText(valueAnimator.getAnimatedValue() + " " + getString(R.string.mb));

            }
        });
        valueAnimator.start();
    }

    @Override
    public boolean onKeyDown(final int pKeyCode, final KeyEvent pEvent) {
        if(pKeyCode == KeyEvent.KEYCODE_BACK && pEvent.getAction() == KeyEvent.ACTION_DOWN) {
            onBackPressed();

            return true;
        } else {
            return super.onKeyDown(pKeyCode, pEvent);
        }
    }

    @Override
    public void onBackPressed() {

        if (poolOfElement) {
            poolOfElement = false;

            findViewById(R.id.files_info).setVisibility(View.GONE);
            findViewById(R.id.where_listView_relative).setVisibility(View.GONE);

            progressBar.setVisibility(View.VISIBLE);

            appsComponentsLoading = new AppsComponentsLoading();
            appsComponentsLoading.execute ();
        } else {
            AdsUtility.showMrec(R.id.exit_banner, this);

            RelativeLayout exitDialog = findViewById(R.id.exit_dialog);
            exitDialog.setVisibility(View.VISIBLE);

            findViewById(R.id.yes).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(getApplicationContext(), MenuActivity.class));

                    AdsUtility.showInterstitial(WhatsappFilesActivity.this);

                    finish();
                }
            });

            findViewById(R.id.no).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    exitDialog.setVisibility(View.GONE);
                }
            });
        }
    }
}

package com.junk.removal.junkremoval.memory.cleaner.actives;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.AsyncTask;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.junk.removal.junkremoval.memory.cleaner.actives.secondary.SuperActivity;
import com.junk.removal.junkremoval.memory.cleaner.obj.AppInit;
import com.google.android.gms.ads.AdRequest;
import com.junk.removal.junkremoval.memory.cleaner.R;
import com.junk.removal.junkremoval.memory.cleaner.utils.AdsUtility;

import static com.junk.removal.junkremoval.memory.cleaner.apps.Applic.isAdAllowed;

public class RemoveAppsActivity extends SuperActivity {

    public static myListAdapter adapterOfList;

    private TextView by_name, by_date, by_size;
    private ProgressBar progressBar;

    private ArrayList<AppInit> apps;
    private AppsComponentsLoading appsComponentsLoading;

//    private InterstitialAd mInterstitialAd;

    private int allSize, allAppsCount;

    private ImageView backBtn;

    private AppInit queryDeleteApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        overridePendingTransition(R.anim.fast_fadein, 0);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.loading_screen);

        try {
            isAdAllowed = !getSharedPreferences("waseem", Context.MODE_PRIVATE).getBoolean(getResources().getString(R.string.is_pay_completed), false);
        } catch (Exception e) {

        }

        try {
            ActionBar actionBar = getSupportActionBar();
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        progressBar = findViewById(R.id.progressBar);

        progressBar.getIndeterminateDrawable()
                .setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN);

        appsComponentsLoading = new AppsComponentsLoading();
        appsComponentsLoading.execute ();
    }

    private class AppsComponentsLoading extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            setContentView(R.layout.activity_apps_removing);

            backBtn = findViewById(R.id.exit_btn);
            backBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });

            by_name = findViewById(R.id.by_name);
            by_date = findViewById(R.id.by_date);
            by_size = findViewById(R.id.by_size);

            Collections.sort(apps, new Comparator<AppInit>() {
                @Override
                public int compare(AppInit initApp, AppInit t1) {
                    if (initApp.getAppDateMillis() < t1.getAppDateMillis())
                        return 1;
                    else if (initApp.getAppDateMillis() > t1.getAppDateMillis())
                        return -1;

                    return 0;
                }
            });

            by_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    by_name.setTextColor(Color.parseColor("#FFEB3B"));
                    by_size.setTextColor(getResources().getColor(android.R.color.white));
                    by_date.setTextColor(getResources().getColor(android.R.color.white));

                    Collections.sort(apps, new Comparator<AppInit>() {
                        @Override
                        public int compare(AppInit initApp, AppInit t1) {
                            return initApp.getApp_name().compareTo(t1.getApp_name());
                        }
                    });

                    RemoveAppsActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapterOfList.notifyDataSetChanged();
                        }
                    });
                }
            });

            by_date.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    by_name.setTextColor(getResources().getColor(android.R.color.white));
                    by_size.setTextColor(getResources().getColor(android.R.color.white));
                    by_date.setTextColor(Color.parseColor("#FFEB3B"));

                    Collections.sort(apps, new Comparator<AppInit>() {
                        @Override
                        public int compare(AppInit initApp, AppInit t1) {
                            if (initApp.getAppDateMillis() < t1.getAppDateMillis())
                                return 1;
                            else if (initApp.getAppDateMillis() > t1.getAppDateMillis())
                                return -1;

                            return 0;
                        }
                    });

                    RemoveAppsActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapterOfList.notifyDataSetChanged();
                        }
                    });
                }
            });

            by_size.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    by_name.setTextColor(getResources().getColor(android.R.color.white));
                    by_date.setTextColor(getResources().getColor(android.R.color.white));
                    by_size.setTextColor(Color.parseColor("#FFEB3B"));

                    Collections.sort(apps, new Comparator<AppInit>() {
                        @Override
                        public int compare(AppInit initApp, AppInit t1) {
                            if (initApp.getAppSize() < t1.getAppSize())
                                return 1;
                            else if (initApp.getAppSize() > t1.getAppSize())
                                return -1;

                            return 0;
                        }
                    });

                    RemoveAppsActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapterOfList.notifyDataSetChanged();
                        }
                    });
                }
            });

            final ListView listView = findViewById(R.id.apps_delete);
            listView.setAdapter(adapterOfList);

            allSize = 0;
            allAppsCount = 0;

            for(AppInit app : apps) {
                allSize += app.getAppSize();
                allAppsCount++;
            }

            TextView appsSize = findViewById(R.id.appsSize);
            animateTextView(0, allSize, appsSize);

            TextView allApps = findViewById(R.id.allApps);
            allApps.setText(allAppsCount + " " + getString(R.string.apps));
        }

        @Override
        protected Void doInBackground(Void... voids) {
            apps = new ArrayList<>();

            PackageManager packageManager = getPackageManager();
            Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
            mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);

            List<ResolveInfo> appList = packageManager.queryIntentActivities(mainIntent, 0);
            Collections.sort(appList, new ResolveInfo.DisplayNameComparator(packageManager));

            for (int i = 0; i < appList.size(); i++) {
                if (appList.get(i).activityInfo.packageName.equals(getPackageName()))
                    continue;

                apps.add(new AppInit(appList.get(i).loadIcon(getPackageManager()),
                        (String) appList.get(i).loadLabel(getPackageManager()), appList.get(i).activityInfo.packageName, false));
                try {
                    ApplicationInfo applicationInfo = packageManager.getApplicationInfo(appList.get(i).activityInfo.packageName, 0);
                    File file = new File(applicationInfo.publicSourceDir);
                    apps.get(apps.size() - 1).setAppSize((int)(file.length()/(1024*1024)));

                    PackageInfo packageInfo = packageManager.getPackageInfo(applicationInfo.packageName, 0);
                    long installTimeInMilliseconds = packageInfo.firstInstallTime;

                    apps.get(apps.size() - 1).setAppDate(getInstallDate(installTimeInMilliseconds, applicationInfo.packageName));
                    apps.get(apps.size() - 1).setAppDateMillis(installTimeInMilliseconds);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
            }

            adapterOfList = new myListAdapter(getApplicationContext(), apps);

            return null;
        }
    }

    private String getInstallDate(long installTimeInMilliseconds, String packageName) {
        // install time is conveniently provided in milliseconds

        Date installDate = null;
        String installDateString = null;

        try {

            Calendar mydate = Calendar.getInstance();
            mydate.setTimeInMillis(installTimeInMilliseconds);

            installDateString  = (mydate.get(Calendar.DAY_OF_MONTH)+"."+mydate.get(Calendar.MONTH)+"."+mydate.get(Calendar.YEAR));
        }
        catch (Exception e) {
            // an error occurred, so display the Unix epoch
            installDate = new Date(0);
            installDateString = installDate.toString();
        }

        return installDateString;
    }

    //работа с отображением элементов ListView

    class myListAdapter extends BaseAdapter {
        private final LayoutInflater mLayoutInflater;
        private ArrayList<AppInit> arrayMyMatches;

        public myListAdapter (Context ctx, ArrayList<AppInit> arr) {
            mLayoutInflater = LayoutInflater.from(ctx);
            setArrayMyData(arr);
        }

        public ArrayList<AppInit> getArrayMyData() {
            return arrayMyMatches;
        }

        public void setArrayMyData(ArrayList<AppInit> arrayMyData) {
            this.arrayMyMatches = arrayMyData;
        }

        public boolean remove (int i) {
            arrayMyMatches.remove(i);

            return true;
        }

        public int getCount () {
            return arrayMyMatches.size();
        }

        public Object getItem (int position) {
            AppInit app = arrayMyMatches.get(position);

            return app;
        }

        public long getItemId (int position) {
            return position;
        }

        //получение элемента ListView и его отправка в активность данных

        public View getView(final int position, View convertView, ViewGroup parent) {
            try {
                if (convertView == null) {
                    LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = inflater.inflate(R.layout.init_app_row, null, true);
                }

                ImageView app_image = convertView.findViewById(R.id.app_image);
                TextView app_name = convertView.findViewById(R.id.name);
                ImageView removeBtn = convertView.findViewById(R.id.remove_btn);

                TextView size = convertView.findViewById(R.id.size);

                removeBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        queryDeleteApp = apps.get(position);

                        Intent intent = new Intent(Intent.ACTION_UNINSTALL_PACKAGE);
                        intent.setData(Uri.fromParts("package", adapterOfList.getArrayMyData().get(position).getApp_package(), null));
                        intent.putExtra(Intent.EXTRA_RETURN_RESULT, true);
                        startActivityForResult(intent, 1);
                    }
                });

                app_image.setImageDrawable(arrayMyMatches.get(position).getApp_image());
                app_name.setText(arrayMyMatches.get(position).getApp_name());

                size.setText(arrayMyMatches.get(position).getAppSize() + " MB");

            } catch (Exception e) {

            } finally {
                return convertView;
            }
        }
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, @Nullable final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1)
            if (resultCode == RESULT_OK) {
                if (queryDeleteApp != null)
                apps.remove(queryDeleteApp);
                adapterOfList.notifyDataSetChanged();

                allSize = 0;
                allAppsCount = 0;

                for(AppInit app : apps) {
                    allSize += app.getAppSize();
                    allAppsCount++;
                }

                TextView appsSize = findViewById(R.id.appsSize);
                appsSize.setText(allSize + " " + getString(R.string.mb));
                TextView allApps = findViewById(R.id.allApps);
                allApps.setText(allAppsCount + " " + getString(R.string.apps));
            }
    }

    public void animateTextView(int initialValue, int finalValue, final TextView  textview) {

        ValueAnimator valueAnimator = ValueAnimator.ofInt(initialValue, finalValue);
        valueAnimator.setDuration(1500);

        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {

                textview.setText(valueAnimator.getAnimatedValue() + " " + getString(R.string.mb));

            }
        });
        valueAnimator.start();
    }

    @Override
    public void onBackPressed() {
        AdsUtility.showMrec(R.id.exit_banner, this);

        try {
            RelativeLayout exitDialog = (RelativeLayout) findViewById(R.id.exit_dialog);
            exitDialog.setVisibility(View.VISIBLE);

            findViewById(R.id.yes).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(getApplicationContext(), MenuActivity.class));

                    AdsUtility.showInterstitial(RemoveAppsActivity.this);

                    finish();
                }
            });

            findViewById(R.id.no).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    exitDialog.setVisibility(View.GONE);
                }
            });
        } catch (Exception e) {}
    }
}

package com.junk.removal.junkremoval.memory.cleaner.actives.secondary;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;

import androidx.appcompat.app.AppCompatActivity;

import com.junk.removal.junkremoval.memory.cleaner.R;

import java.util.Locale;

public class SuperActivity extends AppCompatActivity {
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);

        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        final String lang = sharedPreferences.getString("lang", "");

        if (!lang.equals("")) {
            Locale myLocale = new Locale(lang);
            Resources res = getResources();
            DisplayMetrics dm = res.getDisplayMetrics();
            Configuration conf = res.getConfiguration();
            conf.locale = myLocale;
            res.updateConfiguration(conf, dm);
        }
    }

    @Override
    public void finish() {
        overridePendingTransition(R.anim.fast_fadein, R.anim.fadeout);

        super.finish();
    }
}

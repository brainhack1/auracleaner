package com.junk.removal.junkremoval.memory.cleaner.actives;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.android.billingclient.api.AcknowledgePurchaseParams;
import com.android.billingclient.api.AcknowledgePurchaseResponseListener;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesResponseListener;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.junk.removal.junkremoval.memory.cleaner.R;
import com.junk.removal.junkremoval.memory.cleaner.actives.secondary.SuperActivity;
import com.junk.removal.junkremoval.memory.cleaner.apps.Applic;
import com.junk.removal.junkremoval.memory.cleaner.receiv.AlarmNotif;
import com.junk.removal.junkremoval.memory.cleaner.utils.AdsUtility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import static com.junk.removal.junkremoval.memory.cleaner.apps.Applic.isAdAllowed;

public class InitialScreenActivity extends SuperActivity {

    public static boolean isCleanRequired = true;

    private Timer interstitialTimer;
    int times = 20;

    private BillingClient billingClient;
    private final Map<String, SkuDetails> mSkuDetailsMap = new HashMap<>();
    private final String monthSku = "subsmonth";
    private final String threeMonthSku = "substhreemonth";
    private final String yearSku = "subsyear";

    private boolean isSubsActive;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.splash_screen);

        RotateAnimation rotate = new RotateAnimation(0, 360, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(1500);
        rotate.setRepeatCount(15);
        rotate.setInterpolator(new LinearInterpolator());

        findViewById(R.id.front).setAnimation(rotate);

        //MARK: - Setup Appodeal

        AdsUtility.initialize(this, false);

        SharedPreferences preferences = getSharedPreferences(getResources().getString(R.string.app_preferences), MODE_PRIVATE);
        SharedPreferences.Editor preferencesEditor = preferences.edit();

        isCleanRequired = preferences.getLong(getString(R.string.lastTimeCleaned), 0) < System.currentTimeMillis() - 5 * 60 * 1000;

        animationStart();

        billingClient = BillingClient.newBuilder(this).setListener(new PurchasesUpdatedListener() {
            @Override
            public void onPurchasesUpdated(@NonNull BillingResult billingResult, @Nullable List<Purchase> list) {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK && list != null) {
                    //сюда мы попадем когда будет осуществлена покупка

                    if (list != null)
                        for (Purchase purchase : list) {
                            AcknowledgePurchaseParams acknowledgePurchaseParams =
                                    AcknowledgePurchaseParams.newBuilder()
                                            .setPurchaseToken(purchase.getPurchaseToken())
                                            .build();

                            AcknowledgePurchaseResponseListener acknowledgePurchaseResponseListener = new AcknowledgePurchaseResponseListener() {
                                @Override
                                public void onAcknowledgePurchaseResponse(BillingResult billingResult) {
                                    BillingResult billingResult1 = billingResult;
                                }

                            };

                            billingClient.acknowledgePurchase(acknowledgePurchaseParams, acknowledgePurchaseResponseListener);
                        }

                    payComplete();
                }
            }
        }).enablePendingPurchases().build();

        billingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(@NonNull BillingResult billingResult) {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    //здесь мы можем запросить информацию о товарах и покупках

                    querySkuDetails();

                    billingClient.queryPurchasesAsync(BillingClient.SkuType.SUBS, new PurchasesResponseListener() {
                        @Override
                        public void onQueryPurchasesResponse(@NonNull BillingResult billingResult, @NonNull List<Purchase> purchasesList) {
                            if (purchasesList != null) {
                                //если товар уже куплен, предоставить его пользователю
                                for (int i = 0; i < purchasesList.size(); i++) {
                                    String purchaseId = purchasesList.get(i).getSkus().get(0);
                                    if (purchasesList.get(i).getSkus().contains(monthSku) || purchasesList.get(i).getSkus().contains(threeMonthSku) || purchasesList.get(i).getSkus().contains(yearSku)) {
                                        payComplete();
                                    }
                                }
                            }
                        }
                    });

                    if (!isSubsActive) {
                        isAdAllowed = true;
                    }
                }
            }

            @Override
            public void onBillingServiceDisconnected() {
                //сюда мы попадем если что-то пойдет не так
            }
        });

        SharedPreferences.Editor editor = getSharedPreferences("waseem", Context.MODE_PRIVATE).edit();
        editor.putInt("userEntranceCount", getSharedPreferences("waseem", Context.MODE_PRIVATE).getInt("userEntranceCount", 0) + 1);
        editor.apply();

        if (getSharedPreferences("waseem", Context.MODE_PRIVATE).getInt("userEntranceCount", 1) == 1) {
            Intent notificationIntent = new Intent(InitialScreenActivity.this, AlarmNotif.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager am = (AlarmManager)getSystemService(ALARM_SERVICE);
            am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
        }
    }

    private void querySkuDetails() {
        SkuDetailsParams.Builder skuDetailsParamsBuilder = SkuDetailsParams.newBuilder();
        List<String> skuList = new ArrayList<>();
        skuList.add(monthSku);
        skuList.add(threeMonthSku);
        skuList.add(yearSku);
        skuDetailsParamsBuilder.setSkusList(skuList).setType(BillingClient.SkuType.SUBS);
        billingClient.querySkuDetailsAsync(skuDetailsParamsBuilder.build(), new SkuDetailsResponseListener() {
            @Override
            public void onSkuDetailsResponse(@NonNull BillingResult billingResult, @Nullable List<SkuDetails> list) {
                if (billingResult.getResponseCode() == 0) {
                    for (SkuDetails skuDetails : list) {
                        mSkuDetailsMap.put(skuDetails.getSku(), skuDetails);
                    }
                }
            }
        });
    }

    private void payComplete() {
        isSubsActive = true;
        isAdAllowed = false;

        times = 6;
    }

    private void animationStart() {


        interstTimerWait();
    }

    private void interstTimerWait() {
        interstitialTimer = new Timer();
        final int[] timer = {0};

        interstitialTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
//                        if (timer[0] > 6 && AdsUtility.mInterstitialAd.isLoaded()) {
//
//                            menuStart();
//
//                            AdsUtility.showInterstitial(InitialScreenActivity.this);
//
//                            interstitialTimer.cancel();
//                        } else
                            if (timer[0] >= times) {
                            menuStart();

                            interstitialTimer.cancel();
                        }
                        timer[0]++;
                    }
                });
            }
        }, 0, 500);
    }

    public void menuStart() {
        if (isAdAllowed && getSharedPreferences("waseem", Context.MODE_PRIVATE).getInt("userEntranceCount", 0) == 3) {
            SharedPreferences.Editor editor = getSharedPreferences("waseem", Context.MODE_PRIVATE).edit();
            editor.putInt("userEntranceCount", getSharedPreferences("waseem", Context.MODE_PRIVATE).getInt("userEntranceCount", 0) + 1);
            editor.apply();

            startActivity(new Intent(getApplicationContext(), SubsActivity.class));

            overridePendingTransition(R.anim.fadein, R.anim.fadeout);

            finish();
        } else {
            Intent intent = new Intent(InitialScreenActivity.this, MenuActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
        }
    }
}
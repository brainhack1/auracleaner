package com.junk.removal.junkremoval.memory.cleaner.notific;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.widget.RemoteViews;

import androidx.core.app.NotificationCompat;

import com.junk.removal.junkremoval.memory.cleaner.R;
import com.junk.removal.junkremoval.memory.cleaner.actives.RemoveAppsActivity;
import com.junk.removal.junkremoval.memory.cleaner.actives.SaverBatActivity;
import com.junk.removal.junkremoval.memory.cleaner.actives.JunksDataRemovingActivity;
import com.junk.removal.junkremoval.memory.cleaner.actives.ClipboardActivity;
import com.junk.removal.junkremoval.memory.cleaner.actives.PreferencesActivity;
import com.junk.removal.junkremoval.memory.cleaner.actives.SpeedBoosterActivity;

import static android.content.Context.NOTIFICATION_SERVICE;

public class Util_Ongoing_Notif {

    public static int ONGOING_NOTIFICATION_ID = 501;

    public static void showNotification(Context context) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);

        String channelId = "ongoing channel";

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context.getApplicationContext(), channelId);

        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.ongoing_notification);

        Notification ongoing  = mBuilder
                .setSmallIcon(R.drawable.logo) //set your icon
                .setPriority(Notification.PRIORITY_HIGH)
                .setOngoing(true)
                .setNotificationSilent()
                .setCustomContentView(remoteViews)
                .build();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            NotificationChannel channel = new NotificationChannel(
                    channelId,
                    channelId,
                    NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(channel);
            mBuilder.setChannelId(channelId);
        }

        PendingIntent cleanPending = PendingIntent.getActivity(context, 0, new Intent(context, JunksDataRemovingActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.clean, cleanPending);

        PendingIntent chargePending = PendingIntent.getActivity(context, 0, new Intent (context, SaverBatActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.charge, chargePending);

        PendingIntent appsPending = PendingIntent.getActivity(context, 0, new Intent(context, RemoveAppsActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.apps, appsPending);

        PendingIntent boostPending = PendingIntent.getActivity(context, 0, new Intent (context, SpeedBoosterActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.boost, boostPending);

        PendingIntent clipboardPending = PendingIntent.getActivity(context, 0, new Intent(context, ClipboardActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.clipboard, clipboardPending);

        PendingIntent settingsPending = PendingIntent.getActivity(context, 0, new Intent(context, PreferencesActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.settings, settingsPending);

        notificationManager.notify(ONGOING_NOTIFICATION_ID, ongoing);
    }
}

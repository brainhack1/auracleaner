package com.junk.removal.junkremoval.memory.cleaner.actives;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.junk.removal.junkremoval.memory.cleaner.R;
import com.junk.removal.junkremoval.memory.cleaner.actives.secondary.SuperActivity;

public class SupportActivity extends SuperActivity {

    //here all users can send their mind for developers feedback

    public final String DEVELOPER_EMAIL = "ivlevapps@gmail.com";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        overridePendingTransition(R.anim.fast_fadein, 0);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_developers_support);

        findViewById(R.id.sendFeedBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    final EditText feedbackField = findViewById(R.id.editFeedbackMessage);
                    String feedback = feedbackField.getText().toString();

                    if (feedback == "" || feedback.length() < 1) {
                        Toast.makeText(getApplicationContext(), "Fill in all rows", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    shareToGMail(new String[] {DEVELOPER_EMAIL}, feedback);
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void shareToGMail(String[] email, String content) {
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.putExtra(Intent.EXTRA_EMAIL, email);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Appeal Aura Cleaner");
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_TEXT, content);
        startActivity(emailIntent);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(getApplicationContext(), MenuActivity.class));
        finish();

        super.onBackPressed();
    }
}

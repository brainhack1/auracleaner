package com.junk.removal.junkremoval.memory.cleaner.actives;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.junk.removal.junkremoval.memory.cleaner.R;
import com.junk.removal.junkremoval.memory.cleaner.actives.secondary.SuperActivity;
import com.junk.removal.junkremoval.memory.cleaner.obj.AppInit;
import com.junk.removal.junkremoval.memory.cleaner.utils.AdsUtility;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.junk.removal.junkremoval.memory.cleaner.apps.Applic.isAdAllowed;

public class SpeedBoosterActivity extends SuperActivity {

    private LinearLayout optimizebutton;
    private TextView processes, appsToOptimize, appsSize;
    private SharedPreferences sharedpreferences;
    private SharedPreferences.Editor editor;

//    private InterstitialAd mInterstitialAd;

    private ArrayList<AppInit> apps;
    private ImageView backBtn;
    private myListAdapter measuringAdapter;
    private ListView appsList;

    private int runningApps, allSize;
    private boolean isExitAvaliable = true;

    private final long TIME_TO_CLEAN = 5*60*1000;

    private LinearLayout progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        overridePendingTransition(R.anim.fast_fadein, 0);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_phone_booster);

        try {
            isAdAllowed = !getSharedPreferences("waseem", Context.MODE_PRIVATE).getBoolean(getResources().getString(R.string.is_pay_completed), false);
        } catch (Exception e) {

        }

        backBtn = findViewById(R.id.exit_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        optimizebutton = findViewById(R.id.optbutton);
        processes = findViewById(R.id.processes);
        appsToOptimize = findViewById(R.id.apps_to_optimize);
        appsSize = findViewById(R.id.appsSize);
        appsList = findViewById(R.id.apps);
        sharedpreferences = getSharedPreferences("waseem", Context.MODE_PRIVATE);

//        if (isAdAllowed) {
////            mInterstitialAd = new InterstitialAd(getApplicationContext());
////
////            mInterstitialAd.setAdUnitId(getResources().getString(R.string.phone_booster_screen));
////            AdRequest adRequestInter = new AdRequest.Builder().build();
////
////            mInterstitialAd.loadAd(adRequestInter);
//
//            mInterstitialAd = new InterstitialAd(this, getString(R.string.facebook_interstitial_ad_id));
//            mInterstitialAd.loadAd(mInterstitialAd.buildLoadAdConfig().build());
//        }

        if ((System.currentTimeMillis() - sharedpreferences.getLong("lastBoostTime", 0) > TIME_TO_CLEAN)) {
            runningApps = (int) (Math.random() * 10 + 25);

            processes.setText(runningApps + "");
            appsToOptimize.setText("(" + runningApps + " " + getString(R.string.apps) + ")");

            apps = new ArrayList<>();

            PackageManager packageManager = getPackageManager();
            Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
            mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);

            List<ResolveInfo> appList = packageManager.queryIntentActivities(mainIntent, 0);

            for (int i = 0; i < appList.size(); i++) {
                if (appList.get(i).activityInfo.packageName.equals(getPackageName()))
                    continue;

                try {
                    ApplicationInfo applicationInfo = packageManager.getApplicationInfo(appList.get(i).activityInfo.packageName, 0);
                    File file = new File(applicationInfo.publicSourceDir);

                    if ((int) (file.length() / (1024 * 1024)) > 0) {
                        apps.add(new AppInit(appList.get(i).loadIcon(getPackageManager()),
                                (String) appList.get(i).loadLabel(getPackageManager()), appList.get(i).activityInfo.packageName, true));

                        apps.get(apps.size() - 1).setAppSize((int) (file.length() / (1024 * 1024)));

                        allSize += file.length() / (1024 * 1024);
                    }
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
            }

            for (int i = 0; i < apps.size(); i++)
                if (runningApps < apps.size()) {
                    apps.remove(i);
                    i--;
                }

            allSize = 0;

            for (AppInit app : apps)
                allSize += app.getAppSize();

            appsSize.setText(allSize + " " + getResources().getString(R.string.mb));
            animateTextView(0, allSize, appsSize);

            measuringAdapter = new myListAdapter(getApplicationContext(), apps);

            appsList.setAdapter(measuringAdapter);

            optimizebutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isExitAvaliable = false;

                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putLong("lastBoostTime", System.currentTimeMillis());
                    editor.apply();

                    apps.clear();

                    measuringAdapter.notifyDataSetChanged();

                    findViewById(R.id.content).setVisibility(View.GONE);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            progressBar = findViewById(R.id.progressBar);
                            progressBar.setVisibility(View.GONE);
                        }
                    }, 2500);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            startActivity(new Intent(getApplicationContext(), AfterCleaningActivity.class));

                            AdsUtility.showInterstitial(SpeedBoosterActivity.this);

                            finish();
                        }
                    }, 1000);
                }
            });
        } else
            findViewById(R.id.content).setVisibility(View.GONE);
    }

    class myListAdapter extends BaseAdapter {
        private final LayoutInflater mLayoutInflater;
        private ArrayList<AppInit> arrayMyMatches;

        public myListAdapter (Context ctx, ArrayList<AppInit> arr) {
            mLayoutInflater = LayoutInflater.from(ctx);
            setArrayMyData(arr);
        }

        public void setArrayMyData(ArrayList<AppInit> arrayMyData) {
            this.arrayMyMatches = arrayMyData;
        }

        public boolean remove (int i) {
            arrayMyMatches.remove(i);

            return true;
        }

        public int getCount () {
            return arrayMyMatches.size();
        }

        public Object getItem (int position) {
            AppInit app = arrayMyMatches.get(position);

            return app;
        }

        public long getItemId (int position) {
            return position;
        }

        //получение элемента ListView и его отправка в активность данных

        public View getView(final int position, View convertView, ViewGroup parent) {
            try {
                if (convertView == null) {
                    LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = inflater.inflate(R.layout.init_app_row_checks, null, true);
                }

                ImageView app_image = convertView.findViewById(R.id.app_image);
                TextView app_name = convertView.findViewById(R.id.name);
                CheckBox checker = convertView.findViewById(R.id.checker);

                checker.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        arrayMyMatches.get(position).setApp_in_quae(checker.isChecked());

                        runningApps = 0;

                        for (AppInit app : arrayMyMatches)
                            if (app.getApp_in_quae())
                                runningApps++;

                        if (checker.isChecked())
                            allSize += arrayMyMatches.get(position).getAppSize();
                        else
                            allSize -= arrayMyMatches.get(position).getAppSize();

                        SpeedBoosterActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                processes.setText(runningApps + "");
                                appsToOptimize.setText("(" + runningApps + " " + getString(R.string.apps) + ")");
                                appsSize.setText(allSize + " " + getResources().getString(R.string.mb));
                            }
                        });
                    }
                });

                checker.setChecked(arrayMyMatches.get(position).getApp_in_quae());

                TextView size = convertView.findViewById(R.id.size);

                app_image.setImageDrawable(arrayMyMatches.get(position).getApp_image());
                app_name.setText(arrayMyMatches.get(position).getApp_name());

                size.setText(arrayMyMatches.get(position).getAppSize() + " MB");

            } catch (Exception e) {

            } finally {
                return convertView;
            }
        }
    }

    public void animateTextView(int initialValue, int finalValue, final TextView  textview) {

        ValueAnimator valueAnimator = ValueAnimator.ofInt(initialValue, finalValue);
        valueAnimator.setDuration(1500);

        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {

                textview.setText(valueAnimator.getAnimatedValue() + " " + getString(R.string.mb));

            }
        });
        valueAnimator.start();
    }

    @Override
    public boolean onKeyDown(final int pKeyCode, final KeyEvent pEvent) {
        if(pKeyCode == KeyEvent.KEYCODE_BACK && pEvent.getAction() == KeyEvent.ACTION_DOWN) {
            onBackPressed();

            return true;
        } else {
            return super.onKeyDown(pKeyCode, pEvent);
        }
    }

    @Override
    public void onBackPressed() {
        if (isExitAvaliable) {
            AdsUtility.showMrec(R.id.exit_banner, this);

            RelativeLayout exitDialog = findViewById(R.id.exit_dialog);
            exitDialog.setVisibility(View.VISIBLE);

            findViewById(R.id.yes).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(getApplicationContext(), MenuActivity.class));

                    AdsUtility.showInterstitial(SpeedBoosterActivity.this);

                    finish();
                }
            });

            findViewById(R.id.no).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    exitDialog.setVisibility(View.GONE);
                }
            });
        }
    }
}
package com.junk.removal.junkremoval.memory.cleaner.actives.secondary;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;

import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.junk.removal.junkremoval.memory.cleaner.actives.secondary.FragmentsActivity;
import com.junk.removal.junkremoval.memory.cleaner.R;
import com.junk.removal.junkremoval.memory.cleaner.actives.secondary.SuperActivity;

import static com.junk.removal.junkremoval.memory.cleaner.actives.InitialScreenActivity.isCleanRequired;
import static com.junk.removal.junkremoval.memory.cleaner.actives.secondary.FragmentsActivity.MODE_START_NOW;

public class NotificationsCleanerActivity extends SuperActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        overridePendingTransition(R.anim.fast_fadein, 0);

        setContentView(R.layout.activity_notif_clear);

        isCleanRequired = true;

        if (ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)

            startFragmentActivity(FragmentsActivity.JUNK_CLEANER_CODE, MODE_START_NOW);
        else
            startFragmentActivity(FragmentsActivity.JUNK_CLEANER_CODE);
    }

    private void startFragmentActivity (String REQUEST_ACTIVITY_CODE, String RUNTIME_MODE) {
        Intent intent = new Intent (getApplicationContext(), FragmentsActivity.class);
        intent.putExtra(FragmentsActivity.REQUEST_ACTIVITY_CODE, REQUEST_ACTIVITY_CODE);
        intent.putExtra(FragmentsActivity.RUNTIME_MODE, RUNTIME_MODE);
        startActivity (intent);

        finish();
    }

    private void startFragmentActivity (String REQUEST_ACTIVITY_CODE) {
        Intent intent = new Intent (getApplicationContext(), FragmentsActivity.class);
        intent.putExtra(FragmentsActivity.REQUEST_ACTIVITY_CODE, REQUEST_ACTIVITY_CODE);
        startActivity (intent);

        finish();
    }
}

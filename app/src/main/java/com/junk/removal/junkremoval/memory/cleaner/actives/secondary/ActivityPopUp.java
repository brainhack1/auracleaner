package com.junk.removal.junkremoval.memory.cleaner.actives.secondary;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.OvershootInterpolator;
import android.widget.TextView;

import com.junk.removal.junkremoval.memory.cleaner.users_cust.PowerAdapter;
import com.junk.removal.junkremoval.memory.cleaner.R;
import com.junk.removal.junkremoval.memory.cleaner.obj.PowerItem;

import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.recyclerview.animators.SlideInLeftAnimator;
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;

public class ActivityPopUp extends SuperActivity{

    RecyclerView recyclerView;
    PowerAdapter mAdapter;
    List<PowerItem> items;
    TextView applied;
    TextView extendedtime,extendedtimedetail;
    int hour;
    int min;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Bundle b=getIntent().getExtras();
        setContentView(R.layout.ultra_popup);
        applied = findViewById(R.id.applied);
        extendedtime= findViewById(R.id.addedtime);
        extendedtimedetail= findViewById(R.id.addedtimedetail);

        try {

            hour = Integer.parseInt(b.getString("hour").replaceAll("[^0-9]", "")) - Integer.parseInt(b.getString("hournormal").replaceAll("[^0-9]", ""));
            min = Integer.parseInt(b.getString("minutes").replaceAll("[^0-9]", "")) - Integer.parseInt(b.getString("minutesnormal").replaceAll("[^0-9]", ""));
        }
        catch(Exception e)
        {
            hour=4;
            min=7;
        }

        if(hour==0 && min==0)
        {
            hour=4;
            min=7;
        }

        extendedtime.setText("Increase "+hour+" h " +Math.abs(min)+" m");
        extendedtimedetail.setText("Added battery life up to "+"\n"+Math.abs(hour)+"h "+Math.abs(min)+"m");

        applied.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i=new Intent(ActivityPopUp.this, ActivityApply.class);
                startActivity(i);

                finish();
            }
        });

        items =new ArrayList<>();

        recyclerView = findViewById(R.id.recycler_view);

        recyclerView.setItemAnimator(new SlideInLeftAnimator());

        recyclerView.getItemAnimator().setAddDuration(200);
        mAdapter = new PowerAdapter(items);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new SlideInUpAnimator(new OvershootInterpolator(1f)));
        recyclerView.computeHorizontalScrollExtent();
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

        final Handler handler1 = new Handler();
        handler1.postDelayed(new Runnable() {
            @Override
            public void run() {
                add("Minimize consume of operating architecture", 1);


            }
        }, 1000);

        final Handler handler2 = new Handler();
        handler2.postDelayed(new Runnable() {
            @Override
            public void run() {
                add("Shutdown of modules of consumed applications", 2);


            }
        }, 2000);

        final Handler handler3 = new Handler();
        handler3.postDelayed(new Runnable() {
            @Override
            public void run() {
                add("Get brightness down to 10%", 0);
            }
        }, 3000);

        final Handler handler4 = new Handler();
        handler4.postDelayed(new Runnable() {
            @Override
            public void run() {
                add("Perform low light layouts for maximum energy economy", 3);
            }
        }, 4000);



        final Handler handler5 = new Handler();
        handler5.postDelayed(new Runnable() {
            @Override
            public void run() {
                add("Deny low important services to work in background", 3);

            }
        }, 5000);

        final Handler handler6 = new Handler();
        handler6.postDelayed(new Runnable() {
            @Override
            public void run() {
                add("Shutdown Systems unused services", 3);

            }
        }, 6000);
    }

    public void add(String text, int position) {
        PowerItem item=new PowerItem();
        item.setText(text);
        items.add(item);
        mAdapter.notifyItemInserted(position);

    }
}

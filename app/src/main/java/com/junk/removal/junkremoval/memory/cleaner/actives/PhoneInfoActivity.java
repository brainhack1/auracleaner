package com.junk.removal.junkremoval.memory.cleaner.actives;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Environment;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.junk.removal.junkremoval.memory.cleaner.R;
import com.google.android.gms.ads.AdRequest;
import com.junk.removal.junkremoval.memory.cleaner.actives.secondary.SuperActivity;
import com.junk.removal.junkremoval.memory.cleaner.utils.AdsUtility;

import java.io.File;

import static com.junk.removal.junkremoval.memory.cleaner.apps.Applic.isAdAllowed;

public class PhoneInfoActivity extends SuperActivity {

    private ImageView backBtn;
    private TextView model, brand, ram, cpu, storage, os, resolution, battery;

//    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        overridePendingTransition(R.anim.fast_fadein, 0);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_device_info);

        backBtn = findViewById(R.id.exit_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        model = findViewById(R.id.model);
        brand = findViewById(R.id.brand);
        ram = findViewById(R.id.ram);
        cpu = findViewById(R.id.cpu);
        storage = findViewById(R.id.storage);
        os = findViewById(R.id.os);
        resolution = findViewById(R.id.resolution);
        battery = findViewById(R.id.battery);

        ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        activityManager.getMemoryInfo(memoryInfo);

        double totalRam = (double)(memoryInfo.totalMem) / (1024 * 1024) /1000;
        totalRam = Math.ceil(totalRam);

        os.setText(Build.VERSION.RELEASE);
        model.setText(Build.PRODUCT);
        brand.setText(Build.MANUFACTURER);
        cpu.setText(Build.HARDWARE);
        ram.setText((int)totalRam + " GB");

        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager wm = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE); // the results will be higher than using the activity context object or the getWindowManager() shortcut
        wm.getDefaultDisplay().getMetrics(displayMetrics);
        int screenWidth = displayMetrics.widthPixels;
        int screenHeight = displayMetrics.heightPixels;

        resolution.setText(screenHeight + "x" + screenWidth);

        try {
            battery.setText(getBatteryCapacity().intValue() + " MAh");

            File external = Environment.getExternalStorageDirectory();

            float freeStorageSpace = (float) Math.round((float) external.getFreeSpace() / (1024 * 1024 * 1024) * 100) / 100;
            float totalStorageSpace = (float) Math.round((float) external.getTotalSpace() / (1024 * 1024 * 1024) * 100) / 100;

            storage.setText(((float) Math.round((totalStorageSpace - freeStorageSpace) * 100) / 100) + " / " + totalStorageSpace + " GB");
        } catch (Exception e) {}
    }

    public Double getBatteryCapacity(){
        Object powerProfile = null;
        Double batteryCapacity = 0d;
        final String POWER_PROFILE_CLASS = "com.android.internal.os.PowerProfile";

        try{
            powerProfile = Class.forName(POWER_PROFILE_CLASS)
                    .getConstructor(Context.class).newInstance(this);
        }catch (Exception e){
            e.printStackTrace();
        }

        try{
            batteryCapacity = (Double) Class.forName(POWER_PROFILE_CLASS)
                    .getMethod("getAveragePower", java.lang.String.class)
                    .invoke(powerProfile, "battery.capacity");
        }catch (Exception e) {
            e.printStackTrace();
        }

        return batteryCapacity;
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(getApplicationContext(), MenuActivity.class));

//        if (isAdAllowed && mInterstitialAd != null && mInterstitialAd.isAdLoaded())
//            mInterstitialAd.show();
        AdsUtility.showInterstitial(PhoneInfoActivity.this);
//        else
//            adBlockIntersitialAd.setAdListener(new AdListener() {
//                @Override
//                public void onAdLoaded() {
//                    adBlockIntersitialAd.show();
//                }
//
//                @Override
//                public void onAdFailedToLoad(int i) {
//                    super.onAdFailedToLoad(i);
//                }
//            });

        finish();
    }
}

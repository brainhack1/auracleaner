package com.junk.removal.junkremoval.memory.cleaner.actives;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.bumptech.glide.Glide;
import com.junk.removal.junkremoval.memory.cleaner.actives.secondary.SuperActivity;
import com.junk.removal.junkremoval.memory.cleaner.obj.ImageCell;
import com.junk.removal.junkremoval.memory.cleaner.obj.ImagesStroke;
import com.google.android.gms.ads.AdRequest;
import com.junk.removal.junkremoval.memory.cleaner.R;

import static com.junk.removal.junkremoval.memory.cleaner.apps.Applic.isAdAllowed;

public class ImageSelectorActivity extends SuperActivity {

    public static ListViewAdapter adapterList;
    private ListView listOfImages;

    private AppsComponentsLoading appsComponentsLoading;

    private ImageView backBtn;
    private TextView allSizeText, allFilesText, cleanBtn;
    private ProgressBar progressBar;
    private Switch selectAllSwitch;

//    private InterstitialAd mInterstitialAd;

    private ArrayList<ImagesStroke> rows;

    private String path;

    private int allPhotos, selectedPhotos;
    private float allSize;

    SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        path = getIntent().getStringExtra("path");

        overridePendingTransition(R.anim.fast_fadein, 0);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_image_stocker);

        rows = new ArrayList<>();

        sharedpreferences = getSharedPreferences("waseem", MODE_PRIVATE);

        progressBar = findViewById(R.id.progressBar);
        progressBar.getIndeterminateDrawable()
                .setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN);

        appsComponentsLoading = new AppsComponentsLoading();
        appsComponentsLoading.execute ();
    }

    private class AppsComponentsLoading extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            File whatsappImages = new File(path);

            checkFiles(whatsappImages);

            Collections.sort(rows, new Comparator<ImagesStroke>() {
                public int compare(ImagesStroke o1, ImagesStroke o2) {
                    return o1.getDate().compareTo(o2.getDate());
                }
            });

            ImageSelectorActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    setContentView(R.layout.activity_image_stocker);

                    backBtn = findViewById(R.id.exit_btn);
                    backBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            onBackPressed();
                        }
                    });

                    allSizeText = findViewById(R.id.allSizeText);
                    allFilesText = findViewById(R.id.allFilesText);
                    cleanBtn = findViewById(R.id.clean_all);

                    listOfImages = findViewById(R.id.images);

                    selectAllSwitch = findViewById(R.id.select_all_switch);

                    findViewById(R.id.select_all).setVisibility(View.VISIBLE);
                    findViewById(R.id.files_info).setVisibility(View.VISIBLE);
                    findViewById(R.id.list_view_linear).setVisibility(View.VISIBLE);
                    findViewById(R.id.progressBarLinear).setVisibility(View.GONE);

                    cleanBtn.setVisibility(View.VISIBLE);

                    animateTextView(0, allPhotos, allFilesText);
                    allSizeText.setText(Math.round(allSize) + " " + getString(R.string.mb));

                    adapterList = new ListViewAdapter(getApplicationContext(), rows);

                    listOfImages.setVisibility(View.VISIBLE);
                    listOfImages.setAdapter(adapterList);

                    listOfImages.setVisibility(View.VISIBLE);

                    selectAllSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                            for (ImagesStroke imagesLine : rows) {
                                for (ImageCell imageFile : imagesLine.getImages())
                                    imageFile.setChecked(b);

                                imagesLine.setLineSelected(b);
                            }

                            if (b)
                                cleanBtn.setText(getString(R.string.clean_all) + " " + allPhotos + " " + getString(R.string.photos));
                            else
                                cleanBtn.setText(getString(R.string.select_photos));

                            adapterList.notifyDataSetChanged();
                        }
                    });

                    cleanBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            for (int i = 0; i < rows.size(); i++) {
                                ImagesStroke imagesLine = rows.get(i);

                                for (int j = 0; j < imagesLine.getImages().size(); j++) {
                                    if (imagesLine.getImages().get(j).isChecked()) {
                                        allPhotos--;
                                        allSize -= ((float)imagesLine.getImages().get(j).length())/1024/1024;

                                        deleteRecursive(imagesLine.getImages().get(j));

                                        imagesLine.getImages().remove(j);

                                        j = -1;
                                    }
                                }

                                if (rows.get(i).getImages().isEmpty()) {
                                    rows.remove(i);

                                    i = -1;
                                }
                            }



                            allFilesText.setText(allPhotos + " " + getString(R.string.photos));
                            allSizeText.setText(Math.round(allSize) + " " + getString(R.string.mb));

                            cleanBtn.setText(getString(R.string.select_photos));

                            adapterList.notifyDataSetChanged();

//                            if (isAdAllowed && mInterstitialAd != null && mInterstitialAd.isAdLoaded())
//                                mInterstitialAd.show();

                            showEndWord();
                        }
                    });
                }
            });

            return null;
        }
    }

    private void deleteRecursive(File fileOrDirectory) {
        try {
            fileOrDirectory.delete();
        } catch (Exception e) {}
    }

    private void showEndWord() {
        if(rows.isEmpty()) {
            listOfImages.setVisibility(View.GONE);
            cleanBtn.setVisibility(View.GONE);

            findViewById(R.id.select_all).setVisibility(View.GONE);
            findViewById(R.id.files_info).setVisibility(View.GONE);
            findViewById(R.id.list_view_linear).setVisibility(View.GONE);
            findViewById(R.id.progressBarLinear).setVisibility(View.VISIBLE);
            findViewById(R.id.progressBar).setVisibility(View.GONE);
            findViewById(R.id.allClearLinear).setVisibility(View.VISIBLE);
        }
    }

    //работа с отображением элементов ListView

    class ListViewAdapter extends BaseAdapter {
        private ArrayList<ImagesStroke> arrayMyMatches;

        public ListViewAdapter(Context ctx, ArrayList<ImagesStroke> arr) {
            setArrayMyData(arr);
        }

        public ArrayList<ImagesStroke> getArrayMyData() {
            return arrayMyMatches;
        }

        public void setArrayMyData(ArrayList<ImagesStroke> arrayMyData) {
            this.arrayMyMatches = arrayMyData;
        }

        public boolean remove (int i) {
            arrayMyMatches.remove(i);

            return true;
        }

        public int getCount () {
            return arrayMyMatches.size();
        }

        public Object getItem (int position) {
            ImagesStroke app = arrayMyMatches.get(position);

            return app;
        }

        public long getItemId (int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            try {
                if (convertView == null) {
                    LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = inflater.inflate(R.layout.image_cleaner_init_row, null, true);
                }

                TextView date = convertView.findViewById(R.id.image_date);
                date.setText(arrayMyMatches.get(position).getDate());

                Switch daySwitch = convertView.findViewById(R.id.select_day_switch);
                daySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        for (ImageCell imageFile : rows.get(position).getImages())
                            imageFile.setChecked(b);

                        rows.get(position).setLineSelected(b);

                        selectedPhotos = 0;

                        for (ImagesStroke imagesLine : rows)
                            for (ImageCell imageFile : imagesLine.getImages()) {
                                if (imageFile.isChecked())
                                    selectedPhotos++;
                                else {
                                    imagesLine.setLineSelected(false);
                                }
                            }

                        if (selectedPhotos != 0)
                            cleanBtn.setText(getString(R.string.clean_all) + " " + selectedPhotos + " " + getString(R.string.photos));
                        else
                            cleanBtn.setText(getString(R.string.select_photos));

                        allFilesText.setText(allPhotos + " " + getString(R.string.photos));
                        allSizeText.setText(Math.round(allSize) + " " + getString(R.string.mb));

                        adapterList.notifyDataSetChanged();
                    }
                });
                daySwitch.setChecked(rows.get(position).isLineSelected());

                RecycleImageAdapter recycleListAdapter = new RecycleImageAdapter(arrayMyMatches.get(position).getImages());
                RecyclerView recyclerView = convertView.findViewById(R.id.recycler_view);
                LinearLayoutManager layoutManager = new LinearLayoutManager(ImageSelectorActivity.this, LinearLayoutManager.HORIZONTAL, false);
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setAdapter(recycleListAdapter);

            } catch (Exception e) {

            } finally {
                return convertView;
            }
        }
    }

    private class RecycleImageAdapter extends RecyclerView.Adapter<RecycleImageAdapter.ViewHolderMain> {

        private final List<ImageCell> arrayMyMatches;

        public RecycleImageAdapter(List<ImageCell> arrayMyMatches)
        {
            this.arrayMyMatches = arrayMyMatches;
        }

        @Override
        public ViewHolderMain onCreateViewHolder(ViewGroup parent, int viewType) {

            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.image, parent, false);
            return new ViewHolderMain(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolderMain holder, int position) {
            Glide.with(getApplicationContext()).load(arrayMyMatches.get(position).getAbsoluteFile()).into(holder.image);

            holder.checkBox.setChecked(arrayMyMatches.get(position).isChecked());
            holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    arrayMyMatches.get(position).setChecked(b);

                    selectedPhotos = 0;

                    for (ImagesStroke imagesLine : rows)
                        for (ImageCell imageFile : imagesLine.getImages()) {
                            if (imageFile.isChecked())
                                selectedPhotos++;
                            else {
                                imagesLine.setLineSelected(false);
                            }
                        }

                    if (selectedPhotos != 0)
                        cleanBtn.setText(getString(R.string.clean_all) + " " + selectedPhotos + " " + getString(R.string.photos));
                    else
                        cleanBtn.setText(getString(R.string.select_photos));

                    allFilesText.setText(allPhotos + " " + getString(R.string.photos));
                    allSizeText.setText(Math.round(allSize) + " " + getString(R.string.mb));

                    notifyDataSetChanged();
                }
            });
        }

        @Override
        public int getItemCount() {
            return arrayMyMatches.size();
        }

        private class ViewHolderMain extends RecyclerView.ViewHolder {
            ImageView image;
            CheckBox checkBox;

            public ViewHolderMain(View view) {
                super(view);

                image = view.findViewById(R.id.image);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    image.setClipToOutline(true);
                }

                checkBox = view.findViewById(R.id.isChecked);

            }
        }
    }

    private void checkFiles(File parentDir) {
        File[] files = parentDir.listFiles();

        try {
            if (files != null)
                for (File file : files) {
                    if (!file.isHidden()) {
                        if (file.isDirectory()) {
                            checkFiles(file);
                        } else if (file.isFile()) {
                            Date lastModDate = new Date(file.lastModified());

                            String date = new SimpleDateFormat("yyyy.MM.dd", Locale.getDefault()).format(lastModDate);

                            boolean found = false;

                            for (ImagesStroke line : rows) {
                                if (line.getDate().equals(date)) {
                                     line.getImages().add(new ImageCell(file));
                                    found = true;
                                }
                            }

                            if (!found) {
                                rows.add(new ImagesStroke(date, new ImageCell(file)));
                            }

                            allPhotos++;
                            allSize += ((float)file.length())/1024/1024;
                        }
                    }
                }
        } catch (Exception e) {}
    }

    //проверка наличие разрешение при запросе либо проверка при предоставлении

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (requestCode == 1) {
                startActivity(new Intent(ImageSelectorActivity.this, ImageSelectorActivity.class));
                finish();
            }
        }
    }

    public void animateTextView(int initialValue, int finalValue, final TextView  textview) {

        ValueAnimator valueAnimator = ValueAnimator.ofInt(initialValue, finalValue);
        valueAnimator.setDuration(1500);

        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {

                textview.setText(valueAnimator.getAnimatedValue() + " " + getString(R.string.photos));

            }
        });
        valueAnimator.start();
    }

    @Override
    public boolean onKeyDown(final int pKeyCode, final KeyEvent pEvent) {
        if(pKeyCode == KeyEvent.KEYCODE_BACK && pEvent.getAction() == KeyEvent.ACTION_DOWN) {

            startActivity(new Intent(getApplicationContext(), WhatsappFilesActivity.class));

            finish();

            return true;
        } else {
            return super.onKeyDown(pKeyCode, pEvent);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(getApplicationContext(), WhatsappFilesActivity.class));

        finish();
    }
}
